# coding=utf-8
'''
Created on 8 déc. 2009

@author: Olivier Hoareau
'''

from hd3dlib.util import wsRequestHandler

class ImageFileSystem(object):
    '''
    Utility class to access to set image file.
    '''
    locator = '/images/'

    def __init__(self, locator=None):
        if locator:
            self.locator = locator
     
    
    def postForm(self, target, files={}, fields={}):
        '''
        Post files to target. Expected files are images.  
        '''
        from hd3dlib.util import multipartformencode
        head, body = multipartformencode.encode(files, fields)

        return wsRequestHandler.post('%s%s' % (self.locator, str(target)), body, False, headers=head)
    
        
    def setThumbnail(self, folder, filePath, id):
        '''
        Upload image (located at *filePath*) to specified sub-folder of 
        thumbnail service.
        After uploading image,  thumbnail and preview are built from image
        and accessible to following URLs : /images/thumbnail/folder/id and
        /images/preview/folder/id  
        '''
        print '[INFO] UPLOADING IMAGE to url:thumbnails/%s' % folder
        print '[INFO] UPLOADING IMAGE: %s' % filePath
        self.postForm('thumbnails/%s' % folder, 
                    files={'image':filePath},  
                    fields={'id':str(id)})
