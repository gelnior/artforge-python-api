# coding=utf-8
from hd3dlib.production import Category, Sequence, Constituent, Shot, Composition
from hd3dlib.util import ODSpreadsheet

import hd3dlib.util

class Breakdown(object):
        
    sheetsOfInterest = ['Shots',
                        'Categories',
                        'Constituents',
                        'Sequences']
    
    def __init__(self):
        self.projectDBId = 0
        self.uuid2id={}
        self.projectId=str(hd3dlib.util.generateRandomId(self,12))
        self.hasShots=False
        self.hasCategories=False
        self.hasSequences=False
        self.hasConstituents=False
        
        self.shotRecords={}
        self.categoryRecords={}
        self.sequenceRecords={}
        self.constituentRecords={}
        self.compositionRecords={}
        
            
    def setODS(self, path):
        self.ods=ODSpreadsheet(path)
        foundSheets = self.ods.filterSheetList(self.sheetsOfInterest)
        if 'Shots' in foundSheets: 
            self.hasShots = True
        else:
            raise ValueError, 'ODS file has no shot sheet.'
        if 'Categories' in foundSheets: 
            self.hasCategories = True
        if 'Constituents' in foundSheets: 
            self.hasConstituents = True
        if 'Sequences' in foundSheets: 
            self.hasSequences = True
        
        
    def setProjectDBId(self, projectId):
        self.projectDBId=long(projectId)
        
        
    def processSequences(self):
        recList=self.ods.getRecords('Sequences')
        #original sequence names will be used as unique keys in the main record list
        keyList=[]
        self.sequenceRecords={}
        for rec in recList:
            path = rec['sequence.name']
            rec['path']=path
            keyList.append(path)
            self.sequenceRecords[path]=rec
            
        for path in keyList:
            if path.count('/') > 0:
                #this is a sub-sequence
                indx = path.rfind('/') + 1
                self.sequenceRecords[path]['sequence.name']=path[indx:]
                indx=indx-1
                parentName = path[:indx]
                if parentName in keyList:
                    #parent exists
                    parent = self.sequenceRecords[parentName]['uuid']
                    self.sequenceRecords[path]['sequence.parent.fk']=parent
                else:
                    msg = 'Sequence \'' + path + '\' in ODS file has no parent'
                    raise ValueError, msg
            else:
                self.sequenceRecords[path]['sequence.project.fk']=self.projectId
        
        
    def processCategories(self):
        recList=self.ods.getRecords('Categories')
        #original category names will be used as unique keys in the main record list
        
        titlesArr = self.ods.getTitlesArr('Categories')
        print titlesArr.keys()
        
        keyList=[]
        self.categoryRecords={}
        for rec in recList:
            path = rec['category.name']
            rec['path']=path
            keyList.append(path)
            self.categoryRecords[path]=rec
            
        for path in keyList:
            if path.count('/') > 0:
                #this is a sub-category
                indx = path.rfind('/') + 1
                self.categoryRecords[path]['category.name']=path[indx:]
                indx=indx-1
                parentName = path[:indx]
                if parentName in keyList:
                    #parent exists
                    parent = self.categoryRecords[parentName]['uuid']
                    self.categoryRecords[path]['category.parent.fk']=parent
                else:
                    msg = 'Category \'' + path + '\' in ODS file has no parent'
                    raise ValueError, msg
            else:
                self.categoryRecords[path]['category.project.fk']=self.projectId
        
        
    def processConstituents(self):
        entity = 'Constituents'
        recList=self.ods.getRecords(entity)
            
        
#        attributeList = self.getOffLineAttributes(entity, False)
#        for attributeName in attributeList:
#            #is it a class attribute ?
#            #is it a step attribute ?
#            #is it a dynamic attribute ?
#            pass
        
            
        #Constituent labels will be used as unique keys in the main record list
        keyList=[]
        self.constituentRecords={}
        for rec in recList:
            label = rec['constituent.label']
            keyList.append(label)
            self.constituentRecords[label]=rec
                        
        for label in keyList:
            categoryPath = self.constituentRecords[label]['category.name']
            #category names are converted into category foreign keys
            if categoryPath in self.categoryRecords:
                self.constituentRecords[label]['constituent.category.fk']=self.categoryRecords[categoryPath]['uuid']
            else:
                raise ValueError, 'Some categories in ODS Constituents are missing in Categories'
        
           
    def processShots(self):
        recList=self.ods.getRecords('Shots')
        #Shot sequence + labels will be used as unique keys in the main record list
        keyList=[]
        self.shotRecords={}
        for rec in recList:
            #label = rec['shot.label']
            uniqueLabel = rec['sequence.name'] + rec['shot.label']
            #keyList.append(label)
            #self.shotRecords[label]=rec
            keyList.append(uniqueLabel)
            self.shotRecords[uniqueLabel]=rec
                        
        for uniqueLabel in keyList:
            sequencePath = self.shotRecords[uniqueLabel]['sequence.name']
            if sequencePath in self.sequenceRecords:
                self.shotRecords[uniqueLabel]['shot.sequence.fk']=self.sequenceRecords[sequencePath]['uuid']
            else:
                # sequencePath
                #print self.sequenceRecords.keys()
                raise ValueError, 'Some sequences in ODS Shots are missing in Sequences'
            
        #casting processing
        attributeList = self.getOffLineAttributes('Shots', False)
        for attr in attributeList:
            if attr.find('casting.') == 0:
                #this column contains casting information
                categoryPath = attr[8:]
                #print categoryPath
                if categoryPath in self.categoryRecords:
                    for key in self.shotRecords:
                        shot = self.shotRecords[key]
                        constituents = shot[attr].strip()
                        if constituents is not '':
                            if type(constituents) is not type([]):
                                constituentList = constituents.split(',')
                            else: 
                                constituentList = constituents
                            #print 'Constituent list : \'%s\'' % constituentList
                            if len(constituentList) is not 0:
                                for constituent in constituentList:
                                    constituent = constituent.strip()
                                    #print '>' + constituent + '<'
                                    if constituent in self.constituentRecords:
                                        
                                        #print 'OK ' + key + ' <-> ' + constituent
                                        constituentFK = self.constituentRecords[constituent]['uuid']
                                        shotFK = self.shotRecords[key]['uuid']
                                        joinKey = constituentFK + '_' + shotFK
                                        newCompo = { 'composition.shot.fk' : shotFK,
                                                    'composition.constituent.fk' : constituentFK,
                                                    'uuid' : str(hd3dlib.util.generateRandomId(joinKey,12))
                                                    }
                                        self.compositionRecords[joinKey] = newCompo
                                    else:
                                        msg = 'Constituent \'' + constituent + '\' does not exist as a ' + categoryPath + ' in shot (' + key + ')'
                                        raise ValueError, msg
                else:
                    msg = 'Casting category \'' + categoryPath + '\' does not exist'
                    raise ValueError, msg
             
             
    def createInDatabase(self):
        self.uuid2id[self.projectId]=self.projectDBId
        self.createDBInstances('Categories')
        self.createDBInstances('Constituents')
        self.createDBInstances('Sequences')
        self.createDBInstances('Shots')
        self.createDBInstances('Compositions')
        
        
    def createDBInstances(self, entity):
        records = self.getEntityRecords(entity)
        catKeys = records.keys()
        catKeys.sort()
        
        i = 0
        tmpList = list()
        tmpCat = list()
        for rec in catKeys:
            thisCat = records[rec]
            attributeList = thisCat.keys()
            
            #Filter class attributes
            res = []
            prefix = self.getEntityPrefix(entity) + '.'
            for attribute in attributeList:
                if attribute.find(prefix) == 0:
                    res.append(attribute)
            kw = {}
                    
            for attr in res:
                indx = len(prefix)
                attrName = attr[indx:]
                if attrName.find('.fk') > 0:
                    #this value must be a forein key
                    indx = len(attrName)-3
                    attrName = attrName[:indx]
                    value = self.uuid2id[thisCat[attr]]
                    thisKey = str(attrName)
                    kw[thisKey] = long(value)
                else:
                    if attrName.find('.long') > 0:
                        attrList = attrName.split('.',1)
                        thisKey = str(attrList[0])
                        kw[thisKey] = long(thisCat[attr])
                    else:
                        thisKey = str(attrName)
                        kw[thisKey] = thisCat[attr].encode("utf-8")
            
#            #Filter step attributes
#            res = []
#            prefix = 'step.'
#            for attribute in attributeList:
#                if attribute.find(prefix) == 0:
#                    res.append(attribute)
#            kw = {}
#            for attr in res:
#                indx = len(prefix)
#                attrName = attr[indx:]
#                thisKey = str(attrName)
#                kw[thisKey] = str(thisCat[attr])
                    
                    
            #TODO : compute real step values                             
            if entity == 'Constituents':
                kw['design'] = True
                kw['modeling'] = True
                kw['setup'] = True
            if entity == 'Shots':
                kw['animation'] = True
                kw['rendering'] = True
               
            kw['project'] = self.projectDBId
    
            # Categories and sequence are saved one by one. Shots, constituents 
            # and compositions are saved by pack of 50 (bulk post).             
            if entity == 'Constituents' or entity == 'Shots' or entity == 'Compositions' :
                tmp = self.createNewEntity(entity, **kw)
                tmpList.append(tmp)  
                tmpCat.append(thisCat['uuid'])              
            else :
                tmp = self.createNewEntityInDB(entity, **kw)     
                self.uuid2id[thisCat['uuid']]=tmp.id                               
            
            i = i+1
            if i == 50 and (entity == 'Constituents' or entity == 'Shots' or entity == 'Compositions') : 
                i = 0
                ids = self.createNewEntityList(entity, tmpList)
                print '%d entities posted' % len(tmpList)
                for j in range(50):
                    self.uuid2id[tmpCat[j]]  = ids[j]
                
                tmpList = list()
                tmpCat = list()
                
            print entity + ' : ' + rec.encode("utf-8")
            
        
        if i > 0 and (entity == 'Constituents' or entity == 'Shots' or entity == 'Compositions') :
            ids = self.createNewEntityList(entity, tmpList)
            print '%d entities posted' % len(tmpList)
            for j in range(len(ids)):
                self.uuid2id[tmpCat[j]]  = ids[j]
             
            tmpList = list()
            tmpCat = list()
            
            
                         
    def getEntityPrefix(self, entity):
        if entity == 'Shots': return 'shot'
        if entity == 'Sequences': return 'sequence'
        if entity == 'Constituents': return 'constituent'
        if entity == 'Categories': return 'category'
        if entity == 'Compositions': return 'composition'
        
    def getEntityRecords(self, entity):
        if entity == 'Shots': return self.shotRecords
        if entity == 'Sequences': return self.sequenceRecords
        if entity == 'Constituents': return self.constituentRecords
        if entity == 'Categories': return self.categoryRecords
        if entity == 'Compositions': return self.compositionRecords
        
    def getNumberOfReadItems(self, entity):
        rec = self.getEntityRecords(entity)
        return len(rec.keys())    
    
    def showRecords(self, entity):
        records = self.getEntityRecords(entity)
        for record in records:
            print str(record) +' : '+ str(records[record])
        
        
    def createNewEntity(self, entity, **kw):
        if entity == 'Shots': return Shot(**kw)
        if entity == 'Sequences': return Sequence(**kw)
        if entity == 'Constituents': return Constituent(**kw)
        if entity == 'Categories': return Category(**kw)
        if entity == 'Compositions': return Composition(**kw)
       
        
    def createNewEntityInDB(self, entity, **kw):
        if entity == 'Shots': return Shot(**kw).create()
        if entity == 'Sequences':
            s =  Sequence(**kw)
            s.project = self.projectDBId
            s._resetLocator()
            s.create()
            return s
        if entity == 'Constituents': return Constituent(**kw).create()
        if entity == 'Categories': return Category(**kw).create()
        if entity == 'Compositions': return Composition(**kw).create()
        
    # Perform a bulk post with entities as data. It works only for constituents, 
    # shots and compositions.
    def createNewEntityList(self, entity, entities):
            
        if entity == 'Constituents': 
            ids = Constituent.query().branch('projects', self.projectDBId).postAll(entities)
        elif entity == 'Shots': 
            Shot._locator = 'shots/'
            ids = Shot.query().branch('projects', self.projectDBId).postAll(entities)
        elif entity == 'Compositions': 
            Composition._locator = 'compositions/'
            ids = Composition.query().postAll(entities)
            
        return ids
            
         
        
          
    def getOffLineAttributes(self, entity, forDB):
        records = self.getEntityRecords(entity)
        firstKey = records.keys()[0]
        first = records[firstKey]
        attributeList = first.keys()
        res = []
        if forDB is True:
            prefix = self.getEntityPrefix(entity) + '.'
            for attribute in attributeList:
                if attribute.find(prefix) == 0:
                    res.append(attribute)
        else:
            res = attributeList
        return res
        