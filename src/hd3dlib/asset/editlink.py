# coding=utf-8
'''
Created on 28 juil. 2010

@author: alexandre.pavot
'''

from hd3dlib.asset import AssetGroup

class AddLink():
    
    '''
    Crée et renvoie le lien entre deux assets donn�s.
    On a en entrée les deux assets � li�s.
    '''
    @classmethod
    def addAssetLink(cls,asset1,asset2):
        return asset1.addAssetLink(asset2)
    
    '''
    Ajout d'un asset dans la liste d'un constituent.
    Cette methode prend en entr�e l'asset a rajouter et le constituent auquel on rajoute l'asset.
    '''
    @classmethod 
    def addAssetToConstituent(cls,constituent,assetrevision):
        criteria = 'fr.hd3d.model.lightweight.impl.LConstituent.id'
        assetgroup = AssetGroup.query().filter('value',str(constituent.id)).filter('criteria',criteria).getFirst()
         
        if assetgroup is None:
            assetgroup = AssetGroup.createForWorkObject(constituent)

        assetgroup.addAssetRevision(assetrevision)

    '''
    Ajout d'un asset dans la liste du shot.
    Cette methode prend en entr�e l'asset a rajout� et le shot auquel on rajoute l'asset.
    '''
    @classmethod
    def addAssetToShot(cls,shot,assetrevision):
        criteria = 'fr.hd3d.model.lightweight.impl.LShot.id'
        assetgroup = AssetGroup.query().filter('value',str(shot.id)).filter('criteria',criteria).getFirst()
        
        if assetgroup is None:
            assetgroup = AssetGroup.createForWorkObject(shot)
        
        assetgroup.addAssetrevision(assetrevision)
        

class RemoveLink():
    


    '''
    Supprime un asset dans la liste d'un constituent (de la liset de l'asset group lié au constituant).
    Cette methode prend en entr�e l'asset a supprim� et le constituent auquel on supprime l'asset.
    '''
    @classmethod
    def removeAssetToConstituent(cls, constituent, assetrevision):        
        assetgroup = AssetGroup.getByConstituent(constituent)
        
        assetgroup.assetRevisions.remove(assetrevision.id)
        assetgroup.update()
        
    '''
    Supprime un asset dans la liste d'un shot.
    Cette methode prend en entr�e l'asset a supprim� et le shot auquel on supprime l'asset.
    '''    
    @classmethod
    def removeAssetToShot(cls, shot, assetrevision):
        criteria = 'fr.hd3d.model.lightweight.impl.LShot.id'
        assetgroup = AssetGroup.query().filter('value',str(shot.id)).filter('criteria',criteria).getFirst()

        assetgroup.assetRevisions.remove(assetrevision.id)      
        assetgroup.update()
        
    '''
    Supprime le liens orienter entre deux Revisions
    Cela prend en entr�, les deux AssetRevisionFileRevisions � lier
    '''
    '''
    @classmethod
    def removeLinkFileRevisionToAsset(cls, assetrevisionfilerevision1, assetrevisionfilerevision2):
        assetrevisionfilerevision1.removeAssetRevisionLink(assetrevisionfilerevision2)
    '''