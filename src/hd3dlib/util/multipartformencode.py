# coding=utf-8

'''

'''

def encode(files={}, fields={}):
    '''
    Utility method for form handling. It encodes files dict and fields dict as
    an HTTP form.
    
    Arguments :
        *files* dict to encode as an HTTP form
        *fields* dict to encode as an HTTP form
    '''
    CRLF = '\r\n'
    
    def getContentType(filename):
        '''
        Return content type of a given file.
        
        *filename* Path to the file from which content type is expected. 
        '''
        import mimetypes
        return mimetypes.guess_type(filename)[0] or 'application/octet-stream'
    
    def generateBoundary():
        '''
        Generate boundary for generated form.
        '''
        from random import Random
        from datetime import datetime   
        r = Random(datetime.now())
        boundary = '----' + '-' * r.randint(1, 5) + '-' * r.randint(5, 10)
        boundary += str(r.randint(0, 1000)) + str(r.randint(0, 1000)) + str(r.randint(0, 1000)) + str(r.randint(0, 1000))
        return boundary
    
    boundary = generateBoundary()
    
    # Prepare BODY
    body = []

    for (key, filename) in files.items():
        body.append('--%s' % boundary)
        body.append('Content-disposition:form-data; name=%s; filename=%s' % (key, filename))
        body.append('Content-type:%s' % getContentType(filename))
        body.append('')
        body.append(open(filename, 'rb').read())
    
    for (key, value) in fields.items():
        body.append('--%s' % boundary)
        body.append('Content-disposition:form-data; name=%s;' % key)
        body.append('')
        body.append(value)

    body.append('--%s--' % boundary)
    body.append('')
    
    body = CRLF.join(body)
    
    # Prepare HEADER
    headers = {'Content-type':('multipart/form-data; boundary=%s' % boundary),
               'Content-length':str(len(body))
               }
    
    return headers, body
