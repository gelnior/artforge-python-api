# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from hd3dlib.util import wsRequestHandler

from string import Template

try:
    import json
except ImportError:
    import simplejson as json


SCRIPT_DEFINITION = {
    'name':'ScriptGenerated',
    'attributes':{
        '_class':Attribute('class', 
                           str, 
                           default='fr.hd3d.model.lightweight.impl.LScript', 
                           mutable=False
                           ),
        'name':Attribute('name', str),
        'command':Attribute('location', str),
        'trigger':Attribute('trigger', str),
        'description':Attribute('description', str),
        'exitCode':Attribute('exitCode', int, default=0, mutable=False),
        'lastRun':Attribute('lastRun', str, mutable=False),
        'lastExitCode':Attribute('lastExitCode', int, mutable=False),
        'lastRunMessage':Attribute('lastRunMessage', str, mutable=False)
    }
}

class Script(makeClass(SCRIPT_DEFINITION, BaseObject)):
    '''
    Script allows to execute scripts on request or automatically when a 
    specific action occurs. This can be set via trigger fields. 
    '''
    
    _locatorTmpl = Template('/scripts/')
    _locatorShort = '/scripts/'
    _locator = '/scripts/'
    
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        

    def execute(self, params=None):
        '''
        Make a get request to execute the script command on server with 
        given params.
        
        Arguments:
            *params* The parameters sent to executed script.
        '''
        import urllib
        url = self._locator + self.name
        if params is not None:
            queryString = list()
            jsonParams = ('params', json.dumps(params))
            queryString.append(jsonParams)
            url += '?' + urllib.urlencode(queryString)
        try:
            print 'URL used to launch logs cleaning : ' + url
            res = wsRequestHandler.get(url)
            
            return res
        except Exception:
            print 'Warning in get request (after script execution) - TODO dev'
        return 0
    