# coding=utf-8

from hd3dlib.util.httprequests import HttpRequestHandler, HttpRequestTrace
from hd3dlib import conf
from hd3dlib.asset import AssetGroup
from hd3dlib.asset import AssetRevision
from hd3dlib.asset import AssetRevisionLink

from hd3dlib.task import TaskType

from hd3dlib.production import Project
from hd3dlib.team import Person
from hd3dlib.production import Step


global user

global droppedAssets
global newAssets

droppedAssets = 0
newAssets = 0

UTF8 = 'utf-8'
   
#Each work object has one asset created per non-null step value
#Asset groups are created for each work object.
def createWorkObjectAssetsAndGroups(project, workObject, stepArray, taskTypesArray):
    global user
                                
                                
    taskTypes = TaskType.query().getAll()
    taskTypeIds=dict()
    for taskType in taskTypes:
        taskTypeIds[taskType.name] = taskType.id        
                                
    # Asset groups for work objects are called : '<wo-hook>_GRP'    
    woAssetGroup = AssetGroup.createForWorkObject(workObject)
    
    formerAssetRevision = None
    for stepName in stepArray.keys():
                
        if isinstance(stepName, str):
            stepName = stepName.decode(UTF8)
        
        step = stepArray[stepName]
        duration = step.getValue(workObject)
        taskType = taskTypeIds[stepName]
               
        # Retrieve corresponding asset if exists.
        #assetName = workObject.hook + '_' + stepName
        assetName = '%s (default)' % stepName
        assetName = assetName.encode(UTF8)
        query = AssetRevision.query().filter('name', assetName)
        query = query.filter('project', project.id)
        query = query.filter('type', taskType)
        a = query.getFirst()
                        
        # If duration is over 0, create the asset, link it with previous asset
        # Then add asset to the work object group and to task group corresponding
        # to step.
        if duration > 0:                     
            if a is None:
                a = AssetRevision(name = assetName, 
                                  project = project.id, 
                                  status = 'UNVALIDATED', 
                                  validity = 'PROD', 
                                  taskType = taskType,
                                  variation = 'default', 
                                  creator = user.id, 
                                  lastUser = user.id)

                log.write('Asset created : %s %s' % (assetName,a.name))
                a.create()

                        
            if formerAssetRevision is not None:
                inputVariation = str(formerAssetRevision.variation)
                outputVariation = str(a.variation)
                aLnk = AssetRevisionLink.getOrCreate(
                                         inputKey = str(formerAssetRevision.key),
                                         inputVariation = inputVariation,
                                         inputRevision = formerAssetRevision.revision,
                                         outputKey = str(a.key),
                                         outputVariation = outputVariation,
                                         outputRevision = a.revision)
                
            formerAssetRevision = a
            
            if not a.id in woAssetGroup.assetRevisions:
                woAssetGroup.assetRevisions.append(a.id)
                woAssetGroup.update()

        # If duration is equal to zero and corresponding asset exists, asset  
        # will be deleted.
        else:
            if a is not None:
                a.delete()
                droppedAssets = droppedAssets + 1

              
def doIt(args, log):
    global user
    
    log.write('Before get project')
    project =  Project.get(args['projectId'])
    log.write('Processing for project %s' % project.name.encode(UTF8))
    user = Person.getScriptUser()
    
#    project.deleteEveryAssets()
    
    # Constituents handling 
    constituents = project.getAllConstituents()
    log.write('%d constituents found.' % len(constituents))

    stepArray = Step.getArray('Constituent', project.id)
    stepList =  str(stepArray.keys())
    log.write('Constituent steps : %s' % stepList)
    taskTypesArr = TaskType.getTaskTypesArray(stepArray.keys())
    
    for c in constituents:
        createWorkObjectAssetsAndGroups(project, c, stepArray, taskTypesArr)
            
    #Shots handling       
    shots = project.getAllShots()
    log.write('%d shots found.' % len(shots))
    
    stepArray = Step.getArray('Shot', project.id)
    stepList =  str(stepArray.keys())
    log.write('Shot steps : %s' % stepList)
    taskTypesArr = TaskType.getTaskTypesArray(stepArray.keys())
        
    for s in shots:
        createWorkObjectAssetsAndGroups(project, s, stepArray, taskTypesArr)
        
    log.write('Asset created : %d' % newAssets)
    log.write('Asset deleted : %d' % droppedAssets)    
    
if __name__ == '__main__':
        import sys
        import os
        from hd3dlib.reflex import Log
        from hd3dlib.reflex import evalArgDictionary
        
        log = None
#    
#    try:
        logLocation = conf.dataServerLogPath
        if not os.path.exists(logLocation):
            raise ValueError('Log directory does not exist.')
            
        log = Log(str(os.path.basename(__file__)), logLocation)
        log.enableConsole()
        log.start()
        
        log.write('Arguments are : ' + str(sys.argv))
#        args = eval(eval(sys.argv[1]))
        args = evalArgDictionary(sys.argv[1])
#        p = Project.query().filter('name', 'ProjectA').getFirst()
#        args = {'projectId' : p.id}
        
        if not isinstance(args, dict):
            raise ValueError('Malformed arguments dictionnary.')
        
        log.write('Before I process everything.')
        doIt(args, log)
        log.write('After I\'ve processed everything.')
        
#    except Exception, e:
#        if log is not None:
#            log.write('Error: ' + str(e.message) + ' '  + str(type(e)))
#        else:
#            print 'Error:', e.message, type(e)
#    finally:
#        if log is not None:
#            log.stop()

