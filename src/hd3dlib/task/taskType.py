# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

 
from string import Template

TASK_TYPE_DEFINITION = {
    'name':'TaskTypeGenerated',
    'attributes':{
        '_class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LTaskType', 
            mutable=False
        ),
        'name':Attribute('name', str),
        'color':Attribute('color', str),
        'project':Attribute('projectID', long),
        'description':Attribute('description', str),
        'entityName':Attribute('name', str)
    }
}

class TaskType(makeClass(TASK_TYPE_DEFINITION, BaseObject)):
    '''
    Task type corresponds to a step of fabrication like modeling, texturing,
    layout... This object is useful to connect assets, validations and tasks
    because each of them are linked to it.
    '''
    
    _locatorTmpl = Template('/task-types/')
    _locatorShort = '/task-types/'
    _locator = '/task-types/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
    

    @classmethod
    def getTaskTypesArray(cls, taskTypes):
        '''
        Deprecated
        '''
        taskTypesArr = {}
        defaultTaskTypeColor = '#AAAAAA'
        
        for taskTypeName in taskTypes:
            if isinstance(taskTypeName, str):
                taskTypeName = taskTypeName.decode("utf-8")
            taskTypeName = taskTypeName.encode("utf-8")
            
            taskType = TaskType.query().filter('name', taskTypeName).getFirst()
            
            if taskType is None:
                taskType = TaskType(name=taskTypeName, 
                                    color=defaultTaskTypeColor).create()
            taskTypesArr[taskTypeName.decode("utf-8")] = taskType
        return taskTypesArr
    
    
    @classmethod
    def getByName(cls,  name):
        '''
        Return task type of which name is equal to name.
        
        Arguments:
            name Name of the task type to retrieved.
        '''
        
        return TaskType.query().filter('name', name).getFirst()
    
    @classmethod
    def getOrCreateByName(cls, name, description='', color='#FFFFFF'):
        '''
        Return task type of which name is equal to name. If task type does not 
        exist it is is created. 
        
        Arguments:
            name Name of the task type to retrieved.
        '''        
        taskType = TaskType.getByName(name)
    
        if taskType is None:  
            taskType=TaskType()      
            taskType.name=name
            taskType.description=description
            taskType.color=color
            taskType.create()
        
        return taskType
    
    
    @classmethod
    def getByProject(cls, projectId):
        '''
        Return task types selected for the project of which id is equal 
        to *projectId*.
                
        Arguments:
            projectId ID of the project of which task types are retrieved.
        '''
        
        taskTypes = []
        
        from hd3dlib.production import Project
        project = Project.get(projectId)
        if project:
            taskTypes = project.getTaskTypes()
        
        return taskTypes
    
    @classmethod
    def getSpecificTaskTypeByProject(cls, projectId):
        '''
        Return task types that exists only for a given project.
        
        Arguments:
            projectId ID of the project of which specific task types are 
                      retrieved.
        '''
        
        return TaskType.query().filter("projectId", projectId).getAll();
    
    