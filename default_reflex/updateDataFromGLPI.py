#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

'''
Created on 2011

@author: Jérôme Gonnon, Melissa Faucher
'''

from hd3dlib.util.httprequests import HttpRequestHandler, HttpRequestTrace
from hd3dlib import *
from hd3dlib.inventory import Computer, Screen, Device, Room, ComputerType, ComputerModel
from hd3dlib.team import Person
from hd3dlib import conf

import sys
import pprint
import os
import MySQLdb

#GLOBALS
GLPI_HOST           = '@@GLPI-HOST@@'
GLPI_USER           = '@@GLPI-USER@@'
GLPI_PASS           = '@@GLPI-PASS@@'
GLPI_DATABASE       = '@@GLPI-DB@@'
GLPI_COMPUTER_TABLE = '@@GLPI_COMPUTER_TABLE@@'
GLPI_SCREEN_TABLE   = '@@GLPI_SCREEN_TABLE@@'
GLPI_LINK_TABLE     = '@@GLPI_LINK_TABLE@@'
GLPI_ROOM_TABLE     = '@@GLPI_ROOM_TABLE@@'

STATUS = {} ### TODO : dictionary translating glpi status into inventory status


def createOrUpdateComputer(computer):
    (name, status, serial) = translateData(computer)
    
    roomName = unicode(str(computer[3]), 'ISO-8859-1').encode('utf-8')
    room = Room.getOrCreateByName(roomName)
    roomId = room.id
    
    computer = Computer.query().filter('name', name).getFirst()
    
    if computer is None:        
        computer = Computer(name=name, inventoryStatus=status, room=roomId, serial=serial)
        computer._resetLocator()
        computer.create()
        log.write('computer has been created in db \'%s\'' % str(computer))
    else:
        computer.inventoryStatus = status
        computer.serial = serial
        computer.room = long(roomId)
        computer._resetLocator()
        computer.update()
        log.write('computer \'%s\' has been updated' %  str(computer.name))

def createOrUpdateScreen(screen):
    (name, status, serial) = translateData(screen)
    
    screen = Screen.query().filter('name', name).getFirst()
    
    if screen is None:        
        screen = Screen(name=name, inventoryStatus=status, serial=serial)
        screen._resetLocator()
        screen.create()
        log.write('screen has been created in db \'%s\'' % str(screen))
    else:
        screen.inventoryStatus = status
        screen.serial = serial
        screen._resetLocator()
        screen.update()
        log.write('screen %s has been updated' %  str(screen.name))

def createOrUpdateLink(link):
    computerName = link[0]
    screenName   = link[1]
    print 'computer',computerName,'and screen',screenName
    
    computer = Computer.query().filter('name', computerName).getFirst()
    if computer is None:     
        log.write('Error : link impossible between computer %s and screen %s -- COMPUTER NOT FOUND' %  (str(computerName), str(screenName)))       
        return
    
    screen = Screen.query().filter('name', screenName).getFirst()
    if screen is None:        
        log.write('Error : link impossible between computer %s and screen %s -- SCREEN NOT FOUND' %  (str(computerName), str(screenName)))       
        return
    
    computer.addScreen(screen.id)
    computer._resetLocator()
    computer.update()    
    
    screen.addComputer(computer.id)
    screen._resetLocator()
    screen.update()    
        
    log.write('link between computer %s and screen %s' %  (str(computer.name), str(screen.name)))    

def translateData(ressource):
    name = ressource[0]
    statusCode = ressource[1]
    serial = ressource[2]
    
    if name is None:
        name = ''
    name = unicode(name, 'ISO-8859-1').encode('utf-8')
    
    if serial is None:
        serial = ''
    serial = unicode(serial, 'ISO-8859-1').encode('utf-8')
    
    if statusCode in STATUS.keys() and STATUS[statusCode] is not None:
        status = STATUS[statusCode].encode('utf-8')
    else:
        raise ValueError('Unknown status for this ressource')

    return (name, status, serial)

def doIt(args, log):
    
       # DATABASE
       try:
           mgaDB = MySQLdb.connect(host=GLPI_HOST, user=GLPI_USER, passwd=GLPI_PASS)
           mgaDB.select_db(GLPI_DATABASE)
       except Exception, e:
           print str(e.message)
           print 'Unable to connect to server IT.'
           sys.exit(1)
           
       # RECUPERATION DES MACHINES
       cmd =  'SELECT ' + GLPI_COMPUTER_TABLE + '.NAME, ' + GLPI_COMPUTER_TABLE + '.STATE, ' + GLPI_COMPUTER_TABLE + '.SERIAL, ' + GLPI_ROOM_TABLE + '.NAME'
       cmd += ' FROM ' + GLPI_COMPUTER_TABLE + ', ' + GLPI_ROOM_TABLE 
       cmd += ' WHERE ' + GLPI_COMPUTER_TABLE + '.NAME IS NOT NULL'
       cmd += ' AND ' + GLPI_COMPUTER_TABLE + '.LOCATION = ' + GLPI_ROOM_TABLE +'.ID'
       cmd += ';'
       print cmd
       c = mgaDB.cursor()
       r = c.execute(cmd)
       if r:
          computers = c.fetchall()
          for computer in computers:
              createOrUpdateComputer(computer) 
       log.write('computers updated')   
               
       # RECUPERATION DES ECRANS
       cmd = 'SELECT NAME, STATE, SERIAL FROM ' + GLPI_SCREEN_TABLE + ' WHERE NAME IS NOT NULL;'
       
       c = mgaDB.cursor()
       r = c.execute(cmd)
       if r:
          screens = c.fetchall()
          for screen in screens:
              createOrUpdateScreen(screen)    
       log.write('screens updated')   
           
       # RECUPERATION DES LIENS ECRANS/MACHINE
       cmd =  'SELECT ' + GLPI_COMPUTER_TABLE + '.NAME, ' + GLPI_SCREEN_TABLE + '.NAME'
       cmd += ' FROM ' + GLPI_LINK_TABLE + ', ' + GLPI_COMPUTER_TABLE + ', ' + GLPI_SCREEN_TABLE
       cmd += ' WHERE ' + GLPI_LINK_TABLE + '.TYPE=\'4\''
       cmd += ' AND ' + GLPI_SCREEN_TABLE + '.ID = ' + GLPI_LINK_TABLE +'.END1'
       cmd += ' AND ' + GLPI_COMPUTER_TABLE + '.ID = ' + GLPI_LINK_TABLE +'.END2'
       cmd += ' AND ' + GLPI_SCREEN_TABLE + '.NAME IS NOT NULL'
       cmd += ';'
       c = mgaDB.cursor()
       r = c.execute(cmd)
       if r:
          links = c.fetchall()
          for link in links:
              createOrUpdateLink(link)    
       log.write('links computer/screen updated')   
               
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
        
    log = None
    
    try:
        res = os.popen( 'pgrep -f updateDataFromGLPI.py' ).read()
        output  = res.split("\n")
            
        if len(output)>3:
            log.write('Process already in progress!')
            raise ValueError('Process already in progress!')

        logLocation = conf.dataServerLogPath
        if not os.path.exists(logLocation):
            raise ValueError('Log directory does not exist.')
            
        log = Log(str(os.path.basename(__file__)), logLocation)
        log.enableConsole()
        log.start()
        
        log.write('Arguments are : ' + str(sys.argv))
    
        args = {}
            
        log.write('Before I process anything.')
        doIt(args, log)
        log.write('After I\'ve processed everything.')
        
    except Exception, e:
        if log is not None:
            log.write('Error: ' + str(e.message) + ' '  + str(type(e)))
        else:
            print 'Error:', e.message, type(e)
    finally:
        if log is not None:
            log.stop()
