# coding=utf-8

from hd3dlib.asset import AssetRevision, AssetGroup
from hd3dlib.baseobject import BaseObject
from hd3dlib.production import Category, Constituent, Sequence, Shot
from hd3dlib.production.step import Step
from hd3dlib.util.classbuild import makeClass, Attribute
from string import Template

TAG_DEFINITION = { 
                  'name':'TagGenerated',
                  'attributes':{
                                '_class':Attribute('class',
                                                   str,
                                                   default='fr.hd3d.model.lightweight.impl.LTag'),
                                'name' : Attribute('name', str),
                                'parents' : Attribute('parents', list),
                                'userCanUpdate' : Attribute('userCanUpdate', bool, default=True),
                                'userCanDelete' : Attribute('userCanDelete', bool, default=True),
                                
                                
                                }
                  }

class Tag( makeClass( TAG_DEFINITION, BaseObject ) ):
    '''
    A tag is used to define more precisely an work object, like a shot or a constituent.
    It can be created directly by users, inside the application, and also be removed or updated.
    Users can assign tags to a particular work object, by dragging them into a "tag basket"
    '''
    
    _simpleclassname='Tag'
    _locatorTmpl = Template(  '/tags/' )
    _locatorShort = '/tags/'
    _locator = '/tags/'
    
    def __init__(self, **kw):
        BaseObject.__init__( self, **kw )
        
        if len( kw ) is not 0:
            if self.name is None or self.name == '': 
                self.name = "Tag " + self._jicId
                
    def _resetLocator(self):
        
        self._locator = self._locatorShort
    
    @classmethod
    def getByName(cls, name):
        tag = Tag.query().filter( 'name', name ).getFirst()
        if tag is None:
            return None
        else:
            return tag
        
