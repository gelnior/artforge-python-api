# coding=utf-8

from hd3dlib.util.httprequests import HttpRequestHandler, HttpRequestTrace
from hd3dlib import conf
from hd3dlib.asset import FileRevision
from hd3dlib.production import Project 

global user
TEST = False


def doIt(args, log):
    
    #try:
        from hd3dlib.util.odspreadsheet import ODSpreadsheet
        from hd3dlib.production import Breakdown
        
        project =  Project().get(args['projectId'])
        log.write('Processing for project \'%s\'' % (project.name))
        
        odsPath =  args['odsPath']
        log.write('Import from file \'%s\'' % odsPath)
        
        if not TEST:
            #Retrieved ods path -> file revision in DB -> ods server path
            odsName = os.path.basename(odsPath)
            log.write('File search name \'%s\'' % odsName)
            f = FileRevision().query().filter('key', odsName).getLast()
            #odsServerPath = f.location + '/' + f.relativePath
            odsServerPath = os.path.join(f.location , f.relativePath)
            
            log.write('File path on server: %s' % odsServerPath)
            log.write('Last operation on revision : \'%s\'' % f.lastOperationDate)
        else:
            odsServerPath = odsPath
        
        b = Breakdown()
        b.setODS(odsServerPath)
        b.setProjectDBId(project.id)
        b.processCategories()
        b.processConstituents()
        b.processSequences()
        b.processShots()
        
        log.write('Number of categories read : %s' % (b.getNumberOfReadItems('Categories')))
        log.write('Number of sequences read : %s' % (b.getNumberOfReadItems('Sequences')))
        log.write('Number of constituents read : %s' % (b.getNumberOfReadItems('Constituents')))
        log.write('Number of shots read : %s' % (b.getNumberOfReadItems('Shots')))
        log.write('Number of compositions read : %s' % (b.getNumberOfReadItems('Compositions')))
        b.createInDatabase()
        
    
    #except Exception, e:
    #    log.write('Error: ' + str(e.message))
    #finally:
    #    log.stop()
    
    
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
    
    logLocation = conf.dataServerLogPath
    if not os.path.exists(logLocation):
        sys.exit('Log directory does not exist.')
        
    log = Log(str(os.path.basename(__file__)), logLocation)
    log.enableConsole()
    log.start()
    
    argList = sys.argv[1:]
    if len(argList) > 0:
        msg = "Read arguments are :" + str(argList)
    else:
        msg = "No argument passed" 
    log.write(msg)

    if not TEST:
        args = eval(eval(sys.argv[1]))
    else:
        p = Project.query().filter('name','Project ODS').getFirst()
        odsPath = '/home/breakdown_2mn_Raymond.ods'
        args = {'projectId' : p.id, 'odsPath' : odsPath}
    
    if not isinstance(args, dict):
        sys.exit('Malformed arguments dictionnary')
    
    doIt(args, log)
    
