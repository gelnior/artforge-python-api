# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

APPROVAL_NOTE_TYPE_DEFINITION = {
    'name':'ApprovalNoteTypeGenerated',
    'attributes':{
        '_class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LApprovalNoteType', 
            mutable=False
        ),
        'name':Attribute('name', str),
        'taskType':Attribute('taskType', long),
        'project':Attribute('project', long)
    }
}

class ApprovalNoteType(makeClass(APPROVAL_NOTE_TYPE_DEFINITION, BaseObject)):
    '''
    Approval note type is linked to a task type. Approval type is here to 
    make difference between approvers : directors, producver...
    '''
    
    _locatorTmpl = Template('/approval-note-types/')
    _locatorShort = '/approval-note-types/'
    _locator = '/approval-note-types/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        
    @classmethod
    def getByName(cls, name, projectId):
        '''
        Return approval note type of which name is equal to name.
        
        Arguments:
            *name* Name of the approval note type to retrieved.
            *projectId* the project id to the approval note type to retrieved.
        '''
        
        return ApprovalNoteType.query().filter('name', name).filter('project', projectId).getFirst()
    
    @classmethod
    def getOrCreate(cls, name, projectId, taskType=None):
        '''
        Return approval note type of which name is equal to name. If approval note type does not 
        exist it is is created. 
        
        Arguments:
            *name* Name of the approval note type to retrieved.
            *projectId* the project id to the approval note type to retrieved.
            *taskType* the task type to the approval note type to retrived.

        '''        
        approvalNoteType = ApprovalNoteType.getByName(name,projectId)
    
        if not approvalNoteType:  
            approvalNoteType=ApprovalNoteType()      
            approvalNoteType.name=name
            if taskType : 
                approvalNoteType.taskType=taskType
            approvalNoteType.project=projectId
            approvalNoteType.create()
        
        return approvalNoteType
    