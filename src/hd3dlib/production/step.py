# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

STEP_DEFINITION = {
    'name':'StepGenerated',
    'attributes':{
        '_class' : Attribute(
             'class', 
             str, 
             default='fr.hd3d.model.lightweight.impl.LStep'
        ),
        'boundEntity' : Attribute('boundEntity', long),
        'boundEntityName' : Attribute('boundEntityName', str),
        'taskType' : Attribute('taskType', long),
        'taskTypeName' : Attribute('taskTypeName', str),
        'createTasknAsset' : Attribute('createTasknAsset', bool, default=False),
        'estimatedDuration' : Attribute('estimatedDuration', int, default=0)
     }
}

class Step(makeClass(STEP_DEFINITION, BaseObject)):
    '''
    Step object is an indicator to tell if task and asset should be created
    for a work object. It also allows to set fast estimation.
    
    Step is linked to a work object and a task type. If its duration is
    superior to zero, when it is saved, a task with a duration equal to the one
    set will be created. Corresponding asset is created too.
    '''
    
    _locatorTmpl = Template('/steps/')
    _locatorShort = '/steps/'
    _locator = '/steps/'
            
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        
    def setConstituent(self, constituent):
        '''
        Set as work object the constituent given in parameter.
         
        Arguments :
            *constituent* The work object to set on step.
        '''
        self.boundEntity = constituent.id
        self.boundEntityName = "Constituent"

    def setShot(self, shot):
        '''
        Set as work object the shot given in parameter.
         
        Arguments :
            *shot* The work object to set on step.
        '''
        self.boundEntity = shot.id
        self.boundEntityName = "Shot"

    def setTaskType(self, taskType):
        '''
        Set taskType as step task type.
        
        Arguments :
            *taskType* The task type to set on step.
        '''
        self.taskType = taskType.id

    def setDuration(self, duration):
        '''
        Set duration as duration (and automatically activates the create task
        and asset functionality).
        
        Arguments :
            *duration* Number of days to set as duration.
        '''
        self.createTasknAsset = True
        self.estimatedDuration = duration
        
    @classmethod
    def getOrCreate(cls, taskType, workObject, boundEntityName, duration=28800):
        '''
        Return the step corresponding to the given task type, work object and bound 
        entity name. It creates the step if the step does not already exist.
        
        Arguments:
            *taskType* The task type whose step is retrieved.
            *workObject* The work object whose step is retrieved.
            *boundEntityName* the bound entity of workObject.
            *duration* the duration of the step for creation.
        '''
        step=Step.getByWorkObject(taskType,workObject,boundEntityName)
        
        if step is None:
            step = Step()
            step.taskType = taskType.id
            step.boundEntity = workObject.id
            step.boundEntityName =boundEntityName
            step.estimatedDuration=duration
            step.create()
           
        return step
    
    @classmethod
    def getByWorkObject(cls, taskType, workObject, boundEntityName):
        '''
        Return the step corresponding the task type, work object and bound entity name..
        
        Arguments:
            *taskType* The task type whose step is retrieved.
            *workObject* The work object whose step is retrieved.
            *boundEntityName* the bound entity of workObject.
        '''
        return Step.query().filter('taskType', taskType.id).filter('boundEntity', workObject.id).filter('boundEntityName', boundEntityName).getFirst()