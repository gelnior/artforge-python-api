# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


COMPUTERMODEL_DEFINITION = {
    'name':'ComputerModelGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LComputerModel', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
        
        "manufacturer" : Attribute('manufacturer', long),
        "manufacturerName" : Attribute('manufacturerName', str),
    }
}

class ComputerModel(makeClass(COMPUTERMODEL_DEFINITION, BaseObject)):
    '''
    ComputerModel corresponds to a computer model like : DELL Lattitude E6400.
    With this class all 
    '''

    _locatorTmpl = Template('/computer-models/')
    _locatorShort = '/computer-models/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()

    @classmethod
    def getByName(cls, name):
        '''
        Retrieve the computer model whose the name is equal to name. If the computer model
        does not exist, it returns null.
        
        Arguments:
            *name* The name used for filtering.
        '''
        return ComputerModel.query().filter('name', name).getFirst()
            
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Retrieve the model whose the name is equal to name. If the model
        does not exist, it create it.
        
        Arguments:
            *name* The name used for filtering.
        '''
        model = ComputerModel.getByName(name)
        
        if model is None:
            model = ComputerModel()
            model.name = name
            model.create()
            
        return model
