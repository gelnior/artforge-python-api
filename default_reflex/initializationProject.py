# coding=utf-8

'''
This script creates sheets for constituents, shots and tasks for a given project.
'''

from hd3dlib import conf
from hd3dlib.util.httprequests import HttpRequestHandler, HttpRequestTrace
from hd3dlib.reflex import BaseReflex
from hd3dlib.extra import Sheet, Item, ItemGroup

import sys

global user
TEST = False

# type constants
ATTRIBUTE = 'attribute'
METHOD = 'method'
COLLECTIONQUERY = 'collectionQuery'

# metaType constants
JAVA_LANG_STRING = 'java.lang.String'
JAVA_LANG_BOOLEAN = 'java.lang.Boolean'
JAVA_LANG_LONG = 'java.lang.Long'
JAVA_LANG_DATE = 'java.util.Date'
JAVA_LANG_INTEGER = 'java.lang.Integer'
JAVA_UTIL_LIST = 'java.util.List'

# renderer constants
SHORT_TEXT = 'short-text'
BOOLEAN = 'boolean'
DATE = 'date'
STARTENDDATE = 'startEndDate'
SEQUENCE = 'Sequence'
INT = 'int'

class ProjectInitializer(BaseReflex):

    def __init__(self):
        import os, os.path
        scriptPath = os.path.abspath(__file__)
        BaseReflex.__init__(self, scriptPath)

    def createTaskSheet(self, projectId):
        sheetName = 'Task'
        boundClassName = 'Task'
        Sheet._locator = 'projects/%d/sheets/' % projectId

        #create sheet
        sheet = Sheet(name = sheetName,
                  description = 'Standard task sheet',
                  project = projectId,
                  boundClassName = boundClassName)
        sheet.create()
        self.log.write('New sheet created : %s' % sheet.name)

        hookRoot = '_'.join([str(projectId), boundClassName, sheetName])

        #create main group and items
        igDefault = ItemGroup(name = 'Principal',
                            hook = str('_'.join([hookRoot, 'Principal'])),
                            sheet = sheet.id)
        igDefault._resetLocator()
        igDefault.create()

        self.log.write('Group : %s' % igDefault.name)

        item = Item(sheet.id,
                               name = 'Work Object Thumbnail',
                               query = 'fr.hd3d.services.resources.collectionquery.TaskBoundEntityThumbnailCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_LANG_STRING,
                               renderer = 'thumbnail',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Work Object Path',
                               query = 'fr.hd3d.services.resources.collectionquery.TaskBoundEntityPathCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = 'java.lang.String',
                               renderer = '',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Work Object',
                               query = 'fr.hd3d.services.resources.collectionquery.TaskBoundEntityCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = 'IBase',
                               renderer = 'work-object',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Task Type',
                               query = 'taskType',
                               type = ATTRIBUTE,
                               metaType = 'TaskType',
                               renderer = 'task-type',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Status',
                               query = 'status',
                               type = ATTRIBUTE,
                               metaType = 'fr.hd3d.common.client.enums.ETaskStatus',
                               renderer = 'task-status',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Notes',
                               query = 'fr.hd3d.services.resources.collectionquery.TaskBoundEntityApprovalNotesCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_UTIL_LIST,
                               renderer = 'approval',
                               editor = 'approval',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Worker',
                               query = 'worker',
                               type = ATTRIBUTE,
                               metaType = 'Person',
                               renderer = 'person-task',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Estimated Duration',
                               query = 'duration',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_LONG,
                               renderer = 'duration',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Duration',
                               query = 'fr.hd3d.services.resources.collectionquery.TaskActivitiesDurationCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_LANG_LONG,
                               renderer = 'activity-duration',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Start date',
                               query = 'startDate',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_DATE,
                               renderer = 'date-and-day',
                               editor = DATE,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'End date',
                               query = 'endDate',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_DATE,
                               renderer = 'date-and-day',
                               editor = DATE,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Actual start date',
                               query = 'actualStartDate',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_DATE,
                               renderer = 'date-and-day',
                               editor = DATE,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Actual End date',
                               query = 'actualEndDate',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_DATE,
                               renderer = 'date-and-day',
                               editor = DATE,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        self.log.write('Sheet creation finished : %s' % sheetName)


    def createShotSheet(self, projectId):
        sheetName = 'Shot'
        boundClassName = 'Shot'
        Sheet._locator = 'projects/%d/sheets/' % projectId

        # Create sheet for breakdown
        sheet = Sheet(name = sheetName,
                  description = 'Standard shot sheet',
                  project = projectId,
                  boundClassName = boundClassName,
                  type = 'BREAKDOWN').create()
        self.log.write('New sheet created : %s' % sheet.name)

        hookRoot = '_'.join([str(projectId), boundClassName, sheetName])

        # Create main group and items
        igDefault = ItemGroup(name = 'Principal',
                            hook = str('_'.join([hookRoot, 'Principal'])),
                            sheet = sheet.id)
        igDefault._resetLocator()
        igDefault.create()

        self.log.write('Group : %s' % igDefault.name)

        item = Item(sheet.id,
                               name = 'Thumbnail',
                               query = 'thumbnail',
                               type = METHOD,
                               metaType = JAVA_LANG_STRING,
                               renderer = 'thumbnail',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        itSequence = Item(sheet.id,
                               name = 'Path',
                               query = 'fr.hd3d.services.resources.collectionquery.PathCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_LANG_STRING,
                               renderer = '',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itSequence.name)

        item = Item(sheet.id,
                               name = 'Label',
                               query = 'label',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_STRING,
                               renderer = '',
                               editor = SHORT_TEXT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Description',
                               query = 'description',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_STRING,
                               editor = SHORT_TEXT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        item = Item(sheet.id,
                               name = 'Nb Frame',
                               query = 'nbFrame',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_INTEGER,
                               renderer = INT,
                               editor = INT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        #create 'informations' group and items
        igInfo = ItemGroup(name = 'Steps',
                            hook = str('_'.join([hookRoot, 'Steps'])),
                            sheet = sheet.id)
        igInfo._resetLocator()
        igInfo.create()
        self.log.write('Group : %s' % igInfo.name)



        self.log.write('Sheet creation finished : %s' % sheetName)

        # Standard sheet for production : just label and thumbnail because 
        # validation column depends on project.
        sheet = Sheet(name = sheetName + ' validation',
                  description = 'Standard shot sheet for validation',
                  project = projectId,
                  boundClassName = boundClassName,
                  type = 'PRODUCTION').create()
        self.log.write('New sheet created : %s' % sheet.name)

        hookRoot = '_'.join([str(projectId), boundClassName, sheetName])

        # Create main group and items
        igDefault = ItemGroup(name = 'Principal',
                            hook = str('_'.join([hookRoot, 'Principal'])),
                            sheet = sheet.id)
        igDefault._resetLocator()
        igDefault.create()

        self.log.write('Group : %s' % igDefault.name)

        item = Item(sheet.id,
                               name = 'Thumbnail',
                               query = 'thumbnail',
                               type = METHOD,
                               metaType = JAVA_LANG_STRING,
                               renderer = 'thumbnail',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        itSequence = Item(sheet.id,
                               name = 'Path',
                               query = 'fr.hd3d.services.resources.collectionquery.PathCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_LANG_STRING,
                               renderer = '',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itSequence.name)

        item = Item(sheet.id,
                               name = 'Label',
                               query = 'label',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_STRING,
                               renderer = '',
                               editor = SHORT_TEXT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)


        item = Item(sheet.id,
                               name = 'Nb Tasks',
                               query = 'fr.hd3d.services.resources.collectionquery.TotalNbOfTasksCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_LANG_LONG,
                               renderer = INT,
                               editor = INT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)

        igValidation = ItemGroup(name = 'Validation',
                            hook = str('_'.join([hookRoot, 'Validation'])),
                            sheet = sheet.id)
        igValidation._resetLocator()
        igValidation.create()

        self.log.write('Sheet creation finished : %s' % sheetName)



    def createConstituentSheet(self, projectId):

        sheetName = 'Constituent'
        boundClassName = 'Constituent'
        Sheet._locator = 'projects/%d/sheets/' % projectId

        # Create sheet for breakdown
        constituentSheet = Sheet(name = sheetName,
                  description = 'Standard constituent sheet',
                  project = projectId,
                  boundClassName = boundClassName,
                  type = 'BREAKDOWN').create()
        self.log.write('New sheet created : %s' % constituentSheet.name)

        hookRoot = '_'.join([str(projectId), boundClassName, sheetName])

        # Create main group and items
        igDefault = ItemGroup(name = 'Principal',
                            hook = str('_'.join([hookRoot, 'Principal'])),
                            sheet = constituentSheet.id)
        igDefault._resetLocator()
        igDefault.create()

        self.log.write('Group : %s' % igDefault.name)

        itThumbnail = Item(constituentSheet.id,
                               name = 'Thumbnail',
                               query = 'thumbnail',
                               type = METHOD,
                               metaType = JAVA_LANG_STRING,
                               renderer = 'thumbnail',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itThumbnail.name)

        itCategory = Item(constituentSheet.id,
                               name = 'Path',
                               query = 'fr.hd3d.services.resources.collectionquery.PathCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_LANG_STRING,
                               renderer = '',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itCategory.name)

        itLabel = Item(constituentSheet.id,
                               name = 'Label',
                               query = 'label',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_STRING,
                               renderer = '',
                               editor = SHORT_TEXT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itLabel.name)

        itDescription = Item(constituentSheet.id,
                               name = 'Description',
                               query = 'description',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_STRING,
                               editor = SHORT_TEXT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itDescription.name)


        #create 'informations' group and items
        igInfo = ItemGroup(name = 'Steps',
                            hook = str('_'.join([hookRoot, 'Steps'])),
                            sheet = constituentSheet.id)
        igInfo._resetLocator()
        igInfo.create()
        self.log.write('Group : %s' % igInfo.name)


        self.log.write('Sheet creation finished : %s' % sheetName)

        # Standard sheet for production : just label and thumbnail because 
        # validation column depends on project.
        constituentSheet = Sheet(name = sheetName + ' validation',
                  description = 'Standard constituent validation sheet',
                  project = projectId,
                  boundClassName = boundClassName,
                  type = 'PRODUCTION').create()
        self.log.write('New sheet created : %s' % constituentSheet.name)

        hookRoot = '_'.join([str(projectId), boundClassName, sheetName])

        #create main group and items
        igDefault = ItemGroup(name = 'Principal',
                            hook = str('_'.join([hookRoot, 'Principal'])),
                            sheet = constituentSheet.id)
        igDefault._resetLocator()
        igDefault.create()

        self.log.write('Group : %s' % igDefault.name)

        itThumbnail = Item(constituentSheet.id,
                               name = 'Thumbnail',
                               query = 'thumbnail',
                               type = METHOD,
                               metaType = JAVA_LANG_STRING,
                               renderer = 'thumbnail',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itThumbnail.name)

        itCategory = Item(constituentSheet.id,
                               name = 'Path',
                               query = 'fr.hd3d.services.resources.collectionquery.PathCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_LANG_STRING,
                               renderer = '',
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itCategory.name)

        itLabel = Item(constituentSheet.id,
                               name = 'Label',
                               query = 'label',
                               type = ATTRIBUTE,
                               metaType = JAVA_LANG_STRING,
                               renderer = '',
                               editor = SHORT_TEXT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % itLabel.name)

        item = Item(constituentSheet.id,
                               name = 'Nb Tasks',
                               query = 'fr.hd3d.services.resources.collectionquery.TotalNbOfTasksCollectionQuery',
                               type = COLLECTIONQUERY,
                               metaType = JAVA_LANG_LONG,
                               renderer = INT,
                               editor = INT,
                               itemGroup = igDefault.id).create()
        self.log.write('    Item : %s' % item.name)


        igValidation = ItemGroup(name = 'Validation',
                            hook = str('_'.join([hookRoot, 'Validation'])),
                            sheet = constituentSheet.id)
        igValidation._resetLocator()
        igValidation.create()

        self.log.write('Group : %s' % igValidation.name)

        self.log.write('Sheet creation finished : %s' % sheetName)

    def createDefaultDynamicPath(self, projectId):
        from hd3dlib.asset import DynamicPath
        from hd3dlib.production import Sequence, Category

        self.log.write('Create default Dynamic path for category library')
        dynamicPath = DynamicPath()
        dynamicPath.name = "Default library path"
        dynamicPath.projectId = projectId
        dynamicPath.taskTypeId = -1
        dynamicPath.workObjectId = -1
        dynamicPath.boundEntityName = Category._simpleclassname
        dynamicPath.type = DynamicPath.type_file
        dynamicPath.wipDynamicPath = "${project}/${category}/${constituent}/${tasktype}/wip"
        dynamicPath.publishDynamicPath = "${project}/${category}/${constituent}/${tasktype}/publish"
        dynamicPath.oldDynamicPath = "${project}/${category}/${constituent}/${tasktype}/old"
        dynamicPath.create()
        self.log.write('Default Dynamic path for category library created')

        self.log.write('Create default Dynamic path for sequence Sequences')
        dynamicPath = DynamicPath()
        dynamicPath.name = "Default sequence path"
        dynamicPath.projectId = projectId
        dynamicPath.taskTypeId = -1
        dynamicPath.workObjectId = -1
        dynamicPath.boundEntityName = Sequence._simpleclassname
        dynamicPath.type = DynamicPath.type_file
        dynamicPath.wipDynamicPath = "${project}/${sequence}/${shot}/${tasktype}/wip"
        dynamicPath.publishDynamicPath = "${project}/${sequence}/${shot}/${tasktype}/publish"
        dynamicPath.oldDynamicPath = "${project}/${sequence}/${shot}/${tasktype}/old"
        dynamicPath.create()
        self.log.write('Default Dynamic path for sequence Sequences created')



    def doIt(self):

        try:

            for entityId in self.entityIds:
                self.log.write('Processing for project \'%d\'' % (entityId))

                #self.createConstituentSheet(entityId)
                #self.createShotSheet(entityId)
                self.createTaskSheet(entityId)

                self.createDefaultDynamicPath(entityId)

        except Exception, e:
             self.log.write('Error: ' + str(e.message))
        finally:
             self.log.stop()


if __name__ == '__main__':

    initializer = ProjectInitializer()

    if TEST:
        p = Project.query().filter('name', 'Project ODS').getFirst()
        initializer.entityId = p.id

    initializer.doIt()

