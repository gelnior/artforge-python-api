# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


SCREENMODEL_DEFINITION = {
    'name':'ScreenModelGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LScreenModel', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
        
        "manufacturer" : Attribute('manufacturer', long),
        "manufacturerName" : Attribute('manufacturerName', str),
    }
}

class ScreenModel(makeClass(SCREENMODEL_DEFINITION, BaseObject)):
    '''
    ScreenModel corresponds to a screen model like : DTZ2100D.
    With this class all 
    '''

    _locatorTmpl = Template('/screen-models/')
    _locatorShort = '/screen-models/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()

    @classmethod
    def getByName(cls, name):
        '''
        Retrieve the computer model whose the name is equal to name. If the computer model
        does not exist, it returns null.
        
        Arguments:
            *name* The name used for filtering.
        '''
        return ScreenModel.query().filter('name', name).getFirst()
            
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Retrieve the model whose the name is equal to name. If the model
        does not exist, it create it.
        
        Arguments:
            *name* The name used for filtering.
        '''
        model = ScreenModel.getByName(name)
        
        if model is None:
            model = ScreenModel()
            model.name = name
            model.create()
            
        return model
