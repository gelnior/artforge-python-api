# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

FILEATTRIBUTE_DEFINITION = {
    'name':'FileAttributeGenerated',
        'attributes':{'_class': Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LFileAttribute', 
            mutable=False
        ),
        'file':Attribute('file', long),
        'name':Attribute('name', str),
        'value':Attribute('value',str),
    }
}

class FileAttribute(makeClass(FILEATTRIBUTE_DEFINITION, BaseObject)):
    '''
    File attribute is a dynamic attribute dedicated to a file revision object.
    It is faster than usual dynamic attributes.
    '''
    _locatorTmpl = Template('/file-attributes/')
    _locatorShort = '/file-attributes/'
    _locator = ''
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
    