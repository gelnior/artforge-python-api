# coding=utf-8

from string import Template

from hd3dlib.baseobject import BaseObject
from hd3dlib.util.classbuild import makeClass, Attribute

FILEREVISIONLINK_DEFINITION = {
    'name':'FileRevisionLinkDefinition',
    'attributes':{
        '_class':Attribute(
           'class', 
           str, 
           default='fr.hd3d.model.lightweight.impl.LFileRevisionLink', 
           mutable=False
        ),
        'type':Attribute('type', str),
        'inputFile':Attribute('inputFile', long),
        'outputFile':Attribute('outputFile', long),
    }
}

class FileRevisionLink(makeClass(FILEREVISIONLINK_DEFINITION, BaseObject)):
    '''
    FileRevisionLink let you build links between files. A file can depend
    from one other like when a Maya scene has reference on modeled objects. 
    '''

    _locatorTmpl = Template('/file-revision-links/') 
    _locatorShort = '/file-revision-links/'
    _locator = '/file-revision-links/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self,**kw)
        
        self._resetLocator()

    @classmethod
    def getByFiles(cls, inputFile, outputFile):
        '''
        Return link between inputFile and outputFile.
        
        Arguments:
           inputFile The file of which outputFile depends.
           outputFile The file which depends of inputFile.
        '''
        query = cls.query().filter('inputFileRevision.id', inputFile.id)
        return query.filter('outputFileRevision.id', outputFile.id).getFirst()

    @classmethod
    def getOrCreateByFiles(cls, inputFile, outputFile):
        '''
        Return link between inputFile and outputFile. If link does not exist
        it is created.
        
        Arguments:
           inputFile The file of which outputFile depends.
           outputFile The file which depends of inputFile.
        '''   
        obj = cls.getByFiles(inputFile, outputFile)
    
        if obj is None:        
            obj = FileRevisionLink(inputFile = inputFile.id,
                                   outputFile = outputFile.id)
            obj.create()
        
        return obj
    
    @classmethod
    def query(cls):
        return BaseObject.queryClass(cls)  
        