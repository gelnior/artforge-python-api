# coding=utf-8

from hd3dlib.baseobject import BaseObject
from string import Template
from hd3dlib.util.classbuild import makeClass, Attribute

ITEMGROUP_DEFINITION = {
    'name':'ItemGroupGenerated',
    'attributes':{
        '_class':Attribute('class', 
                           str, 
                           default='fr.hd3d.model.lightweight.impl.LItemGroup', 
                           mutable=False
                           ),
        'name':Attribute('name', str),
        'hook':Attribute('hook', str),
        'sheet':Attribute('sheet', long),
        'items':Attribute('items', list)
    }
}

class ItemGroup(makeClass(ITEMGROUP_DEFINITION, BaseObject)):
    '''
    Sheet contains item groups that contains items. Item group act as folder
    (or column grouper) for items.
    '''
    
    _locatorTmpl = Template('/sheets/$sheets/item-groups/')
    _locatorShort = '/sheets/0/item-groups/'
    _locator = ''
    
    def __init__(self, **kw):
        BaseObject.__init__(self,**kw)

    def _resetLocator(self):
        self._locatorTmpl = Template(ItemGroup._locatorTmpl.template)
        self._locator = self._locatorTmpl.safe_substitute(sheets=self.sheet)
        
                 
    @classmethod
    def query(cls):
        '''
        Return query object for item group.
        '''
        return BaseObject.queryClass(cls) 
    
    @classmethod
    def getOrCreate(cls,name,sheetId):
        '''
        Return the item group of which name is equal to name and the sheetId is equal to the sheet id. 
        It creates it if the sheet does not already exist.
        
        Arguments:
            *name* item group name.
            *sheetId* the sheet id of the item group
        '''
        itemGroup=ItemGroup.getByName(name,sheetId)
        
        if itemGroup is None:
            itemGroup = ItemGroup()
            itemGroup.name = name
            itemGroup.sheet = sheetId
            itemGroup.hook = str('_'.join(["%d" % sheetId,name]))
            itemGroup._resetLocator()
            itemGroup.create()
        
        itemGroup._resetLocator()
        return itemGroup
    
    @classmethod
    def getByName(cls, name, sheetId):
        '''
        Return the item group with the given  name and the given sheet id.
        
        Arguments:
            *name* Name of item group to retrieve
            *sheetId* the sheet id to retrieve
        '''   
        return ItemGroup.query().branch("sheets",sheetId).filter('name', name).getFirst()
    
    @classmethod
    def getBySheet(cls,sheetId):
        '''
        Return item groups from sheet of which id is equals to *sheetId*.
        
        Arguments:
            *sheetId* Id of sheet of which groups are requested.
        '''   
        return ItemGroup.query().branch("sheets", sheetId).getAll()
