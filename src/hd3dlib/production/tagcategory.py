# coding=utf-8

from hd3dlib.asset import AssetRevision, AssetGroup
from hd3dlib.baseobject import BaseObject
from hd3dlib.production import Category, Constituent, Sequence, Shot
from hd3dlib.production.tag import Tag
from hd3dlib.production.step import Step
from hd3dlib.util.classbuild import makeClass, Attribute
from string import Template

TAGCATEGORY_DEFINITION = { 
                  'name':'TagCategory Generated',
                  'attributes':{
                                '_class':Attribute('class',
                                                   str,
                                                   default='fr.hd3d.model.lightweight.impl.LTagCategory'),
                                'name' : Attribute('name', str),
                                'parent' : Attribute('parent', long, default=long(0)),
                                'children' : Attribute('children', list),
                                'boundTags' : Attribute('boundTags', list),
                                'userCanUpdate' : Attribute('userCanUpdate', bool, default=True),
                                'userCanDelete' : Attribute('userCanDelete', bool, default=True),
                                
                                
                                }
                  }

class TagCategory( makeClass( TAGCATEGORY_DEFINITION, BaseObject ) ):
    '''
    A tag is used to define more precisely an work object, like a shot or a constituent.
    It can be created directly by users, inside the application, and also be removed or updated.
    Users can assign tags to a particular work object, by dragging them into a "tag basket"
    '''
    
    _simpleclassname='TagCategory'
    _locatorTmpl = Template(  '/tag-categories/' )
    _locatorShort = '/tag-categories/'
    _locator = '/tag-categories/'
    
    def __init__(self, **kw):
        BaseObject.__init__( self, **kw )
        
        if len( kw ) is not 0:
            if self.name is None or self.name == '': 
                self.name = "TagCategory " + self._jicId
                
    def _resetLocator(self):
        
        self._locator = self._locatorShort
        
    def deleteAll(self):
        obj = TagCategory.get( self.id )
        if not len(obj.boundTags) == 0: 
            for boundTag in obj.boundTags:
                obj.boundTags.remove( boundTag )
                tag = Tag.get( boundTag )
                tag.delete()
        if len(obj.children) == 0:
            return
        else: 
            for child in obj.children:
                tagCategory = TagCategory.get( child )
                tagCategory.deleteAll()
                tagCategory.delete()
        return
    
    @classmethod
    def getByName(cls, name):
        for tagCateg in TagCategory.query().getAll():
            if tagCateg.name.lower() == name.lower() :
                return tagCateg
            else:
                if not len( tagCateg.children ) == 0 :
                    for child in tagCateg.children:
                        id = cls.fetchCategoryChildren( name, child )
                        if id is not None:
                            return TagCategory.get( id )
                        
    @classmethod                    
    def fetchCategoryChildren(cls, name, rootId):
        categ = TagCategory.get( rootId )
        if categ.name.lower() == name.lower():
            return rootId
        else:
            if not len( categ.children ) == 0:
                for child in categ.children:
                    id = cls.fetchCategoryChildren( name, child )
                    if id is not None:
                        return id
                    
    def searchParents(self):
        if self.parent == None:
            listCategParents =  list()
            return listCategParents
        else:
            parentCat = TagCategory.get( self.parent )
            listCategParents = parentCat.searchParents()
            listCategParents.append( parentCat.id )
            return listCategParents
        
