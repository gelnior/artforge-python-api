# coding=utf-8

import hashlib
import base64
import urllib2

class PasswordHash(object):
    '''
    PasswordHash provides a process method to encrypt password at sha1 format 
    then transform it to base 64.
    '''
     
    def __init__(self):
        '''
        Constructor, set encryption builder.
        '''
        self.digester = hashlib.sha1()
        
    def process(self, value):
        '''
        Return *value* at encrypted format.
        
        Arguments :
            *value* The value to encrypt.
        '''
        self.digester.update(value)
        output = self.digester.digest()
        output = base64.b64encode(output)
        return output[0:len(output)-1]
    
class Authentication(object):    
    '''    
    Authentication provides tools to automatically authenticate scripts when 
    accessing to services.
    ''' 
    def __init__(self, 
                 baseUrl = None, 
                 username = 'graph', 
                 password = 'graphpass'):
        '''
        Constructor : Build an Authentication object is enough to set 
        authentication for every HTTP call to services.
        
        Arguments :
            *baseUrl* Services  URL
            *userName* The login
            *password* The password
        '''
        
        self.baseUrl = baseUrl
        self.username = username
        self.password = password
        
        # this creates a password manager
        passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, self.baseUrl, self.username, self.password)
        
        # because we have put None at the start it will always
        # use this username/password combination for urls
        # for which `self.baseUrl` is a super-url
        
        # create the AuthHandler
        authhandler = urllib2.HTTPBasicAuthHandler(passman)
        opener = urllib2.build_opener(authhandler)
        urllib2.install_opener(opener)
        
        # All calls to urllib2.urlopen will now use our handler
        # Make sure not to include the protocol in with the URL, or
        # HTTPPasswordMgrWithDefaultRealm will be very confused.
        
        # You must (of course) use it when fetching the page though.#        
        # theurl = 'http://dev.hd3d.fr:11800/dev-Hd3dServices/v1/'
        # pagehandle = urllib2.urlopen(self.baseUrl)
        # authentication is now handled automaticaly for us     

        
    def getId(self):
        return base64.encodestring('%s:%s' % (self.username, self.password))[:-1]
    
        