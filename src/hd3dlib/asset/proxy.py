# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute

from hd3dlib.baseobject import BaseObject



from string import Template

PROXY_DEFINITION = {
        'name':'ProxyGenerated',
        'attributes':{
            '_class':Attribute(
                'class', str,
                default = 'fr.hd3d.model.lightweight.impl.LProxy'
            ),
            'fileRevisionId':Attribute('fileRevisionId', long),
            'proxyTypeId':Attribute('proxyTypeId', long),
            'proxyTypeName':Attribute('proxyTypeName', str),
            'mountPoint':Attribute('mountPoint', str),
            'location':Attribute('location', str),
            'filename':Attribute('filename', str),
            'description':Attribute('description', str),
            'framerate':Attribute('framerate', long),
        }
}

class Proxy(makeClass(PROXY_DEFINITION, BaseObject)):
    '''
    Proxy allows to retrieve proxy  list.
    '''

    _locatorTmpl = Template('/proxies/')
    _locatorShort = '/proxies/'
    _locator = '/proxies/'

    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
