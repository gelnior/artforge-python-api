# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


DEVICEMODEL_DEFINITION = {
    'name':'DeviceModelGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LDeviceModel', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
        "manufacturer" : Attribute('manufacturer', long),
        "manufacturerName" : Attribute('manufacturerName', str),
    }
}

class DeviceModel(makeClass(DEVICEMODEL_DEFINITION, BaseObject)):
    '''
    DeviceModel corresponds to a device model like : Canon PIXMA iX5000.
    With this class all device can be linked to a model. 
    '''

    _locatorTmpl = Template('/device-models/')
    _locatorShort = '/device-models/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()
        
        
    @classmethod
    def query(cls):
        '''
        Return query object for item group.
        '''
        return BaseObject.queryClass(cls) 