#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

'''
Created on 10 Nov 2010

@author: Jérôme Gonnon
'''

from hd3dlib.util.httprequests import HttpRequestHandler, HttpRequestTrace
from hd3dlib import *
from hd3dlib.production import Project
from hd3dlib.production import Constituent
from hd3dlib.production import Shot
from hd3dlib.team import Person

from hd3dlib import conf
from hd3dlib.asset import ImageFileSystem

import sys
import pprint
import os
import MySQLdb

#GLOBALS

global user
global project

def doIt(projectName, path, log):
    
       global user
       global project
           
       user = Person.getScriptUser()   
       
       pprint.pprint(str(projectName))
       project = Project.getByName(projectName)  
       if  project is not None:
           
           objects = project.getAllConstituents()
           for object in objects :
                # Use Label .jpg
                name =  path + object.label + ".png"
                pprint.pprint(name)
                if os.path.exists(name):
                    object.setThumbnail(name.encode('utf-8'))
                continue
            
           objects = project.getAllShots()
           for object in objects :
                # Use Label .jpg
                name =  path + object.label + ".png"
                pprint.pprint(name)
                if os.path.exists(name):
                    object.setThumbnail(name.encode('utf-8'))
                continue
                         
    
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
        
    log = None
    
    try:
        
        res = os.popen( 'pgrep -f UploadThumbnailsProject.py' ).read()
        output  = res.split("\n")
            
        if len(output)>3:
            #log.write('Process already in progress!')
            raise ValueError('Process already in progress!')

        logLocation = conf.dataServerLogPath
        if not os.path.exists(logLocation):
            raise ValueError('Log directory does not exist.')
            
        log = Log(str(os.path.basename(__file__)), logLocation)
        log.enableConsole()
        log.start()
        
        log.write('Arguments are : ' + str(sys.argv))
        
        if len(sys.argv) != 3:
             log.write('Arguments must be Project and Directory')
             # ./UploadThumbnailsProject Haribo /home/toto/Images/
             sys.exit(1)
        
        projectName = str(sys.argv[1]) 
        pprint.pprint('Project is : ' + projectName)
        
        path = str(sys.argv[2]) 
        pprint.pprint('Path is : ' + path)
        
        if not os.path.exists(path):
            raise ValueError('Directory does not exist.')
            
        log.write('Before I process everything.')
        doIt(projectName, path, log)
        log.write('After I\'ve processed everything.')
        
    except Exception, e:
        if log is not None:
            log.write('Error: ' + str(e.message) + ' '  + str(type(e)))
        else:
            print 'Error:', e.message, type(e)
    finally:
        if log is not None:
            log.stop()
