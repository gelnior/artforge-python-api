# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template


CATEGORY_DEFINITION = {
    'name':'CategoryGenerated',
    'attributes':{
        '_class' : Attribute(
             'class', 
             str, 
             default='fr.hd3d.model.lightweight.impl.LCategory', 
             mutable=False
        ),
        'simpleClassName' : Attribute('simpleClassName', str),
        'name' : Attribute('name', str),
        'parent' : Attribute('parent', long),
        'project' : Attribute('project', long), 
    }
}
    
class Category(makeClass(CATEGORY_DEFINITION, BaseObject)):
    '''
    Category allows to organize constituents in folders. 
    Category act as a folder in the constituent navigation tree..
    '''
    
    _simpleclassname='Category'
    _locatorTmpl = Template('/projects/$projects/categories/')
    _locatorShort = '/projects/0/categories/'
    _locator = '/projects/0/categories/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)

        if self.name is None or self.name == '': 
            self.name = "New category " + self._jicId

        if self.project is not None:
            self._resetLocator()
    
    
    def _resetLocator(self):
        self._locatorTmpl = Template(Category._locatorTmpl.template)
        self._locator = self._locatorTmpl.safe_substitute(projects=self.project)
            
            
    @classmethod
    def getByName(cls, project, name):
        '''
        Return the category of which name is equal to name.         
        It returns None if object does not exist.        
        
        Arguments:
            *project* Project in which category will appear.
            *name* Name of the category to create.
        '''
        obj = cls.query().branch('projects', project.id).leaf("all").filter('name', name).getFirst()
     
        return obj
            
            
    @classmethod
    def getOrCreateByName(cls, project, name):
        '''
        Return the category of which name is equal to name.         
        It creates the category if it does not already exist.
        
        Category is created as a root category.
        
        Arguments:
            *project* Project in which category will appear.
            *name* Name of the category to create.
        '''
        obj = cls.query().branch('projects', project.id).leaf("all").filter('name', name).getFirst()
    
        if obj is None:        
            obj = cls(name=name)
            obj.project = project.id
            obj._resetLocator()
            obj.create()
        
        return obj
    
    @classmethod
    def getByProject(cls, projectId):
        '''
        Return categories from a project.         
        
        Arguments:
            *projectId* the project id in which categories will appear.
        '''
        return Category.query().branch('projects', projectId).getAll()
            

    @classmethod
    def query(cls):
        '''
        Return query object for Category.
        '''
        return BaseObject.queryClass(cls) 