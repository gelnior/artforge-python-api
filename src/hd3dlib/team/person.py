# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template
from hd3dlib.asset import ImageFileSystem
from hd3dlib.util import wsRequestHandler
from hd3dlib.util.authutils import Authentication
import urllib
  
PERSON_DEFINITION = {
    'name':'PersonGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LPerson', 
            mutable=False
        ),
        "id" : Attribute('id',int),
        "userLogin" : Attribute('login', str),
        "name" : Attribute('name', str),
        "email" : Attribute('email', str),
        "firstName" : Attribute('firstName', str),
        "lastName" : Attribute('lastName', str),
        "accountStatus" : Attribute('accountStatus', int, mutable=False),
        "resourceGroupNames" : Attribute('resourceGroupNames', list, 
                                         mutable=False),
        "resourceGroupIDs" : Attribute('resourceGroupIDs', list),
        "userCanDelete" : Attribute('userCanDelete', bool, default=False),
        "userCanUpdate" : Attribute('userCanUpdate', bool, default=False),
        "skillLevelNames" : Attribute('skillLevelNames', list, mutable=False),
        "skillLevelIDs" : Attribute('skillLevelIDs', list, mutable=False),
        "defaultPath" : Attribute('defaultPath', str, mutable=False),
    }
}

class Person(makeClass(PERSON_DEFINITION, BaseObject)):
    '''
    Person describes a real worker. Workers are owned by groups and have 
    assigned tasks.
    '''

    _locatorTmpl = Template('/persons/')
    _locatorShort = '/persons/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()

    
        
    def setThumbnail(self, path):
        '''
        Set thumbnail for person from the image located at path.
        
        Arguments:
            *path* The path of image to set as thumbnail.
        '''
        if self.id is not None:
            fs = ImageFileSystem() 
            fs.setThumbnail('persons/', path, self.id)
    
    def createAccount(self, password):
        '''
        Create account for current user. Set *password* as password for this 
        account. If the user has no account, the account isn't created.
        
        Arguments:
            *password* The user new password.
        '''
        if self.accountStatus != 0 :
            url = "user-accounts/%s" % self.userLogin
            body = urllib.urlencode({"account_secret" : password},)
            wsRequestHandler.post(url, body, returnsOnlyId=False, headers={'Content-type':'application/x-www-form-urlencoded', 'Charset':'utf-8'})        
        
    def login(self, password):     
        '''
        Log in Python scripts as current person. It is very handy for CG 
        software script development.
        
        Arguments:
            *password* The user password.         
        '''     
        url = '../login'            
        body = urllib.urlencode({"login" : self.userLogin, "password": password},)
        dic = wsRequestHandler.post(
            url, 
            body, 
            returnsOnlyId=False, 
            returnDataAsDict=True, 
            headers={'Content-type':'application/x-www-form-urlencoded', 
                     'Charset':'utf-8'}
        )
        wsRequestHandler.authentication = Authentication(wsRequestHandler.baseUrl, 
                                                         self.userLogin, 
                                                         password)
                
        if 'result' in dic:            
            result = dic['result']
            if result == "ok":                
                print "login succeeds"
                return True
                        
        print 'login failed' 
        return False
    
    def logout(self):
        '''
        Logout current person, if its account is used by Python API for 
        authentication.
        '''
        url = '../logout'               
        dic = wsRequestHandler.get(url)
        
        if 'result' in dic:            
            result = dic['result']
            if result == "ok":                
                wsRequestHandler.authenticate('', '')                
                print "logout succeeds"
                return True
                        
        print 'logout failed' 
        return False
    
    
    @classmethod          
    def getScriptUser(cls):
        '''
        Return default script user. If script user does not exist, it is 
        created.
        '''
        user = Person.query().filter('login', 'script').getFirst()
        if user is None:
            user = Person(firstName='Script', lastName='User')
            user.userLogin = 'script'
            user.create()
        return user
    

    @classmethod
    def getByLogin(cls, login):
        '''
        Retrieve the person whose the login is equal to login. If the person
        does not exist, it returns null.
        
        Arguments:
            *login* The login used for filtering.
        '''
        return Person.query().filter('login', login).getFirst()

    
    @classmethod
    def getOrCreateByLogin(cls, firstName, lastName, login):
        '''
        Retrieve the person whose the login is equal to login. If the person
        does not exist, he is created with *firstName* and *lastName* as 
        First Name and last name.
            
        Arguments:
            *login* The login used for filtering.
            *firstName* First name of person to create if no person exist with this login.
            *lastName*  Last name of person to create if no person exist with this login.
        '''
        person = Person.getByLogin(login)
        
        if person is None:
            person = Person()
            person.userLogin = login
            person.firstName = firstName
            person.lastName = lastName 
            person.create()
           
        return person
