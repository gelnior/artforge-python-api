# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from hd3dlib.asset.assetrevision import AssetRevision

from string import Template

ASSETGROUP_DEFINITION = {
    'name':'AssetGroupGenerated',
    'attributes':{
        '_class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LAssetRevisionGroup'
        ),
        'name':Attribute('name', str),
        'project':Attribute('project', long),
        'criteria':Attribute('criteria', str),
        'value':Attribute('value', str),
        'assetRevisions':Attribute('assetRevisions', list, 
                                   default=[], mutable=False)
    }
 }

class AssetGroup(makeClass(ASSETGROUP_DEFINITION, BaseObject)):
    '''
    Asset group contains a list of assets. Asset group is linked to a work object
    via two fields criteria (the simple classname of work object)and the id 
    of the linked work object. Criteria is needed to differentiate shot from
    constituent.
    '''

    _locatorTmpl = Template('/asset-revision-groups/')
    _locatorShort = '/asset-revision-groups/'
    _locator = '/asset-revision-groups/'

    def __init__(self, **params):
        BaseObject.__init__(self, **params)


    @classmethod
    def createForWorkObject(cls, workObject):
        '''
        Create work asset group for work object, if it does not already exists.
        Group name is formed this way : *id_hook_GRP*. Where id is the work 
        object id and, hook the work object hook.
                
        Arguments:
            workObject The work object for which a group will be created.
        '''            
        groupName = unicode(workObject.id) + u'_' + workObject.hook + u'_GRP'
        groupName = groupName.encode("utf-8")
        query = cls.query().filter('criteria', groupName)
        query = query.filter('value',str(workObject.id))        
        woAssetGroup = query.getFirst()
        
        if woAssetGroup is None:
            woAssetGroup = AssetGroup(name=str(groupName),
                                      criteria=str(workObject._class) + '.id',
                                      value=str(workObject.id))            
            woAssetGroup.create()    
            print 'Asset group : %s created' % groupName
            
        return woAssetGroup
    
    @classmethod
    def createForTaskName(cls, project, name, value):
        '''
        Create work asset group for task object, if it does not already exists.
        '''    
        criteria = 'fr.hd3d.model.lightweight.impl.LTask.name'
        
        query = cls.query().filter('name', name)
        query = query.filter('criteria', criteria)
        query = query.filter('value', str(value))
        taskAssetGroup = query.getFirst()
        
        if taskAssetGroup is None:
            taskAssetGroup = AssetGroup(name=name,
                                        project=project.id,
                                        criteria=criteria,
                                        value=str(value))
            taskAssetGroup.create()
            
        return taskAssetGroup
    
    
    @classmethod
    def getByConstituent(cls, constituent):
        '''
        Return asset group corresponding to given constituent. If several
        asset groups exist for this constituent only one is retrieved. 
        
        Arguments:
            *constituent* The given constituent.
        '''
        
        criteria = 'Constituent'
        req = cls.query()
        req = req.filter('criteria', criteria)
        req = req.filter('value', str(constituent.id))
        
        return req.getFirst()
    
    
    @classmethod
    def getAllByConstituent(cls, constituent):
        '''
        Return all asset group corresponding to given constituent.
        
        Arguments:
            *constituent* The given constituent.
        '''
        
        criteria = 'Constituent'
        req = cls.query()
        req = req.filter('criteria', criteria)
        req = req.filter('value', str(constituent.id))
        
        return req.getAll()
    
    
    @classmethod
    def getByShot(cls, shot):
        '''
        Return asset group corresponding to given constituent. If several
        asset groups exist for this shot only one is retrieved. 
        
        Arguments:
            *shot* The given shot.
        '''
        
        criteria = 'Shot'
        req = cls.query()
        req = req.filter('criteria', criteria)
        req = req.filter('value', str(shot.id))
        
        return req.getFirst()    
    
    @classmethod
    def getAllByShot(cls, shot):
        '''
        Return all asset group corresponding to given shot.
        
        Arguments:
            *shot* The given shot.
        '''
        
        criteria = 'Shot'
        req = cls.query()
        req = req.filter('criteria', criteria)
        req = req.filter('value', str(shot.id))
        
        return req.getAll()   

                           
    def addAssetRevision(self, asset):
        '''
        Add *asset* to the group and save it directly to services.
        
        Arguments:
            asset The asset to add.
        '''
        self.assetRevisions.append(asset.id)        
        self.update()
        
    def getAssets(self, taskTypeId = -1):
        '''
        Return assets linked to the group by requesting list from server.
        '''
        AssetRevision._locatorTmpl=Template('asset-revisions/')
        if self.assetRevisions is not None and len(self.assetRevisions) > 0:
            if taskTypeId != -1:
                req = AssetRevision.query().filter('taskType', taskTypeId).filter_custom('in', 'id', self.assetRevisions)
            else:
                req = AssetRevision.query().filter_custom('in', 'id', self.assetRevisions)
            return req.getAll()
        else: 
            return None 
                 
    @classmethod
    def query(cls):
        '''
        Return query object for sheet.
        '''
        return BaseObject.queryClass(cls) 
        
