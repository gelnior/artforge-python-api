# coding=utf-8
from hd3dlib.baseobject import BaseObject
from hd3dlib.util.classbuild import makeClass, Attribute

from string import Template


SHEET_DEFINITION = {
        'name':'SheetGenerated',
        'attributes':{
            '_class':Attribute(
                'class', 
                str, 
                default='fr.hd3d.model.lightweight.impl.LSheet', 
                mutable=False
            ),
            'name':Attribute('name', str), 
            'description':Attribute('description', str),
            'project':Attribute('project', long),
            'boundClassName':Attribute('boundClassName', str),
            'type':Attribute('type', str),
            'itemGroups':Attribute('itemGroups', list)
    }
}

class Sheet(makeClass(SHEET_DEFINITION, BaseObject)):
    '''
    Sheet allows user to make views on data via explorer widget. Sheet is 
    composed of items (grouped in item groups). Each item displays a field
    selected by user. 
    '''
    
    _locatorTmpl = Template('/sheets/')
    _locatorShort = '/sheets/'
    _locator = '/sheets/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self,**kw)

        
    @classmethod
    def query(cls):
        '''
        Return query object for Sheet.
        '''
        return BaseObject.queryClass(cls) 
    
    @classmethod
    def getOrCreate(cls, name, description, projectId, boundClassName, type):
        '''
        Return the sheet of which name is equal to name. 
        It creates it if the sheet does not already exists.
        
        Arguments:
            *name* the sheet name.
            *description* the description of the sheet
            *projectId* the project id of the sheet
            *boundClassName* the bound class name of the sheet
            *type* the type of sheet
        '''
        
        sheet = Sheet.getByName(name, projectId)
        
        if sheet is None:
            sheet = Sheet()
            sheet.name = name
            sheet.description = description
            sheet.project = projectId
            sheet.boundClassName = boundClassName
            sheet.type=type
            sheet.create()
           
        return sheet
    
    @classmethod
    def getByName(cls, name, projectId):
        '''
        Return the sheet of which name is equal to name to a given project .
        
        Arguments:
            *name* Name of sheet to retrieve
            *projectId* the project id of the sheet
        '''   
        return Sheet.query().filter('name', name).filter('project',projectId).getFirst()
    
    @classmethod
    def getByProject(cls, projectId):
        '''
        Return the sheets from a project.
        
        Arguments:
            *projectId* the project id
        '''   
        return Sheet.query().filter('project', projectId).getAll()