# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


COMPUTERTYPE_DEFINITION = {
    'name':'ComputerTypeGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LComputerType', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
    }
}

class ComputerType(makeClass(COMPUTERTYPE_DEFINITION, BaseObject)):
    '''
    ComputerType corresponds to a computer type like : laptop, tower, 
    server...
    '''

    _locatorTmpl = Template('/computer-types/')
    _locatorShort = '/computer-types/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()
        
    @classmethod
    def getByName(cls, name):
        '''
        Retrieve the computer type whose the name is equal to name. If the computer type
        does not exist, it returns null.
        
        Arguments:
            *name* The name used for filtering.
        '''
        return ComputerType.query().filter('name', name).getFirst()
            
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Retrieve the type whose the name is equal to name. If the type
        does not exist, it create it.
        
        Arguments:
            *name* The name used for filtering.
        '''
        type = ComputerType.getByName(name)
        
        if type is None:
            type = ComputerType()
            type.name = name
            type.create()
            
        return type
