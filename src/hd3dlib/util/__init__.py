# coding=utf-8
'''
Utility classes for the library.
'''

__all__ = [  'PasswordHash',
            'wsRequestHandler',
            'HttpRequestHandler',
            'httprequestconstraints',
            'classbuild',
            'generateRandomId',
            'multipartformencode',
            'ODSpreadsheet', 
            'DynamicPathUtils',]

import classbuild
import httprequestconstraints
import multipartformencode

from authutils import PasswordHash
from httprequests import HttpRequestHandler
from odspreadsheet import ODSpreadsheet
from randomid import generateRandomId
from dynamicpathutils import DynamicPathUtils


def initHttpRequestHandler():
    '''
    Build request handler that will be used to send data to services. 
    '''
    from hd3dlib import conf
    
    try:
        handler = HttpRequestHandler(conf.dataServerName, conf.dataServerPort, 
                                     conf.dataServerRootUrl, conf.protocol)
    except Exception:
        print "Error occurs, or no protocol is set. HTTP will be used by default."
        handler = HttpRequestHandler(conf.dataServerName, conf.dataServerPort, 
                                     conf.dataServerRootUrl)
        
    if hasattr(conf, 'defaultUser') and hasattr(conf, 'defaultPassword'): 
        handler.authenticate(conf.defaultUser, conf.defaultPassword)
    
    #handler.setMaxRecord()
    return handler

# Global declaration for request handler, so it can be called from anywhere.
global wsRequestHandler 
wsRequestHandler = initHttpRequestHandler()
