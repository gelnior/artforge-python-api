# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

PROJECT_TYPE_DEFINITION = {
    'name':'ProjectTypeGenerated',
    'attributes':{
        '_class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LProjectType'
        ),
        'name' : Attribute('name', str),
        'description' : Attribute('description', str),
        'color' : Attribute('color', str, default='#FFAAFF'),
    }
}

class ProjectType(makeClass(PROJECT_TYPE_DEFINITION, BaseObject)):
    '''
    Project type allows to classify project by nature : advertising, short
    movie, long movie...
    '''
    
    _locatorTmpl = Template('/project-types/')
    _locatorShort = '/project-types/'
    _locator = '/project-types/'
            
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
                
    
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Return the project type of which name is equal to name. 
        It creates the project if the project does not already exists.
        
        Arguments:
            *name* Project type name.
        '''
        projectType = ProjectType.getByName(name);
    
        if projectType is None:
            projectType=ProjectType()        
            projectType.name = name
            projectType.create()
        
        return projectType
    
    @classmethod
    def getByName(cls,name):
        '''
        Return the project type corresponding to name.
        
        Arguments:
            *name* The name whose project type is retrieved.
        '''
        return ProjectType.query().filter('name', name).getFirst()

