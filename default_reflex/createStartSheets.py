# coding=utf-8

from hd3dlib import conf
from hd3dlib.util.httprequests import HttpRequestHandler, HttpRequestTrace
from hd3dlib.extra import Sheet, ItemGroup, Item

PERSON_SHEET_NAME = 'People'
CONTRACT_SHEET_NAME = 'Contracts'
ATTRIBUTE = 'attribute'
JAVA_LANG_STRING = 'java.lang.String'
SHORT_TEXT = 'short-text'


def createMachinesSheet():
    print "Machines : done"

def createTeamsSheet():
    
    boundClassName = 'Person'
    Sheet._locator = 'sheets/'
     
    #create sheet
    defaultPersonSheet = Sheet(name = PERSON_SHEET_NAME, 
                               description = 'Standard Person sheet',
                               boundClassName = boundClassName).create()
    print 'TEAMS : New sheet created : %s' % defaultPersonSheet.name
    
    hookRoot = '_'.join([boundClassName, PERSON_SHEET_NAME])
    
    #create main group and items
    igDefault=ItemGroup(name = 'Principal',
                        hook = str('_'.join([hookRoot, 'Principal'])),
                        sheet = defaultPersonSheet.id)
    igDefault._resetLocator()
    igDefault.create()
    
    print 'Group : %s' % igDefault.name

    item         = Item(defaultPersonSheet.id,
                           name = 'First Name', 
                           query = 'firstName', 
                           type = ATTRIBUTE, 
                           metaType = JAVA_LANG_STRING,
                           renderer = '',
                           editor = SHORT_TEXT,
                           itemGroup = igDefault.id).create()
    print '    Item : %s' % item.name

    item         = Item(defaultPersonSheet.id,
                           name = 'Name', 
                           query = 'lastName', 
                           type = ATTRIBUTE, 
                           metaType = JAVA_LANG_STRING,
                           renderer = '',
                           editor = SHORT_TEXT,
                           itemGroup = igDefault.id).create()
    print '    Item : %s' % item.name
    
    item     = Item(defaultPersonSheet.id,
                           name = 'Thumbnail', 
                           query = 'photoPath', 
                           type = ATTRIBUTE,
                           metaType = JAVA_LANG_STRING,
                           renderer = 'thumbnail',
                           itemGroup = igDefault.id).create()
    print '    Item : %s' % item.name
    
    item         = Item(defaultPersonSheet.id,
                           name = 'Login', 
                           query = 'login', 
                           type = ATTRIBUTE, 
                           metaType = JAVA_LANG_STRING,
                           renderer = '',
                           editor = SHORT_TEXT,
                           itemGroup = igDefault.id).create()                        
    print '    Item : %s' % item.name
    
    item         = Item(defaultPersonSheet.id,
                           name = 'Phone', 
                           query = 'phone', 
                           type = ATTRIBUTE, 
                           metaType = JAVA_LANG_STRING,
                           renderer = '',
                           editor = SHORT_TEXT,
                           itemGroup = igDefault.id).create()                           
    print '    Item : %s' % item.name
    
    item         = Item(defaultPersonSheet.id,
                           name = 'Email', 
                           query = 'email', 
                           type = ATTRIBUTE, 
                           metaType = JAVA_LANG_STRING,
                           renderer = '',
                           editor = SHORT_TEXT,
                           itemGroup = igDefault.id).create()
    print '    Item : %s' % item.name
    
    print 'Sheet creation finished : %s' % PERSON_SHEET_NAME
 
def createContractsSheet():
    
    boundClassName = 'Contract'
    Sheet._locator = 'sheets/'
     
    #create sheet
    defaultContractSheet = Sheet(name = CONTRACT_SHEET_NAME, 
                               description = 'Standard Contract sheet',
                               boundClassName = boundClassName).create()
    print 'TEAMS : New sheet created : %s' % defaultContractSheet.name
    
    hookRoot = '_'.join([boundClassName, CONTRACT_SHEET_NAME])
    
    #create main group and items
    igDefault=ItemGroup(name = 'Principal',
                        hook = str('_'.join([hookRoot, 'Principal'])),
                        sheet = defaultContractSheet.id)
    igDefault._resetLocator()
    igDefault.create()
    
    print 'Group : %s' % igDefault.name

    item         = Item(defaultContractSheet.id,
                           name = 'Person', 
                           query = 'person', 
                           type = ATTRIBUTE, 
                           metaType = 'Person', 
                           renderer = 'Person',
                           editor ='Person',
                           itemGroup = igDefault.id).create()
    print '    Item : %s' % item.name

    item         = Item(defaultContractSheet.id,
                           name = 'Job Title', 
                           query = 'name', 
                           type = ATTRIBUTE, 
                           metaType = JAVA_LANG_STRING,
                           renderer = '',
                           editor = SHORT_TEXT,
                           itemGroup = igDefault.id).create()
    print '    Item : %s' % item.name
    
    item     = Item(defaultContractSheet.id,
                           name = 'Type', 
                           query = 'type', 
                           type = ATTRIBUTE,
                           metaType = 'fr.hd3d.common.client.enums.EContractType',
                           renderer = 'contract-type',
                           editor = 'contract-type',
                           itemGroup = igDefault.id).create()
    print '    Item : %s' % item.name
    
    item         = Item(defaultContractSheet.id,
                           name = 'Start date', 
                           query = 'startDate', 
                           type = ATTRIBUTE, 
                           metaType = 'java.util.Date',
                           renderer = 'date-and-day',
                           editor = 'date',
                           itemGroup = igDefault.id).create()                        
    print '    Item : %s' % item.name
    
    item         = Item(defaultContractSheet.id,
                           name = 'End date', 
                           query = 'endDate', 
                           type = ATTRIBUTE, 
                           metaType = 'java.util.Date',
                           renderer = 'date-and-day',
                           editor = 'date',
                           itemGroup = igDefault.id).create()                        
    print '    Item : %s' % item.name
    
    item         = Item(defaultContractSheet.id,
                           name = 'Real end date', 
                           query = 'realEndDate', 
                           type = ATTRIBUTE, 
                           metaType = 'java.util.Date',
                           renderer = 'date-and-day',
                           editor = 'date',
                           itemGroup = igDefault.id).create()                        
    print '    Item : %s' % item.name
    
    item         = Item(defaultContractSheet.id,
                           name = 'Doc path', 
                           query = 'docPath', 
                           type = ATTRIBUTE, 
                           metaType = JAVA_LANG_STRING,
                           renderer = '',
                           editor = SHORT_TEXT,
                           itemGroup = igDefault.id).create()                           
    print '    Item : %s' % item.name
    
    print 'Sheet creation finished : %s' % CONTRACT_SHEET_NAME
       

def doIt(log):
    
    try:
        from hd3dlib.util.odspreadsheet import ODSpreadsheet
        from hd3dlib.production.breakdown import Breakdown
        
        createTeamsSheet()
        createMachinesSheet()    
        createContractsSheet()    
    
    except Exception, e:
        log.traceback(e)
    finally:
        log.stop()
    
    
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
    
    logLocation = conf.dataServerLogPath
    if not os.path.exists(logLocation):
        sys.exit('Log directory does not exist.')
        
    log = Log(str(os.path.basename(__file__)), logLocation)
    log.enableConsole()
    log.start()

    doIt(log)
    
