# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


DEVICETYPE_DEFINITION = {
    'name':'DeviceTypeGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LDeviceType', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
    }
}

class DeviceType(makeClass(DEVICETYPE_DEFINITION, BaseObject)):
    '''
    DeviceType corresponds to a device type like : printer, hard disk, 
    server...
    '''

    _locatorTmpl = Template('/device-types/')
    _locatorShort = '/device-types/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()
    
    @classmethod
    def query(cls):
        '''
        Return query object for item group.
        '''
        return BaseObject.queryClass(cls) 