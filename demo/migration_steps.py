# coding=utf-8


import time
from datetime import timedelta
from datetime import date

from hd3dlib.extra import Sheet, ItemGroup, Item, DynMetaDataValue
from hd3dlib.production import Project, Step
from hd3dlib.team import Person
from hd3dlib.task import TaskType

UTF8 = 'utf-8'

def createStepsForProject(project, user):
    
    print 'Processing for project %s' % project.name.encode(UTF8)
        
    # Constituents step migration 
    constituents = project.getAllConstituents()
    print '%d constituents found.' % len(constituents)

    stepArray = OldStep.getArray('Constituent', project.id)
    stepList =  str(stepArray.keys())
    print 'Constituent steps : %s' % stepList
    taskTypesArr = TaskType.getTaskTypesArray(stepArray.keys())
    
    for c in constituents:
        createSteps("Constituent", project, c, stepArray, taskTypesArr)

    # Shots step migration
    shots = project.getAllShots()
    print '%d shots found.' % len(shots)
    
    stepArray = OldStep.getArray('Shot', project.id)
    stepList =  str(stepArray.keys())
    print 'Shot steps : %s' % stepList
    taskTypesArr = TaskType.getTaskTypesArray(stepArray.keys())
        
    for s in shots:
        createSteps("Shot", project, s, stepArray, taskTypesArr)



def createSteps(boundEntitiyName, project, workObject, stepArray, taskTypesArray):
                                 
    formerAssetRevision = None
    for stepName in stepArray.keys():
                
        if isinstance(stepName, str):
            stepName = stepName.decode(UTF8)
        
        step = stepArray[stepName]
        duration = step.getValue(workObject)
        taskType = taskTypesArray[stepName]
        
        
        if duration > 0:
            print "create step for %d and of task type %s: %d" % (duration, taskType.name, taskType.id)
            step = Step()
            step.taskType = taskType.id
            step.createTasknAsset = True
            step.estimatedDuration = 1
            step.boundEntity = workObject.id
            step.boundEntityName = boundEntitiyName
            step.create()
            
        else :
            print "Don't create step for %d and of task type %s: %d" % (duration, taskType.name, taskType.id)
            




class OldStep(object):
    '''
    Log file for a reflex script
    '''
    entityClassName = ''
    name = ''
    type = ''
    metaType = ''
    query = ''
    
    
    def __init__(self, entityClassName, name, type, metaType, query):
        self.entityClassName = entityClassName
        self.name = name
        self.type = type
        self.metaType = metaType
        self.query = query
        
        
    def getValue(self, entity):
        entityStepValue = 0
        if self.type == 'attribute':
            if self.metaType == 'java.lang.Boolean' and eval('entity.' + self.query) is True:
                entityStepValue = 1
            elif self.metaType == 'java.lang.Long' and eval('workObject.' + self.query) > 0:
                entityStepValue = eval('workObject.' + self.query)
            elif self.metaType == 'java.lang.String' and eval('workObject.' + self.query) > 0:
                tmpValue = eval('workObject.' + self.query)
                if type(1) == type(tmpValue):
                    entityStepValue = int(tmpValue)      
        elif self.type == 'metaData':
            mdv = DynMetaDataValue().query().filter('parent', entity.id).filter('classDynMetaDataType', long(self.query)).getLast()
            if mdv is not None and self.metaType == 'java.lang.Boolean' and mdv.value == 'true': 
                entityStepValue = 1
            elif mdv is not None and self.metaType == 'java.lang.Long' and mdv.value > 0: 
                entityStepValue = mdv.value
            elif mdv is not None and self.metaType == 'java.lang.String':
                val = mdv.value.strip()
                if val.isalnum():
                    if int(val) > 0: 
                        entityStepValue = int(val)
        return entityStepValue
    
    
    @classmethod
    def getArray(self, entityClassName, projectId):
        'Returns a list of step instances define for a certain class'
        stepArray = {}

        #The reference sheet is named like the work object class
        query = Sheet.query().filter('project.id', projectId)
        query = query.filter('boundClassName', entityClassName)                                          
        query = query.filter('name', entityClassName)
         
        conventionalSheet =  query.getLast()
        
        if conventionalSheet is None:
            print 'No conventional sheet was found for class name (%s)' % entityClassName
            return stepArray
        
        #retrieve item group (called 'steps')
        ig = ItemGroup.query().branch('sheets', conventionalSheet.id).filter('name', 'Steps').getLast()
        if ig is None:  
            print RuntimeError, 'No conventional item group was found for sheet (%s)' % conventionalSheet.name
            return stepArray
     
        for itemId in ig.items:
            itId = long(itemId)
            it = Item(conventionalSheet)            
            it = it.read(itId)
            s = OldStep(entityClassName, it.name, it.type, it.metaType, it.query)
            stepArray[s.name]=s
                
        return stepArray
    
    

    


def doIt():

    projects = Project.getOpenProjects()    
    user = Person.getScriptUser()
    
    for project in projects:
        createStepsForProject(project, user)
    
    
    
if __name__ == '__main__':
         
    print '[INFO] Migration starts.'
    doIt()
    print '[INFO] Migration is finished.'
    