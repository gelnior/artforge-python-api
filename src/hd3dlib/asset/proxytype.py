# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute

from hd3dlib.baseobject import BaseObject



from string import Template

PROXYTYPE_DEFINITION = {
        'name':'ProxyTypeGenerated',
        'attributes':{
            '_class':Attribute(
                'class',str,
                default='fr.hd3d.model.lightweight.impl.LProxyType'
            ),
            'name':Attribute('name', str),
            'openType':Attribute('openType', str),
            'description':Attribute('description', str),
        }
}

class ProxyType(makeClass(PROXYTYPE_DEFINITION, BaseObject)):
    '''
    ProxyType allows to retrieve proxy type list.
    '''
    
    _locatorTmpl = Template('/proxy-types/')
    _locatorShort = '/proxy-types/'
    _locator = '/proxy-types/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        
    @classmethod
    def getOrCreate(cls,name, description="", openType=""):
        proxyType=ProxyType.getByName(name)
        if not proxyType:
            proxyType=ProxyType()
            proxyType.name=str(name)
            proxyType.description=str(description)
            proxyType.openType=str(openType)
            proxyType.create()
            
        return proxyType
            
    @classmethod
    def getByName(cls,name):
        return ProxyType.query().filter("name",name).getFirst()