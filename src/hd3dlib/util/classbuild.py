# coding=utf-8

__all__ = ['Attribute', 'makeClass']

class Attribute(object):  
    '''
    Attribute correspond to a service class attribute. When JSON from services
    is parsed data are store in Attributes.
    '''
    def __init__(self, mapping, objType, default = None, mutable = True):
        '''
        Constructor : it checks that given value is rightly typed.        
        '''
        self.mapping = mapping
        self.type = objType
        self.mutable = mutable
        
        if default is None or isinstance(default, objType):
            self.default = default
        else:
            raise ValueError, 'Can\'t build attribute definition : default value of type %s is not applicable for attribute "%s". Expecting %s.' % (type(default), mapping, objType)


def makeClass(definition, *parents):
    '''
    Generate a class with the class fields set in the definition dict, so as to 
    match the _jsonRepresentation dictionary.
    
    The definition must be a dict like : 
        ('name':'className', 
         'attributes':{'attr1':Attribute('mappingAttr1', str), 
                       'attr2':Attribute('mappingAttr2', )}}
                       
    All attributes are typed.
    
    To be used properly it should be inherited.
    
    Mutables attribute are not editable.
    '''
    name = definition['name']
    attrs = definition['attributes']
    
    # Checks whether the _jsonRepresentation & _fields_ dicts already exist
    # If so, concatenate existing values to the corresponding dicts in child 
    # class
    _jsonRepresentation = {}
    _fields_ = set()
    for curClass in parents:
        if hasattr(curClass, '_jsonRepresentation'):
            _jsonRepresentation.update(getattr(curClass, '_jsonRepresentation'))
        if hasattr(curClass, ('_fields_')):
            _fields_.update(getattr(curClass,'_fields_'))
            
    # For each attribute name build, corresponding attribute, with right 
    # properties : get and set for mutable attributes. get for non mutable 
    # attributes. 
    for attributeName in attrs:
        if not isinstance(attrs[attributeName], Attribute):
            raise ValueError, 'Class definition contained an invalid attribute definition (%s).' % attributeName

        _fields_.add(attributeName)
        
        def makeProperty(myAttribute):
            
            currentName = attributeName
            currentType = myAttribute.type
            currentMapping = myAttribute.mapping
            currentDefault = myAttribute.default
            currentMutable = myAttribute.mutable
            
            # Manage default value
            if currentDefault is not None:
                if isinstance(currentDefault, currentType):
                    _jsonRepresentation[currentMapping] = currentDefault
                else:
                    raise ValueError, 'Default %s is not applicable for attribute "%s". Expecting %s.' % (type(currentDefault), currentName, currentType)
            else:
                _jsonRepresentation[currentMapping] = None
            
            # Construct getter and setter
            def get(obj):
                if currentMapping in obj._jsonRepresentation:
                    return obj._jsonRepresentation[currentMapping]
                else:
                    return None
            
            def set(obj, value):
                if isinstance(value, currentType):
                    obj._jsonRepresentation[currentMapping] = value
                elif currentType is long and isinstance(value, int):
                    obj._jsonRepresentation[currentMapping] = long(value)
                elif value is None:
                    obj._jsonRepresentation[currentMapping] = value
                else:
                    raise ValueError, '%s is not applicable for attribute "%s\". Expecting %s.' % (type(value), currentName, currentType)
            
            def remove(obj):
                if currentMapping in obj._jsonRepresentation:
                    obj._jsonRepresentation.pop(currentMapping) 
            if currentMutable is True:
                return property(get,set,remove)
            else:
                return property(get)
            
        attrs[attributeName] = makeProperty(attrs[attributeName])
    attrs['_jsonRepresentation'] = _jsonRepresentation
    attrs['_fields_'] = _fields_
    
    return type(name, parents, attrs)