'''
Created on 24 nov. 2009

@author: Nicolas Provost

Description : This script uploads pasta images to the thumbnail services. Its 
purpose is to enhance the Pasta project look'n'feel.
'''

from hd3dlib.util.httprequests import HttpRequestHandler, HttpRequestTrace
from hd3dlib.asset import ImageFileSystem
from hd3dlib.team import Person
from hd3dlib.production import Shot, Constituent, Project

from string import Template


PROJECT_NAME = 'Pat et Stanley'

def setShotThumbnail(label, path):
        
    shot = Shot.query().filter('hook', label).getFirst()
    if shot is not None:
        fs = ImageFileSystem()
        fs.setThumbnail('shots/', path, shot.id)
        print '[INFO] %s uploaded.' % label
    else:
        print '[WARN] Shot %s does not exist.' % label

def setConstituentThumbnail(label, path):
        
    project = Project.query().filter('name', PROJECT_NAME).getFirst()    
    constituent = Constituent.query().branch('projects', project.id).filter('label', label).getFirst()
    if constituent is not None:
        fs = ImageFileSystem()
        fs.setThumbnail('constituents/', path, constituent.id)
        print '[INFO] %s uploaded.' % label
    else:
        print '[WARN] Constituent %s does not exist.' % label
        

def setPersonThumbnail(name, path):
        
    person = Person.query().filter('lastName', name).getFirst()
    if person is not None:
        fs = ImageFileSystem()
        fs.setThumbnail('persons/', path, person.id)
        print '[INFO] %s uploaded.' % name
    else:
        print '[WARN] Person %s does not exist.' % name

def setPersonThumbnails(path):
        
    path += 'persons/'
    
    setPersonThumbnail('Alt', path + 'laurentalt.jpg')
    setPersonThumbnail('Nicolas', path + 'blandinenicolas.jpg')
    setPersonThumbnail('Lam', path + 'trylam.jpg')
    setPersonThumbnail('De Luca', path + 'georgesdeluca.jpg')
    setPersonThumbnail('Rousseau', path + 'frankrousseau.jpg')

def setConstituentThumbnails(path):
    
    Constituent._locatorTmpl = Template('/constituents/')
    path += 'constituents/'
    
    setConstituentThumbnail('ENTREE', path + 'ENTREE_preview.jpg')               
    setConstituentThumbnail('CALENDRIER', path + 'CALENDRIER_preview.jpg')    
    setConstituentThumbnail('CHAMBRE', path + 'CHAMBRE_preview.jpg')    
    setConstituentThumbnail('CHICHI', path + 'ENTREE_preview.jpg')
    setConstituentThumbnail('CUISINE', path + 'CUISINE_preview.jpg')  
    setConstituentThumbnail('EMILIE', path + 'EMILIE_preview.jpg')  
    setConstituentThumbnail('GRANDRUE', path + 'GRANDRUE_preview.jpg')
    setConstituentThumbnail('JEANLUC', path + 'JEANLUC_preview.jpg')
    setConstituentThumbnail('MAISON', path + 'MAISON_preview.jpg')
    setConstituentThumbnail('MARTHE', path + 'MARTHE_preview.jpg')
    setConstituentThumbnail('MUG', path + 'MUG_preview.jpg')
    setConstituentThumbnail('PAT', path + 'PAT_preview.jpg')
    setConstituentThumbnail('PORTESFENETRES', path + 'PORTESFENETRES_preview.jpg')
    setConstituentThumbnail('REVEIL', path + 'REVEIL_preview.jpg')
    setConstituentThumbnail('SALON', path + 'SALON_preview.jpg')
    setConstituentThumbnail('STAN', path + 'STAN_preview.jpg')
    setConstituentThumbnail('STEPHANIE', path + 'STEPHANIE_preview.jpg')
    setConstituentThumbnail('VOITURE1', path + 'VOITURE1_preview.jpg')
    setConstituentThumbnail('VOITURE2', path + 'VOITURE2_preview.jpg')
              
def setShotThumbnails(path):
    
    Shot._locatorTmpl = Template('/shots/')
    path += 'shots/'
    
    setShotThumbnailSequence('E0001_S01_P0', path, 7)
    setShotThumbnailSequence('E0001_S02_P0', path, 1)
    setShotThumbnailSequence('E0001_S03_P0', path, 1)
    setShotThumbnailSequence('E0001_S04_P0', path, 1)
    setShotThumbnailSequence('E0001_S04_P0', path, 1)
    
    setShotThumbnailSequence('E0002_S01_P0', path, 1)
    setShotThumbnailSequence('E0002_S02_P0', path, 1)
    setShotThumbnailSequence('E0002_S03_P0', path, 1)
    setShotThumbnailSequence('E0002_S04_P0', path, 1)
    setShotThumbnailSequence('E0002_S05_P0', path, 1)
    
    setShotThumbnailSequence('E0004_S01_P0', path, 1)
    setShotThumbnailSequence('E0004_S02_P0', path, 1)
    setShotThumbnailSequence('E0004_S03_P0', path, 1)
    setShotThumbnailSequence('E0004_S04_P0', path, 1)
    setShotThumbnailSequence('E0004_S05_P0', path, 1)
    
    setShotThumbnailSequence('E0005_S01_P0', path, 9)
    setShotThumbnailSequence('E0005_S02_P0', path, 1)
    setShotThumbnailSequence('E0005_S03_P0', path, 1)
    setShotThumbnailSequence('E0005_S04_P0', path, 1)
    setShotThumbnailSequence('E0005_S05_P0', path, 8)
    
        
def setShotThumbnailSequence(prefix, path, number):
    for i in range(number):
        setShotThumbnail('%s%.2d' % (prefix, (i+1)), 
                         '%s%s%.2d_preview.jpg' % (path, prefix, (i+1)))
              
def doIt():
    path =  '/home/services_web/donnees/dev-hd3d-web-services/tmp/imagesInitialisationPasta/'
     
    setPersonThumbnails(path)   
    setConstituentThumbnails(path)
    setShotThumbnails(path)
    
    
if __name__ == '__main__':
    import sys
    import os
    
    try:
                      
        print '[INFO] Thumbnail creation starts.'
        doIt()
        print '[INFO] Thumbnail creation is finished.'
        
    except Exception, e:
        print 'Error:', e.message, type(e)
