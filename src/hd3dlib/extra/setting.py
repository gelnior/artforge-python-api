# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

SETTING_DEFINITION = {
    'name':'SettingGenerated',
    'attributes':{
        '_class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LSetting'
        ),
        'login':Attribute('login', str),
        'person':Attribute('person', long),
        'key':Attribute('key', str),
        'value':Attribute('value', str),
    }
 }

class Setting(makeClass(SETTING_DEFINITION, BaseObject)):
    '''
    Setting save the user preference. Its store in key:value format. 
    '''
    
    _locatorTmpl = Template('/settings/')
    _locatorShort = '/settings/'
    _locator = '/settings/'

    def __init__(self, **params):
        BaseObject.__init__(self, **params)
        
    @classmethod
    def getByPerson(cls, person):
        '''
        Return settings corresponding to given person.  
        
        Arguments:
            person The given person.
        '''
        
        return Setting.query().filter("person",person.id).getAll()
    
    @classmethod
    def getOrCreate(cls, person,key,value):
        '''
        Return setting corresponding to given person and a given key. It's not exist, it'll be create. The value is used to create.   
        
        Arguments:
            person The given person.
            key the key of setting.
            value the value of key setting.
        '''
        
        setting=Setting.query().filter("person",person.id).filter("key",key).getFirst()
        if setting is None :
            setting = Setting()
            setting.key = key
            setting.value = value
            setting.create()
            

        return setting