# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template
  
ROLE_DEFINITION = {
    'name':'RoleGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LRole', 
            mutable=False
        ),
        "name" : Attribute('name',str),
        "resourceGroupIDs" : Attribute('resourceGroupIDs', list),
        "resourceGroupNames" : Attribute('resourceGroupNames', list),
        "permissions" : Attribute('permissions', list),
        "bans" : Attribute('bans', list),
    }
}

class Role(makeClass(ROLE_DEFINITION, BaseObject)):
    '''
    Role describes permissions and bans for a resource group.
    '''

    _locatorTmpl = Template('/roles/')
    _locatorShort = '/roles/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()
    
    @classmethod
    def getByName(cls, name):
        '''
        Retrieve the role whose the name is equal to name. If the role
        does not exist, it returns null.
        
        Arguments:
            *name* The name used for filtering.
        '''
        return Role.query().filter('name', name).getFirst()
    
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Retrieve the role whose the name is equal to name. If the role
        does not exist, it create it.
        
        Arguments:
            *name* The name used for filtering.
        '''
        role = Role.getByName(name)
        
        if role is None:
            role = Role()
            role.name = name
            role.create()
           
        return role
