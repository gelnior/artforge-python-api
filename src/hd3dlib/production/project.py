# coding=utf-8

from hd3dlib.asset import AssetRevision, AssetGroup
from hd3dlib.baseobject import BaseObject
from hd3dlib.production import Category, Constituent, Sequence, Shot
from hd3dlib.production.step import Step
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.task import TaskType
from string import Template


PROJECT_DEFINITION = {
    'name':'ProjectGenerated',
    'attributes': {
        '_class':Attribute(
           'class', 
           str, 
           default='fr.hd3d.model.lightweight.impl.LProject'
        ),
        'name' : Attribute('name', str),
        'hook' : Attribute('hook', str),
        'color' : Attribute('color', str, default='#FF9900'),
        'status' : Attribute('status', str, default='OPEN'),
        'staffIDs' : Attribute('staffIDs', list),
        'staff' : Attribute('staff', list),
        'staffNames' : Attribute('staffNames', list),
        'resourceGroupIDs' : Attribute('resourceGroupIDs', list),
        'resourceGroupNames' : Attribute('resourceGroupNames', list),
        'projectTypeId' : Attribute('projectTypeId', long),
        'projectTypeName' : Attribute('projectTypeName', str),
        'taskTypeIDs' : Attribute('taskTypeIDs', list),
        'taskTypeNames' : Attribute('taskTypeNames', list),
        'taskTypeColors' : Attribute('taskTypeColors', list),
    }
}

class Project(makeClass(PROJECT_DEFINITION, BaseObject)):
    '''
    Project contains constituents, shots, assets and tasks. It links them all
    to define a production made or to be made. 
    '''
     
    _locatorTmpl = Template('/projects/')
    _locatorShort = '/projects/'
    _locator = '/projects/'
            
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        
        
        if len(kw) == 0:
            '''
            This section contains default settings for instances. It allows to :
                1) fix some attributes with generated values, when possible.
                2) raise exceptions when other required attributes are not 
                   properly set.
            '''
            if self.name is None or self.name == '': 
                self.name = "New project " + self._jicId
                
                
    def getAllConstituents(self):
        '''
        Return list of current project constituents.
        '''
        categories = Category.query().branch('projects', self.id).leaf('all').getAll()
        constituents = [] 
        for c in categories:
            constituents.extend(Constituent.query()
                                .branch('projects', self.id)
                                .branch('categories', c.id).getAll())
        return constituents
                
                
    def getAllShots(self):        
        '''
        Return list of current project shots.
        '''
        sequences = Sequence.query().branch('projects', self.id).leaf('children').getAll()
        shots = [] 
        for seq in sequences:
            shots.extend(Shot.query()
                         .branch('projects', self.id)
                         .branch('sequences', seq.id)
                         .getAll())
        return shots
     
     
    def getSequences(self):               
        '''
        Return current project sequences.
        '''     
        return Sequence.query().branch('projects', self.id).leaf('all').getAll()
    
    
    def getSteps(self):               
        '''
        Return current project steps.
        '''     
        return Step.query().branch('projects', self.id).getAll()
    
      
    def getDisk(self):               
        '''
        Return current project default disk.
        '''     
        return Step.query().branch('projects', self.id).getAll()
    
      
    def deleteEveryAssets(self):
        '''
        Delete every assets associated to the project.
        WARNING : This method delete everything, you may lost a lot of data.
        '''    
        groups = AssetGroup.query().branch('projects', self.id).getAll()
        for g in groups:
            if g.criteria is 'fr.hd3d.model.lightweight.impl.LConstituent.id':
                assets = AssetRevision.query().branch('assetrevisiongroups', g.id).getAll()
                for a in assets:
                    a.delete()
            if g.criteria is 'fr.hd3d.model.lightweight.impl.LShot.id':
                assets = AssetRevision.query().branch('assetrevisiongroups', g.id).getAll()
                for a in assets:
                    a.delete()
            g._locatorTmpl = AssetGroup._locatorTmpl
            g._resetLocator()
            g.delete()
                       
                            
    def getTaskTypes(self):
        '''
        Return list of task types selected for this project.
        ''' 
        
        taskTypes = []
        if self.taskTypeIDs:
            taskTypes = TaskType.query().filter_custom('in', 'id', self.taskTypeIDs).getAll()

        return taskTypes
    
    
    @classmethod
    def getByName(cls, name, dyn=False):
        '''
        Return the project of which name is equal to name.
        
        Arguments:
            *name* Name of project to retrieve
            *dyn* True if dynamic attributes should be retrieved.
        '''    
        serviceQuery = cls.query()
        if (dyn) :
            serviceQuery.dyn()
        obj = serviceQuery.filter('name', name).getFirst()
            
        return obj
      
      
    @classmethod
    def getOrCreateByName(cls, name, dyn=False):
        '''
        Return the project of which name is equal to name. 
        It creates the project if the project does not already exists.        
        
        Arguments:
            *name* Name of project to retrieve
            *dyn* True if dynamic attributes should be retrieved.
        '''
        
        obj = cls.getByName(name, dyn)
    
        if obj is None:        
            obj = cls(name=name)
            obj.create()
        
        return obj
    
    
    @classmethod
    def getOpenProjects(cls):
        '''
        Return all projects of which status is set as OPEN.
        '''
        return cls.query().filter('status', 'open').getAll()
        
