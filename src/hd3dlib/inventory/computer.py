# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


COMPUTER_DEFINITION = {
    'name':'ComputerGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LComputer', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
        "serial" : Attribute('serial', str),
        "billingReference" : Attribute('billingReference', str),
        "purchaseDate" : Attribute('purchaseDate', str),
        "warrantyEnd" : Attribute('warrantyEnd', str),
        "inventoryStatus" : Attribute('inventoryStatus', str),
        "dnsName" : Attribute('dnsName', str),
        "ipAdress" : Attribute('ipAdress', str),
        "macAdress" : Attribute('macAdress', str),
        "procFrequency" : Attribute('procFrequency', int),
        "numberOfProcs" : Attribute('numberOfProcs', int),
        "numberOfCores" : Attribute('numberOfCores', int),
        "ramQuantity" : Attribute('ramQuantity', int),
        "workerStatus" : Attribute('workerStatus', str),
        "xPosition" : Attribute('xPosition', int),
        "yPosition" : Attribute('yPosition', int),
        
        "studioMap" : Attribute('studioMapId', long),
        "studioMapName" : Attribute('studioMapName', str),
        "room" : Attribute('roomId', long),
        "roomName" : Attribute('roomName', str),
#        "room" : Attribute('roomId', long),
#        "roomName" : Attribute('roomName', str),        
        "person" : Attribute('personId', long),
        "personName" : Attribute('personName', str),
        "processor" : Attribute('processorId', long),
        "processorName" : Attribute('processorName', str),
        "computerType" : Attribute('computerTypeId', long),
        "computerTypeName" : Attribute('computerTypeName', str),
        "computerModel" : Attribute('computerModelId', long),
        "computerModelName" : Attribute('computerModelName', str),

        "resourceGroupIDs" : Attribute('resourceGroupIDs', list, mutable=False),
        "resourceGroupNames" : Attribute('resourceGroupNames', list, mutable=False),
        "poolIDs" : Attribute('poolIDs', list, mutable=False),
        "poolNames" : Attribute('poolNames', list, mutable=False),
        "softwareIDs" : Attribute('softwareIDs', list, mutable=False),
        "softwareNames" : Attribute('softwareNames', list, mutable=False),
        "licenseIDs" : Attribute('licenseIDs', list, mutable=False),
        "licenseNames" : Attribute('licenseNames', list, mutable=False),
        "softwareIDs" : Attribute('softwareIDs', list, mutable=False),
        "softwareNames" : Attribute('softwareNames', list, mutable=False),
        "deviceIDs" : Attribute('deviceIDs', list, mutable=False),
        "deviceNames" : Attribute('deviceNames', list, mutable=False),
        "screenIDs" : Attribute('screenIDs', list, mutable=False),
        "screenNames" : Attribute('screenNames', list, mutable=False),
    }
}

class Computer(makeClass(COMPUTER_DEFINITION, BaseObject)):
    '''
    Computer describes a computer machine and its links with other devices
    or softwares.
    '''

    _locatorTmpl = Template('/computers/')
    _locatorShort = '/computers/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()

    @classmethod
    def query(cls):
        '''
        Return query object for Computer.
        '''
        return BaseObject.queryClass(cls) 

    @classmethod
    def getByName(cls, name):
        '''
        Retrieve the computer whose the name is equal to name. If the computer
        does not exist, it returns null.
        
        Arguments:
            *name* The name used for filtering.
        '''
        return Computer.query().filter('name', name).getFirst()
    
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Retrieve the computer whose the name is equal to name. If the computer
        does not exist, it create it.
        
        Arguments:
            *name* The name used for filtering.
        '''
        computer = Computer.getByName(name)
        
        if computer is None:
            computer = Computer()
            computer.name = name
            computer.create()
           
        return computer
    
    def addScreen(self, screenId):
        '''
        Adds a screen to the list of associated screens
        
        Arguments:
            *screenId* The id of the screen
        '''
        
        if screenId not in self.screenIDs:
            self.screenIDs.append(screenId)
            
    def addDevice(self, deviceId):
        '''
        Adds a device to the list of associated devices
        
        Arguments:
            *deviceId* The id of the device
        '''
        
        if deviceId not in self.deviceIDs:
            self.deviceIDs.append(deviceId)
     