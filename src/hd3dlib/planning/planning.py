# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template
  
PLANNING_DEFINITION = {
    'name':'PlanningGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LPlanning', 
            mutable=False
        ),
        "name" : Attribute('name',str),
        "project" : Attribute('project',long),
        "master" : Attribute('master',bool,default=False),
        "startDate" : Attribute('startDate',str),
        "endDate" : Attribute('endDate',str),
    }
}

class Planning(makeClass(PLANNING_DEFINITION, BaseObject)):
    '''
    Planning describes a planning of a project.
    '''

    _locatorTmpl = Template('/projects/$projects/plannings/')
    _locatorShort = '/plannings/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()
    
    def _resetLocator(self):
        self._locatorTmpl = Template(Planning._locatorTmpl.template)
        self._locator = self._locatorTmpl.safe_substitute(projects=self.project)
    
    @classmethod
    def getByProject(cls, projectId):
        '''
        Retrieve the planning with the project id. If the planning
        does not exist, it returns null.
        
        Arguments:
            *projectId* The project id used for filtering.
        '''

        return Planning.query().branch("projects",projectId).getFirst()
    
    @classmethod
    def getOrCreateByProject(cls, projectId):
        '''
        Return the planning of which projectId is equal to the project id. 
        It creates it if the planning does not already exists.
        
        Arguments:
            *projectId* Project id.
        '''
        planning = Planning.getByProject(projectId)
        
        if planning is None:
            planning = Planning()
            planning.project = projectId
            planning._resetLocator()
            planning.create()
           
        return planning
