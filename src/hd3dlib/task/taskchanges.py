# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from string import Template


TASK_CHANGES_DEFINITION = {
    'name':'TaskChangesDefinition',
    'attributes':{
        '_class': Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LTaskChanges', 
            mutable=False),        
        'task':Attribute('taskId', long),
        'taskName':Attribute('taskName', str),
        'worker':Attribute('workerId',long),
        'workerName':Attribute('personName',str),
        'planning':Attribute('planningId',long),
        'workObjectName':Attribute('workObjectName',str),
        'color':Attribute('color',str)
    }    
}


class TaskChanges(makeClass(TASK_CHANGES_DEFINITION, BaseObject)):
    '''
    TaskChanges offers possibility to set fake start and end dates on a task.
    It allows to make plannings without modifying the real planning.  
    '''    
    
    _locatorTmpl = Template('/projects/$projects/plannings/$plannings')
    _locatorShort = ''
    _locator = ''
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        
    
