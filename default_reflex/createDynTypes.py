'''
Created on 14 apr. 2010

Create basic types for dynamic attributes : string, boolean, long and date types.

@author: HD3D
'''

from hd3dlib import conf

def doIt(log):
    
    try:
        from hd3dlib.extra.dynutils import DynUtils
        
        string = DynUtils.createType('text', 'java.lang.String')
        if string.id is not None:
            log.write('Attribut type %s created' % string.name)   
            
        boolean = DynUtils.createType('0/1', 'java.lang.Boolean')
        if boolean.id is not None:
            log.write('Attribut type %s created' % boolean.name)   
            
        long = DynUtils.createType('numeric', 'java.lang.Long')
        if long.id is not None:
            log.write('Attribut type %s created' % long.name)   
            
        date = DynUtils.createType('date', 'java.util.Date')
        if date.id is not None:
            log.write('Attribut type %s created' % date.name)   
   
    
    except Exception, e:
        log.exception(e)
    finally:
        log.stop()
    
    
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
    
    logLocation = conf.dataServerLogPath
    if not os.path.exists(logLocation):
        sys.exit('Log directory does not exist.')
        
    log = Log(str(os.path.basename(__file__)), logLocation)
    log.enableConsole()
    log.start()

    doIt(log)
    
