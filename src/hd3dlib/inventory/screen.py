# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


SCREEN_DEFINITION = {
    'name':'ScreenGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LScreen', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
        "serial" : Attribute('serial', str),
        "billingReference" : Attribute('billingReference', str),
        "purchaseDate" : Attribute('purchaseDate', str),
        "warrantyEnd" : Attribute('warrantyEnd', str),
        "inventoryStatus" : Attribute('inventoryStatus', str),
       
        "qualification" : Attribute('qualification', str),
        "type" : Attribute('type', str),
        "size" : Attribute('size', float),
        
        "screenModel" : Attribute('screenModelId', long),
        "screenModelName" : Attribute('screenModelName', str),

        "resourceGroupIDs" : Attribute('resourceGroupIDs', list, mutable=False),
        "resourceGroupNames" : Attribute('resourceGroupNames', list, mutable=False),
        "computerIDs" : Attribute('computerIDs', list, mutable=False),
        "computerNames" : Attribute('computerNames', list, mutable=False),
    }
}

class Screen(makeClass(SCREEN_DEFINITION, BaseObject)):
    '''
    Screen describes a screen device and its links with computers. A screen can 
    be qualified and is linked to a screen model.
    '''

    _locatorTmpl = Template('/screens/')
    _locatorShort = '/screens/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()


    @classmethod
    def query(cls):
        '''
        Return query object for Screen.
        '''
        return BaseObject.queryClass(cls) 

    @classmethod
    def getByName(cls, name):
        '''
        Retrieve the screen whose the name is equal to name. If the screen
        does not exist, it returns null.
        
        Arguments:
            *name* The name used for filtering.
        '''
        return Screen.query().filter('name', name).getFirst()
    
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Retrieve the screen whose the name is equal to name. If the screen
        does not exist, it create it.
        
        Arguments:
            *name* The name used for filtering.
        '''
        screen = Screen.getByName(name)
        
        if screen is None:
            screen = Screen()
            screen.name = name
            screen.create()
           
        return screen

    def addComputer(self, computerId):
        '''
        Adds a computer to the list of associated computers
        
        Arguments:
            *computerId* The id of the computer
        '''
        
        if computerId not in self.computerIDs:
            self.computerIDs.append(computerId)
