# coding=utf-8

from hd3dlib.baseobject import BaseObject
from hd3dlib.util.classbuild import makeClass, Attribute

from string import Template

SCRIPT_DEFINITION = {'name':'TaskGroupGenerated',
    'attributes':{
        'class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LTaskGroup', 
            mutable=False
        ),
        'name':Attribute('name', str),
        'color':Attribute('color', str),
        'planning':Attribute('planning', long),
        'filter':Attribute('filter', str),
        'startDate':Attribute('startDate', str),
        'endDate':Attribute('endDate', str),
        'duration':Attribute('duration',int),
        'index':Attribute('index',int),
        'taskGroup':Attribute('taskGroupID', long),
        'taskType':Attribute('taskTypeId', long)
    }
}

class TaskGroup(makeClass(SCRIPT_DEFINITION, BaseObject)):
    '''
    Task group is used in plannings to display only a group of tasks rather
    than a big list of tasks. It makes it easier to read.
    '''
    
    _locatorTmpl = Template('/projects/$projects/plannings/$plannings/task-groups/')
    _locatorShort = '/projects/$projects/plannings/$plannings/task-groups/'
    _locator = ''


    def __init__(self, **kw):
        '''
        Constructor set name by default : task_group_ + random key.
        '''        
        BaseObject.__init__(self, **kw)
        if len(kw) is not 0:
            if self.name is None or self.name == '': 
                self.name = "task_group_" + self._jicId
                
        
    
    def _resetLocator(self, project):
        self._locatorTmpl = Template(TaskGroup._locatorTmpl.template)
        self._locator = self._locatorTmpl.safe_substitute(projects=project, 
                                                          plannings=self.planning)
