# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

APPROVAL_NOTE_DEFINITION = {
    'name':'ApprovalNoteGenerated',
    'attributes':{
        '_class':Attribute(
               'class',
               str,
               default = 'fr.hd3d.model.lightweight.impl.LApprovalNote',
               mutable = False
         ),
         'boundEntity':Attribute('boundEntity', long),
         'boundEntityName':Attribute('boundEntityName', str),
         'approvalDate':Attribute('approvalDate', str),
         'status':Attribute('status', str),
         'comment':Attribute('comment', str),
         'approver':Attribute('approver', long),
         'filerevisions':Attribute('filerevisions', list),
         'approvalNoteType':Attribute('approvalNoteType', long),
         'media':Attribute('media', str)
    }
}

class ApprovalNote(makeClass(APPROVAL_NOTE_DEFINITION, BaseObject)):
    ''' 
    Approval note allows user to say if work object is approved or not.
    
    Approval note has a type, useful to not melt producer approval, realisator 
    approvals... 
    
    Approval note is linked to a work object via boundEntity and boundEntityName
    fields. 
    '''

    _locatorTmpl = Template('/approval-notes/')
    _locatorShort = '/approval-notes/'
    _locator = '/approval-notes/'

    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
