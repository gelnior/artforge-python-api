# coding=utf-8
from hd3dlib.baseobject import BaseObject, Query
from hd3dlib.util.classbuild import makeClass, Attribute

from string import Template

ITEM_DEFINITION = {
    'name':'ItemGenerated',
    'attributes':{
        '_class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LItem', 
            mutable=False
        ),
        'name':Attribute('name',str),
        'query':Attribute('query', str),
        'type':Attribute('type', str),
        'metaType':Attribute('metaType', str),
        'renderer':Attribute('renderer', str),
        'editor':Attribute('editor', str),
        'itemGroup':Attribute('itemGroup', long),
        'transformer':Attribute('transformer',long),
        'transformerParameters':Attribute('transformerParameters',str),
    }
}
 

class Item(makeClass(ITEM_DEFINITION, BaseObject)):
    '''
    Item is a column of a sheet. 
    
    Sheet contains item group that contains items. Item group act as folder
    (or column grouper) for items.
    Items are specified by several fields : 
    * query corresponds to the data field to display. 
    * type is the java language type of displayed data. 
    * metaType corresponds to service type : attribute, method...
    
    Item can corresponds to data field or method (calculated values). 
    '''
    
    _locatorTmpl = Template('/sheets/$sheets/item-groups/$itemgroups/items/')
    _locatorShort = '/sheets/0/item-groups/0/items/'
    _locator = ''
    _sheet = None
    _itemGroup = None
            
    def __init__(self, sheet=None, **kw):
        BaseObject.__init__(self,**kw)
        self._sheet = sheet
        
        if self._sheet is not None and self.itemGroup is not None:
            self._resetLocator()
        
    def _resetLocator(self):
        self._locatorTmpl = Template(Item._locatorTmpl.template)
        self._locator = self._locatorTmpl.safe_substitute(itemgroups=self.itemGroup, sheets=self._sheet)
        
    
    @classmethod
    def requestQuery(cls):
        '''
        Because of the query field of item object, the query() method is renamed in requestQuery(). 
        requestQuery() does exactly the same as query().
        '''
        return Query(cls)
    
    @classmethod
    def getOrCreate(cls,name,itemGroup,query,type,metaType,renderer,editor,transformerParameters):
        '''
        Return the item with the given name and the given item group.
        It creates it if the item does not already exists.
        
        Arguments:
            *name* item group name.
            *itemGroup* the item group of the item
            *query* the query of item
            *metaType* the meta type of the item
            *renderer* the renderer of the item
            *editor* the editor of the item
            *transformerParameters* the transformer parameters of the item
        '''
        item=Item.getByName(name,itemGroup)
        
        if item is None:
            item = Item()
            item.name = name.encode()
            item.sheet = itemGroup.sheet
            item.itemGroup = itemGroup.id
            item.query = query 
            item.type = type 
            item.metaType = metaType
            item.renderer = renderer
            item.editor = editor
            item.transformerParameters=transformerParameters
            item._resetLocator()
            item.create()
        
        itemGroup._resetLocator()
        return itemGroup
    
    @classmethod
    def getByName(cls,name,itemGroup):
        '''
        Return the item with the given name and the given item group.
        
        Arguments:
            *name* Name of item to retrieve
            *itemGroup* the item group of item to retrieve
        ''' 
        substitutes=dict()
        substitutes["sheets"]=itemGroup.sheet
        substitutes["itemgroups"]=itemGroup.id
        
        return Item.requestQuery().branch("sheets",itemGroup.sheet).branch("item-groups",itemGroup.id).filter('name', name).getFirst()
    
    @classmethod
    def getByItemGroup(cls,itemGroup):
        '''
        Return items with the given item group.
        
        Arguments:
            *itemGroup* the item group of item to retrieve
        ''' 
        substitutes=dict()
        substitutes["sheets"]=itemGroup.sheet
        substitutes["itemgroups"]=itemGroup.id
        
        return Item.requestQuery().branch("sheets",itemGroup.sheet).branch("item-groups",itemGroup.id).getAll()
    