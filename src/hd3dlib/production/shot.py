# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from hd3dlib.asset import ImageFileSystem
from hd3dlib.production import Composition, Constituent
from hd3dlib.asset.assetgroup import AssetGroup

from string import Template

SHOT_DEFINITION = {
    'name':'ShotGenerated',
    'attributes':{
        '_class' : Attribute(
             'class',
             str,
             default = 'fr.hd3d.model.lightweight.impl.LShot'
        ),
        'simpleClassName' : Attribute('simpleClassName', str),
        'label' : Attribute('label', str),
        'hook' : Attribute('hook', str),
        'description' : Attribute('description', str),
        'sequence' : Attribute('sequence', long),
        'difficulty' : Attribute('difficulty', int, default = 0),
        'completion' : Attribute('completion', int, default = 0),
        'trust' : Attribute('trust', int, default = 0),
        'mattePainting' : Attribute('mattePainting', bool, default = False),
        'layout' : Attribute('layout', bool, default = False),
        'tracking' : Attribute('tracking', bool, default = False),
        'dressing' : Attribute('dressing', bool, default = False),
        'animation' : Attribute('animation', bool, default = False),
        'export' : Attribute('export', bool, default = False),
        'lighting' : Attribute('lighting', bool, default = False),
        'rendering' : Attribute('rendering', bool, default = False),
        'compositing' : Attribute('compositing', bool, default = False),
        'nbFrame' : Attribute('nbFrame', long, default = long(0)),
        'startFrame' : Attribute('startFrame', long, default = long(0)),
        'endFrame' : Attribute('endFrame', long, default = long(0))
     }
}


class Shot(makeClass(SHOT_DEFINITION, BaseObject)):
    '''
    Shot is the base element of project temporal aspect. Movies are made of
    sequences which are made of shots.
    
    Tasks and assets are linked to shot (a shot is a work object).
    '''

    #project = None
    _simpleclassname = 'Shot'
    _locatorTmpl = Template('/projects/$projects/sequences/$sequences/shots/')
    _locatorShort = '/shots/'
    _locator = "/shots/"

    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)

        if len(kw) is not 0:
            if self.label is None or self.label == '':
                self.label = "Shot " + self._jicId


    def _resetLocatorWithProject(self, project):
        '''
        Reset locator URL depending of project, following this pattern :
        projects/projectId/shots/
        
        Arguments:
            *project* The project used for building locator.
        '''
        self._locator = self._locatorTmpl.safe_substitute(projects = str(project.id))


    def _resetLocatorWithProjectAndSequence(self, project, sequence):
        '''
        Reset locator URL depending of project, following this pattern :
        projects/projectId/sequences/sequenceId/shots/
        
        Arguments:
            *project* The project used for building locator.
            *sequence* The sequence used for building locator.
        '''
        self._locator = self._locatorTmpl.safe_substitute(projects = str(project.id),
                                                          sequences = str(sequence.id))


    def getCasting(self, project):
        '''
        Return the shot casting, it means all constituents linked to shot via 
        compositions. 
        
        Arguments:
            *project* The project by which current is owned.
        '''
        compositions = Composition.getByShot(project.id, self.sequence, self.id)
        constituentIds = list()

        for composition in compositions:
            constituentIds.append(composition.constituent)

        if(len(constituentIds) > 0):
            req = Constituent.query().branch('projects', project.id)
            req = req.filter_custom('in', 'id', constituentIds)
            return req.getAll()
        else:
            return list()



    def setThumbnail(self, path):
        '''
        Set thumbnail for current shot with file located at *path*.
        
        Arguments:
           *path* The path of the file to set as thumbnail. 
        '''
        if self.id is not None:
            fs = ImageFileSystem()
            fs.setThumbnail('shots/', path, self.id)

    def getAssets(self, taskTypeId = -1):
        '''
        Return assets linked to current shot.
        '''
        assetGroups = AssetGroup.getAllByShot(self)
        assets = []
        if assetGroups:
            for assetGroup in assetGroups:
                assets += assetGroup.getAssets(taskTypeId)

        return assets

    def getFiles(self, taskType = ""):
        '''
        Return files linked to current constituent (via its assets). 
        
        *NB: Assets are retrieved too.*
        
        Arguments:
           *taskType* The taskType of the asset.
        '''
        assets = self.getAssets()

        files = list()
        if assets is not None and len(assets) > 0:
            for asset in assets:
                if taskType == "" or asset.taskTypeName == taskType:
                    files.extend(asset.getFiles())

        return files

    @classmethod
    def getByName(cls, project, sequence, name):
        '''
        Return the shot of which name is equal to name.         
        It creates the shot if it does not already exist.
        
        Arguments:
            *project* Project in which shot will appear.
            *sequence* Sequence in which shot will appear.
            *name* Name of the shot to create or retrieve.
        '''
        if isinstance(name, str):
            name = name.decode("utf-8").encode("utf-8")
        if isinstance(name, unicode):
            name = name.encode("utf-8")

        req = cls.query().branch('projects', project.id)
        req = req.branch('sequences', sequence.id).filter('label', name)
        obj = req.getFirst()

        return obj


    @classmethod
    def getOrCreateByName(cls, project, sequence, name):
        '''
        Return the shot of which name is equal to name.         
        It creates the shot if it does not already exist.
        
        Arguments:
            *project* Project in which shot will appear.
            *sequence* Sequence in which shot will appear.
            *name* Name of the shot to create or retrieve.
        '''
        obj = cls.getByName(project, sequence, name)

        if obj is None:
            obj = cls(label = name)
            obj.sequence = sequence.id
            obj._resetLocatorWithProjectAndSequence(project, sequence)
            obj.create()

        return obj


    @classmethod
    def getDynByName(cls, project, sequence, name):
        '''
        Return the shot of which name is equal to name.         
        It creates the shot if it does not already exist.
        
        Arguments:
            *project* Project in which shot appears.
            *sequence* Sequence in which shot appears.
            *name* Name of the shot to retrieve.
        '''

        query = cls.query().branch('projects', project.id)
        query = query.branch('sequences', sequence.id).filter('label', name)
        obj = query.dyn().getFirst()

        return obj

    @classmethod
    def getByProject(cls, projectId):
        '''
        Return shots from a project.         
        
        Arguments:
            *projectId* the project id in which shots will appear.
        '''
        return Shot.query().setLocator("/shots/").filter('sequence.project.id', projectId).getAll()

    @classmethod
    def query(cls):
        '''
        Return query object for shot.
        '''
        return BaseObject.queryClass(cls)
