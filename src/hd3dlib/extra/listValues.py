from hd3dlib.util.classbuild import Attribute, makeClass
from string import Template
from hd3dlib.baseobject import BaseObject

LIST_VALUES_DEFINITION = {
    'name':'ListValues',
    'attributes':{
        '_class':Attribute('class', str, default='fr.hd3d.model.lightweight.impl.LListValues', mutable=False),
        'separator':Attribute('separator', str),
        'values':Attribute("values", str)
    }
}

class ListValues( makeClass(LIST_VALUES_DEFINITION, BaseObject) ):
    '''
    ListValues represent the values of a dynamic attribut whose 'nature' is java.util.List
    '''
    _locatorTmpl = Template('/lists-values/')
    _locatorShort = '/lists-values/'
    _locator = '/lists-values/'
    
    
    def __init__ (self, **kw):
        BaseObject.__init__(self, **kw)


    @classmethod
    def query(cls):
        '''
        Return query object for ListValues.
        '''
        return BaseObject.queryClass(cls) 
