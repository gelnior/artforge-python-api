# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.asset import AssetGroup
from hd3dlib.production import Shot, Constituent
from hd3dlib.baseobject import BaseObject
from hd3dlib.task.taskactivity import TaskActivity


from string import Template

SCRIPT_DEFINITION = {
    'name':'TaskGenerated',
    'attributes':{
        '_class':Attribute(
           'class', 
           str, 
           default='fr.hd3d.model.lightweight.impl.LTask', 
           mutable=False
       ),           
       'name':Attribute('name', str),
       'status':Attribute('status', str),
       'completion':Attribute('completion', int),
       'project':Attribute('projectID', long),
       'projectName':Attribute('projectName', str),
       'projectColor':Attribute('projectColor', str),
       'creator':Attribute('creatorID', long),
       #'creatorName':Attribute('creatorName', str),
       'worker':Attribute('workerID', long),
       'actualStartDate':Attribute('actualStartDate', str ),
       'actualEndDate':Attribute('actualEndDate', str ),
       'startDate':Attribute('startDate', str),
       'endDate':Attribute('endDate', str),
       'startable':Attribute('startable', bool),
       'taskType':Attribute('taskTypeID', long),
       'duration':Attribute('duration', long),
       'extraLine':Attribute('extraLineID', long),
       'taskTypeName':Attribute('taskTypeName',str),
       'workObjectParentsNames':Attribute('workObjectParentsNames',str),
       'workObjectName':Attribute('workObjectName',str),
       'boundEntityName':Attribute('boundEntityName', str),
       'workObjectId':Attribute('workObjectId',long)
    }
}


class Task(makeClass(SCRIPT_DEFINITION, BaseObject)):
    '''
    Task do what its name says, it represents a task a worker should do on
    a work object. It is the heart object of the task manager, planning
    and time sheets. 
    '''
    
    _locatorTmpl = Template('/projects/$projects/tasks/')
    _locatorShort = '/tasks/'
    _locator = '/tasks/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self.completion = 0
        self.startable = True
        
        if self.project is not None:
            self._resetLocator()

    def _resetLocator(self):
        self._locatorTmpl = Template(Task._locatorTmpl.template)
        self._locator = self._locatorTmpl.safe_substitute(projects=self.project)
        
    @classmethod 
    def getByName(cls, project, name, dyn=False):
        '''
        Return task corresponding to name. If task does not exists, None is 
        returned.
        
        Arguments:
            project The task project
            name The task name
            dyn True if dynamic attributes should be retrieved.
        '''
        serviceQuery = cls.query()
        if (dyn) :
            serviceQuery.dyn()
        query = serviceQuery.branch('projects', project.id)
        obj = query.filter('name', name).getFirst()
            
        return obj    
                
    @classmethod
    def GetTaskByWorker(cls, worker):
        '''
        Return tasks corresponding assigned to worker.
        
        Arguments:
            *worker* The worker whose tasks are retrieved.
        '''
        query = cls.query()
        query.locTemplate='/tasks/'
        list = query.filter('worker.id', worker.id).getAll()

        return list
    
    @classmethod
    def getTaskByWOTasktype(cls, workobject, boundEntityName, taskType):
        '''
        Return tasks corresponding to work object and task type.
        
        Arguments:
           *workobject* The work object whose tasks are retrieved.
            *boundEntityName* the boundEntityName of the workObject whose tasks are retrieved.
            *taskType* the task type whose tasks are retrieved.
        '''
        query = cls.query()
        query.locTemplate='/tasks/'
        list = query.filter('boundEntityTaskLinks.boundEntity',workobject.id) \
            .filter('boundEntityTaskLinks.boundEntityName',boundEntityName) \
            .filter('taskType',taskType.id).getAll()
        
        return list

    @classmethod
    def getTasksByWorkObject(cls, workobject, boundEntityName):
        '''
        Return tasks described work object.
        
        Arguments:
            *workobject* The work object whose tasks are retrieved.
            *boundEntityName* the boundEntityName of the workObject whose tasks are retrieved.
            *taskType* the task type whose tasks are retrieved.
        '''        
        if not workobject.id:
            raise Exception("Cannot retrieve tasks linked to constituent if object has no ID.")
            
        query = cls.query()
        query.locTemplate='/tasks/'
        query = query.filter('boundEntityTaskLinks.boundEntityName', boundEntityName)
        query = query.filter('boundEntityTaskLinks.boundEntity', workobject.id)
        
        return query.getAll()
    
    def getActivities(self):        
        '''
        Return all activities linked to task.
        '''
        req = TaskActivity.query()
        req = req.filter('task.id', self.id)
        
        return req.getAll()

    
    def getAssets( self ):
        '''
        Return assets linked to current task.
        '''

        assetGroups = None
        if ( self.boundEntityName  == Shot._simpleclassname ):
            shot = Shot.get( self.workObjectId )
            assetGroups = AssetGroup.getByShot( shot )
        else:
        #if ( boundEntityName  == Constituent._simpleclassname ):
            constituent = Constituent.get( self.workObjectId )
            assetGroups = AssetGroup.getAllByConstituent( constituent )
        assets = []
        for assetGroup in assetGroups:
            assets += assetGroup.getAssets( self.taskType )
       
        return assets
