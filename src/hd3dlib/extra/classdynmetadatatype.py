# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

CLASS_DYN_METADATA_TYPE_DEFINITION = {
    'name':'ClassDynMetaDataTypeGenerated',
    'attributes':{
        '_class': Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LClassDynMetaDataType', 
            mutable=False
        ),
        'name':Attribute('name', str),
        'dynMetaDataType':Attribute('dynMetaDataType', long),
        'predicate':Attribute('predicate',str),
        'className':Attribute('className',str),
        'extra':Attribute('extra',str)
    }
} 


class ClassDynMetaDataType(makeClass(CLASS_DYN_METADATA_TYPE_DEFINITION, BaseObject)):
    '''
    ClassDynMetaDataType corresponds to the dynamic attribute set on a class.
    '''
    
    _locatorTmpl = Template('/class-dyn-metadata-types/')
    _locatorShort = '/class-dyn-metadata-types/'
    _locator = '/class-dyn-metadata-types/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)


    @classmethod
    def query(cls):
        '''
        Return query object for ClassDynMetaDataType.
        '''
        return BaseObject.queryClass(cls)
