# coding=utf-8

from hd3dlib import conf

                          
def cleanLogFile(logPath, cleanMessage = ''):
    '''
    Delete all log files.
    '''
    logPath = os.path.normpath(logPath)
    f = open(logPath, 'w')
    f.write(cleanMessage)
    f.close()
    log.write('Clean ' + logPath)
            
              
def doIt(args, log, allLogLocation):
    event = args['event']
    
    logList = []
    if event is 'all':
        tmpList = os.listdir(allLogLocation)
        for el in tmpList:
            logLocation = os.path.join(allLogLocation, el)
            logList.append(logLocation)
    else:
        relativePath = event + '.txt'
        logLocation = os.path.join(allLogLocation, relativePath)
        logList.append(logLocation)
            
    cleanMessage = log.getCleanLogMessage()
    for logPath in logList:
        if logPath is not 'cleanScriptLogs.txt':
            cleanLogFile(logPath, cleanMessage)
    
    
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
    
    log = None
    
    try:
        allLogsLocation = os.path.normpath(conf.dataServerLogPath)
        print 'All logs location : ' + allLogsLocation
        if not os.path.exists(allLogsLocation):
            raise ValueError('Log directory does not exist.')
            
        log = Log(str(os.path.basename(__file__)), allLogsLocation)
        log.enableConsole()
        log.start()
        
        log.write('Arguments are : ' + str(sys.argv))
        args = reflex.evalArgDictionary(sys.argv[1])
        
        if not isinstance(args, dict):
            raise ValueError('Malformed arguments dictionnary.')
        
        log.write('Before I process everything.')
        doIt(args, log, allLogsLocation)
        log.write('After I processed everything.')
        
    except Exception, e:
        if log is not None:
            log.write('Error: ' + str(e.message) + ' '  + str(type(e)))
        else:
            print 'Error:', e.message, type(e)
    finally:
        if log is not None:
            log.stop()
