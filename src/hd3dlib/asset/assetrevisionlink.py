# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

ASSETREVISIONLINK_DEFINITION = {
    'name':'AssetRevisionLinkGenerated',
    'attributes':{
        '_class':Attribute(
            'class',
            str,
            default='fr.hd3d.model.lightweight.impl.LAssetRevisionLink'
        ),
        'inputAsset':Attribute('inputAsset', long),
        'outputAsset':Attribute('outputAsset', long),
        'type':Attribute('type', str),
    }
}



class AssetRevisionLink(makeClass(ASSETREVISIONLINK_DEFINITION, BaseObject)):
    '''
    AssetRevisionLink represents link between assets. Asset link shows 
    dependency between assets. It is an oriented link.
    
    Ex: Texturing asset depends on Modeling asset.
    '''
    
    _locatorTmpl = Template('/asset-revision-links/')
    _locatorShort = '/asset-revision-links/'
    _locator = '/asset-revision-links/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        if self.inputVariation == 'None':
            self.inputVariation = ''
        if self.outputVariation == 'None':
            self.outputVariation = ''


    @classmethod
    def getByData(cls, inputKey, inputVariation, inputRevision, outputKey,
                    outputVariation, outputRevision):
        '''
        Retrieve asset link corresponding to parameters. If asset link does not
        exist, None is returned.
        '''
        query = AssetRevisionLink.query().filter('inputKey', inputKey)
        query = query.filter('inputVariation', inputVariation)
        query = query.filter('inputRevision', inputRevision)
        query = query.filter('outputKey', outputKey)        
        query = query.filter('outputVariation', outputVariation)
        query = query.filter('outputRevision', outputRevision)
        link = query.getFirst()
        
        return link


    @classmethod    
    def getByAssets(cls, inAsset, outAsset, type=""):
        '''
        Return asset link from *inAsset* to *outAsset*. If asset link does not
        exist, None is returned.
        
        Arguments:
            inAsset The asset from where comes the link.
            outAsset The asset to where the link goes.
        '''
        
        query = AssetRevisionLink.query().filter('inputAsset', inAsset.id)
        return query.filter('outputAsset', outAsset.id).getFirst()

    
    @classmethod        
    def getOrCreateByAssets(cls, inAsset, outAsset, type=""):
        '''
        Return asset link from *inAsset* to *outAsset*. If asset link does not
        exist, None is returned.
        
        Arguments:
            inAsset The asset from where comes the link.
            outAsset The asset to where the link goes
        '''
        query = AssetRevisionLink.query().filter('inputAsset', inAsset.id)
        assetRevisionLink = query.filter('outputAsset', outAsset.id).getFirst()
        if not assetRevisionLink :
            assetRevisionLink = AssetRevisionLink(
                inputAsset = inAsset.id, 
                outputAsset = outAsset.id, 
                type = type
            )
            assetRevisionLink.create()
            
        return assetRevisionLink 
    

    @classmethod
    def query(cls):
        return BaseObject.queryClass(cls)  
