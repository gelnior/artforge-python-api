# coding=utf-8

from hd3dlib.util.randomid import generateRandomId

import os, re
import zipfile
import xml.dom.minidom

# Xml namespace for open document table tags
TABLE_NAMESPACE = 'urn:oasis:names:tc:opendocument:xmlns:table:1.0'

class ODSpreadsheet(object):
    '''
    ODSSpreasheet is a tool to parse data from Open Document Spreadsheet.
    '''
    def __init__(self, path):
        if not os.path.isfile(path):
            raise ValueError, 'ODS file not found.'
        else:
            # Temporarily unzip file to get data
            zip_data = zipfile.ZipFile(path, 'r')
            fileContent = zip_data.read('content.xml')
            self.content = xml.dom.minidom.parseString(fileContent)
            # It is important to search using namespace
            self.sheets = self.content.getElementsByTagNameNS(TABLE_NAMESPACE, 'table')    
            zip_data.close()
            
        
    def filterSheetList(self, sheetsOfInterest):
        selectedSheets = []
        selectedSheetNames = []
        for sheet in self.sheets:
            currSheetName = sheet.getAttribute('table:name')
            if currSheetName.lower() in ([(name.lower()) for name in sheetsOfInterest]):
                selectedSheets.append(sheet)
                selectedSheetNames.append(currSheetName)
        self.sheets = selectedSheets
        return selectedSheetNames
        
        
    def showSheetList(self):
        for sheet in self.sheets:
            currSheetName = sheet.getAttribute('table:name')
            print currSheetName
        
        
    def getNodeContent(self, node):
        text = ''
        for child in node.childNodes:
            ## Skip notes
            if child.nodeName == 'office:annotation':
                continue
            if child.nodeType == child.ELEMENT_NODE:
                if text:
                    text = '%s&&&&%s' % (text, self.getNodeContent(child))
                else:
                    text = self.getNodeContent(child)
            elif child.nodeType == child.TEXT_NODE:          
                if text:
                    text = '%s&&&&%s' % (text, unicode(child.nodeValue))
                else:
                    text = unicode(child.nodeValue)
        return text.rstrip().lstrip()

    def getTitlesArr(self, sheetName):
        isSet = False
        for currSheet in self.sheets:
            currSheetName = currSheet.getAttribute('table:name')
            if currSheetName.lower() == sheetName.lower():
                sheet = currSheet
                isSet = True
        if not isSet:
            raise ValueError, 'Requested sheet in ODS file not found.'
        rows = sheet.getElementsByTagNameNS(TABLE_NAMESPACE, 'table-row')
        #Title row mining
        attrArr = {}
        cnt = 0
        firstRow = rows[0]
        cells = firstRow.getElementsByTagNameNS(TABLE_NAMESPACE, 'table-cell')
        for cell in cells:
            cellContent = self.getNodeContent(cell)
            #TODO : Make sure this is a valid column name (regarding the sheet, other column names, etc.)
            if cellContent:
                attrArr[cellContent]=cnt
            cnt = cnt + 1        
        return attrArr

    def getRecords(self, sheetName):
        isSet = False
        for currSheet in self.sheets:
            currSheetName = currSheet.getAttribute('table:name')
            if currSheetName.lower() == sheetName.lower():
                sheet = currSheet
                isSet = True
        if not isSet:
            raise ValueError, 'Requested sheet in ODS file not found.'
        rows = sheet.getElementsByTagNameNS(TABLE_NAMESPACE, 'table-row')
        
        #Title row mining
        attrlist = {}
        cnt = 0
        firstRow = rows[0]
        cells = firstRow.getElementsByTagNameNS(TABLE_NAMESPACE, 'table-cell')
        for cell in cells:
            cellContent = self.getNodeContent(cell)
            #TODO : Make sure this is a valid column name (regarding the sheet, other column names, etc.)
            if cellContent:
                attrlist[cellContent]=cnt
            cnt = cnt + 1
            
        #Other rows' mining
        records = []
        for cnt in range(1, len(rows)):
            row = rows[cnt]
            cells = row.getElementsByTagNameNS(TABLE_NAMESPACE, 'table-cell')
            if not cells: continue
            rowSize = len(cells)
            if rowSize < 1: continue
            newRow= {}
            isEmptyRow = True
            for key in attrlist:
                index = attrlist[key]
                if index > rowSize-1:
                    cellContent = ''
                else:
                    cell = cells[index]
                    cellContent = self.getNodeContent(cell)
                    if re.compile('&&&&').search(cellContent):
                        #cell content is a list
                        cellContent = cellContent.split('&&&&')
                    if cellContent is not '':
                        isEmptyRow = False
                newRow[key] = cellContent
            if isEmptyRow is not True:
                newRow['uuid'] = str(generateRandomId(newRow,12))
                records.append(newRow)
        return records
    