# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from hd3dlib.production.shot import Shot

from string import Template

SEQUENCE_DEFINITION = {
    'name':'SequenceGenerated',
    'attributes':{
        '_class':Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LSequence', 
            mutable = False
        ),
        'simpleClassName' : Attribute('simpleClassName',str),
        'name':Attribute('name', str),
        'title':Attribute('title', str),                                     
        'hook':Attribute('hook', str),
        'parent':Attribute('parent', long),
        'project':Attribute('project', long)
    }
}

class Sequence(makeClass(SEQUENCE_DEFINITION, BaseObject)):
    '''
    Sequence allows to organize shots in folders. 
    
    Shot act as a folder in the constituent navigation tree.
    
    Sequence has also a real functional meaning: it corresponds to the sequence
    of a movie.
    '''
    
    _simpleclassname='Sequence'
    _locatorTmpl = Template('/projects/$projects/sequences/')
    _locatorShort = '/projects/0/sequences/'
    _locator = ''
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)

        if len(kw) is not 0:
            if self.name is None or self.name == '': 
                self.name = "Sequence " + self._jicId
                
    
    def _resetLocator(self):
        '''
        Reset locator depending on project set in sequence
        '''
        self._locator = self._locatorTmpl.safe_substitute(projects=self.project)
            

    def getShots(self):
        '''
        Return all shots contained in current sequence.
        '''    
        query = Shot.query() 
        query = query.branch('projects', self.project)
        query = query.branch('sequences', self.id)
        
        return query.getAll()
    
    @classmethod
    def getByName(cls, project, name):   
        '''
        Return the sequence of which name is equal to name. If sequence does
        not exist None is returned.
        
        Arguments: 
            *project* Project at which sequence owns.
            *name* Name of the sequence to retrieve.
        ''' 
        obj = cls.query().branch('projects', project.id).leaf('all').filter('name', name).getFirst()
            
        return obj
    

    @classmethod
    def getOrCreateByName(cls, project, name):   
        '''
        Return the sequence of which name is equal to name. If sequence does
        not exist, it is created.
        
        Arguments: 
            *project* Project at which sequence owns.
            *name* Name of the sequence to retrieve or create.
        ''' 
        obj = cls.getByName(project, name)
        
        if obj is None:        
            obj = cls(name=name)
            obj.project = project.id
            obj._resetLocator()
            obj.create()
        
        return obj
    
    @classmethod
    def getByProject(cls, projectId):
        '''
        Return sequences from a project.         
        
        Arguments:
            *projectId* the project id in which sequences are.
        '''
        return Sequence.query().branch('projects', projectId).getAll()
    

    @classmethod
    def query(cls):
        '''
        Return query object for Sequence.
        '''
        return BaseObject.queryClass(cls) 