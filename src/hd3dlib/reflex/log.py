# coding=utf-8

import os 
import time
import traceback


class Log(object):
    '''
    Log provides utility to write Log in console and inside files.
    Files are located in the dataServerLogPath variable from conf.py.
    '''
    def __init__(self, scriptName = None, folderPath = None, relativePath = None):
        '''
        Constructor
        Arguments:
            *scriptName* Log file will be scriptName.txt
            *Folder Path*  Relative path to access log files.
            *Relative Path*  Relative path to access log files.
        ''' 
        self.script = scriptName
        if relativePath is None:
            relativePath = scriptName.split(".")[0] + '.txt'
        folderPath = os.path.normpath(folderPath)
        relativePath = os.path.normcase(relativePath)
        self.filepath = os.path.join(folderPath, relativePath)
        self.isConsoleEnabled = False
        
     
    def getCleanLogMessage(self):
        '''
        Return a string indicating that log file was cleaned with current time.
        '''        
        now = time.localtime(time.time())
        header = time.strftime(u"%d/%m %H:%M:%S", now)
        message = header + u"   " +  u'----   LAST LOG FILE CLEANING   ----'
        message += u'\n'
        return message        


    def write(self, message = u'Empty log message'):
        '''
        Write *message* to message to log file. If *self.isConsoleEnabled* is set to
        True, it writes it to the console too.
        
        Arguments:
            *message* The message to write.
        '''
        
        now = time.localtime(time.time())
        header = time.strftime(u"%d/%m %H:%M:%S", now)
        message = header + u"   " + message.decode('utf-8')
        if( os.path.exists(self.filepath)):
            file = open(self.filepath, "a")  
        else:
            file = open(self.filepath, "w")
        file.write(message.encode('utf-8', "ignore"))
        file.write("\n".encode('utf-8', "ignore"))
        file.close()
        if self.isConsoleEnabled: print message.encode('utf-8', "ignore")


    def newLine(self):
        '''
        Add a new line in log file and write to the console if isConsoleEnabled
        is set at True.
        '''
        if( os.path.exists(self.filepath)):
            file = open(self.filepath, "a")  
        else:
            file = open(self.filepath, "w")
        file.write("\n".encode('utf-8', "ignore"))
        file.close()
        if self.isConsoleEnabled: print ''

    
    def start(self):
        '''
        Add a start message containing script name in log file and write to the 
        console if isConsoleEnabled is set at True.
        '''
        
        message = u'--- Script START (' + self.script + ')'
        self.write(message)
    
        
    def stop(self):
        '''
        Add a done message containing script name in log file and write to the 
        console if isConsoleEnabled is set at True.
        '''
        
        message = 'Done (' + self.script + ')'
        self.write(message)
        self.newLine()
        
        
    def addEmptyLine(self):
        '''
        Add an empty line in log file and write to the console if isConsoleEnabled
        is set at True.
        '''
        
        if( os.path.exists(self.filepath)):
            file = open(self.filepath, "a")  
        else:
            file = open(self.filepath, "w")
        file.write("\n".encode('utf-8', "ignore"))
        file.close()
        if self.isConsoleEnabled: print ""
    
        
    def info(self, msg):
        '''
        Write a message with [INFO] tag at the beginning.
        
        Arguments:
            *msg* The message to write.
        '''
        self.write('[INFO] ' + msg)
    
    
    def warn(self, msg):
        '''
        Write a message with [WARN] tag at the beginning.
                
        Arguments:
            *msg* The message to write.    
        '''
        self.write('[WARN] ' + msg)
    
    
    def error(self, msg):
        '''
        Write a message with [ERROR] tag at the beginning.
                
        Arguments:
            *msg* The message to write.    
        '''
        self.write('[ERROR] ' + msg)
    
    
    def errorInfo(self, msg):
        '''
        Write a message with [ERROR-INFO] tag at the beginning.
                
        Arguments:
            *msg* The message to write.    
        '''
        self.write('[ERROR-INFO] ' + msg) 
        
        
    def traceback(self, exception):
        '''
        Write exception stack trace with [ERROR-INFO] tag at the beginning.   
                
        Arguments:
            *exception* The exception of which stack trace will be written.    
        '''
        self.errorInfo(traceback.format_exc(exception))
        
    # 
    def exception(self, exception):    
        '''
        Write exception message with [ERROR] tag then write stack trace with
        [ERROR-INFO] tag at the beginning.
                
        Arguments:
            *exception* The exception of which stack trace and error
            message  will be written.    
        '''
        self.error(str(exception))        
        self.traceback(exception)
       
        
    # 
    def enableConsole(self):
        '''
        This enables log message writing to the console.
        '''
        self.isConsoleEnabled = True
        
        
    def disableConsole(self):
        '''
        This disables log message writing to the console.
        '''
        self.isConsoleEnabled = False
    
    