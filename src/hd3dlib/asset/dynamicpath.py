# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute

from hd3dlib.baseobject import BaseObject



from string import Template

DYNAMICPATH_DEFINITION = {
        'name':'DynamicPathGenerated',
        'attributes':{
            '_class':Attribute(
                'class',str,
                default='fr.hd3d.model.lightweight.impl.LDynamicPath'
            ),
            'name':Attribute('name', str),
            'projectId':Attribute('projectId', long),
            'taskTypeId':Attribute('taskTypeId', long),
            'taskTypeName':Attribute('taskTypeName', str),
            'taskTypeColor':Attribute('taskTypeColor', str),
            'workObjectId':Attribute('workObjectId', long),
            'boundEntityName':Attribute('boundEntityName', str),
            'wipDynamicPath':Attribute('wipDynamicPath', str),
            'publishDynamicPath':Attribute('publishDynamicPath', str),
            'oldDynamicPath':Attribute('oldDynamicPath', str),
            'type':Attribute('type',str),
            'parent':Attribute('parent', long),
        }
}

class DynamicPath(makeClass(DYNAMICPATH_DEFINITION, BaseObject)):
    '''
    DynamicPath allows to retrieve dynamic path list. This dynamic path linked to work object and project. That defines a path with variables where store the file of work object.
    '''
    
    _locatorTmpl = Template('/dynamic-paths/')
    _locatorShort = '/dynamic-paths/'
    _locator = ''
    
    type_file="file"
    type_proxy="proxy"
    type_thumbnail="thumbnail"
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)

    @classmethod        
    def getByProjectAndType(cls,projectId,type):
        '''
        Returns dynamic paths from a project.
        
         Arguments :
            *projectId* The project id to retrieved dynamic paths.
            *type* the type of dynamic path in ('file','thumbnail','proxy').
        '''
        
        return DynamicPath.query().filter('project', projectId).filter('type', type).getAll()
    
    