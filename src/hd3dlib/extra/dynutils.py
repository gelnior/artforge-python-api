# coding=utf-8

from hd3dlib.extra import ClassDynMetaDataType
from hd3dlib.extra import DynMetaDataValue
from hd3dlib.extra import DynMetaDataType
from hd3dlib.extra.listValues import ListValues
import string
    
class DynUtils(object): 
    '''
    Class with class methods for handling dynamic attributes.
    
    Dynamic value chain :
    instance <-> Dyn value <-> Dyn Attribute <-> Dyn type 
    '''
    
         
    @classmethod     
    def getType(cls, name):
        '''
        Return first type corresponding to name.
        '''    	
        return DynMetaDataType.query().filter('name', name).getFirst()
    

    @classmethod    
    def createType(cls, name, type):
        '''
        Create type if it does not already exist. Return the created type.
        
        Arguments:
            *name* The type name
            *type* The equivalent type for Java language.
        '''
        dynType = DynMetaDataType.query().filter('name', name).getFirst()
    
        if dynType is None:        
            dynType = DynMetaDataType(name=name, type=type, nature='simple')
            dynType.create()  
        
        return dynType
   
    @classmethod    
    def createListType(cls, name, valuesType, values, separator, defaultValue):
        '''
        Create a list type if it does not already exist. Return the created type.
        
        Arguments:
            *name* The list name
            *valuesType* The equivalent type for Java language.
            *values* An array with all the values to put in the attribut list list.
            *separator* The character to separate each value
            *defaultValue* The default value of the list
        '''
        if name is None :
            raise ValueError('name must not be None')
        if valuesType is None :
            raise ValueError('valuesType must not be None')
        if values is None :
            raise ValueError('values must not be None')
        if separator is None :
            raise ValueError('separator must not be None')
        
        if not isinstance(values, list):
            raise ValueError('values must be a list')
        
        dynType = DynMetaDataType.query().filter('name', name).getFirst()
    
        if dynType is None:        
            # Creates the ListValue object
            valueString = string.join(values, separator);
            listValues = ListValues(values = valueString, separator = separator);
            listValues.create();
            if listValues is not None and listValues.id is not None:
                dynType = DynMetaDataType(name=name, 
                                          type=valuesType, 
                                          nature='java.util.List', 
                                          structName="ListValues", 
                                          structId=listValues.id)
                if defaultValue is not None:
                    dynType.defaultValue = defaultValue
                dynType.create()  
        
        return dynType
    
     
    @classmethod     
    def createAttribute(cls, name, type, className):
        '''
        Create attribute for *className* if it does not already exist. 
        Return the created attribute.
        
        Arguments:
            *name* The attribute name.
            *type* The attribute type.
            *className* The class name.
        '''
        dynClass = ClassDynMetaDataType.query().filter('name', name).getFirst()
        
        if dynClass is None:        
            dynClass = ClassDynMetaDataType(name=name, 
                                            dynMetaDataType=type.id, 
                                            className=className, 
                                            predicate='*')
            dynClass.create()
            
        return dynClass
    
    
    @classmethod
    def createAttributeValue(cls, attribute, type, className, object):
        '''
        Create attribute value of *attribute* for *object*. Return the 
        created value.
        If *attribute* does not exist, it is created. 
        
        Arguments:
            *attribute* The attribute name.
            *type* The attribute type.
            *className* The object class name.
            *object* The object on which value will be set.
        '''
        attribute = DynUtils.createAttribute(attribute, type, className)
        DynUtils.createValue(className, attribute, object)
        
        return attribute


    @classmethod
    def createValue(cls, className, attribute, object):
        '''
        Create attribute value of *className* for *object*.  
        Return the created attribute.
        
        Arguments:
            *className* The object class name.
            *attribute* The attribute type.
            *className* The class name.
        '''
        if not attribute:
            return None

        dynValue = DynMetaDataValue(parent=object.id, 
                                    parentType=className, 
                                    classDynMetaDataType=attribute.id)
        dynValue.create()

        return dynValue


    @classmethod    
    def updateValue(cls, object, attribute, value):
        '''
        Update to services given attribute value with value for object.
        Return the updated dynamic value instance.
                
        Arguments:
            *object* The object on which value is set.
            *attribute* The attribute type.
            *className* The value to update.
        '''
        dynValue = DynMetaDataValue.query().filter('parent', object.id).filter('classDynMetaDataType', attribute.id).getFirst()
    
        if dynValue is not None:
            dynValue.value=str(value)
            dynValue.update()
            
        return dynValue
    
    

