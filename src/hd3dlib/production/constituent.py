# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from string import Template
from hd3dlib.asset import ImageFileSystem


from hd3dlib.asset.assetgroup import AssetGroup

CONSTITUENT_DEFINITION = {
    'name': 'ConstituentGenerated',
    'attributes':{
        '_class' : Attribute(
             'class',
             str,
             default = 'fr.hd3d.model.lightweight.impl.LConstituent'
        ),
        'simpleClassName' : Attribute('simpleClassName', str),
        'label' : Attribute('label', str),
        'hook' : Attribute('hook', str),
        'description' : Attribute('description', str),
        'category' : Attribute('category', long),
        'difficulty' : Attribute('difficulty', int, default = 0),
        'completion' : Attribute('completion', int, default = 0),
        'trust' : Attribute('trust', int, default = 0),
        'hairs' : Attribute('hairs', bool, default = False),
        'setup' : Attribute('setup', bool, default = False),
        'texturing' : Attribute('texturing', bool, default = False),
        'design' : Attribute('design', bool, default = False),
        'shading' : Attribute('shading', bool, default = False),
        'modeling' : Attribute('modeling', bool, default = False),
        'categoriesNames' : Attribute('categoriesNames', str),
    }
}

class Constituent(makeClass(CONSTITUENT_DEFINITION, BaseObject)):
    '''
    Constituent is the base element of the breakdown. It describes a single 
    object that will compose the project. 
    Tasks and assets are linked to constituent (a constituent is a work object).
    '''
    
    _simpleclassname = 'Constituent'
    _locatorTmpl = Template('/projects/$projects/constituents/')
    _locatorShort = '/constituents/'
    _locator = '/constituents/'

    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)

        if len(kw) is not 0:
            if self.label is None or self.label == '':
                self.label = "Constituent " + self._jicId

            self._resetLocator()

    def _resetLocator(self):
        '''
        Set /constituents/ as locator URL.
        '''
        self._locator = self._locatorShort

    def _resetLocatorWithProject(self, project):
        '''
        Reset locator URL depending of project, following this pattern :
        projects/projectId/constituents/
        
        *project* The project used for building locator
        '''
        self._locator = self._locatorTmpl.safe_substitute(projects = str(project.id))

    def getAssets(self, taskTypeId = -1):
        '''
        Return assets linked to current constituent.
        '''
        assetGroups = AssetGroup.getAllByConstituent(self)
        assets = []
        if  assetGroups:
            for assetGroup in assetGroups:
                assetResult = assetGroup.getAssets(taskTypeId)
                if assetResult:
                    assets += assetResult

        return assets

    def getFiles(self, taskType = "", state = ""):
        '''
        Return files linked to current constituent (via its assets). 
        
        *NB: Assets are retrieved too.*
        
        Arguments:
           *taskType* The taskType of the asset.
        '''
        assets = self.getAssets()

        files = list()
        if assets is not None and len(assets) > 0:
            for asset in assets:
                if taskType == "" or asset.taskTypeName == taskType:
                    files.extend(asset.getFiles(state))

        return files

    def setThumbnail(self, path):
        '''
        Set thumbnail for current constituent with file located at *path*.
        
        Arguments:
           *path* The path of the file to set as thumbnail. 
        '''
        if self.id is not None:
            fs = ImageFileSystem()
            fs.setThumbnail('constituents/', path, self.id)                        


    @classmethod
    def getOrCreateByName(cls, project, category, name):
        '''
        Return the constituent of which name is equal to name.         
        It creates the constituent if it does not already exist.
        
        Arguments:
            *project* Project in which category will appear.
            *category* Category in which constituent will appear.
            *name* Name of the constituent to create or retrieve.
        '''
        obj = cls.query().branch('projects', project.id).branch('categories', category.id).filter('label', name).getFirst()

        if obj is None:
            obj = cls(label = name)
            obj.category = category.id
            obj._resetLocatorWithProject(project)
            obj.create()

        return obj

    @classmethod
    def getByProject(cls, projectId):
        '''
        Return constituents from a project.         
        
        Arguments:
            *projectId* the project id in which constituents will appear.
        '''
        return Constituent.query().branch('projects', projectId).getAll()


    @classmethod
    def getDynByName(cls, project, category, name):
        '''
        Return the constituent of which name is equal to name.         
        It returns None if it does not exist. 
        Dynamic attributes values for this constituent are retrieved.
                
        Arguments:
            *project* Project in which constituent appears.
            *category* Category in which constituent appears.
            *name* Name of the constituent to retrieve.
        '''
        query = cls.query().branch('projects', project.id)
        query = query.branch('categories', category.id).filter('label', name)
        obj = query.dyn().getFirst()

        return obj
    
    
    @classmethod
    def query(cls):
        '''
        Return query object for constituent.
        '''
        return BaseObject.queryClass(cls)

