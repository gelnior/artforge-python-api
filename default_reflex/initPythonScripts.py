# coding=utf-8


if __name__ == '__main__':
    from hd3dlib.extra import Script
    from hd3dlib import conf

   	#To use 'AllEvents_PlaceHolder' (.py) instead of all defined scripts
    USE_PLACEHOLDER = False
    cleanScript = None
    commandList = []

    commandCollection = {'updateAssetList' : [
                              conf.dataServerPythonPath,
                              'updateAssetList.py',
                              'Create assets and asset groups out of breakdown info',
                              None ],
                        'updateTaskList' : [
                              conf.dataServerPythonPath,
                              'updateTaskList.py',
                              'Create tasks out of breakdown info',
                              None],
                        'initializeWithOds' : [
                              conf.dataServerPythonPath,
                              'default_reflex/initializeWithOds.py',
                              'Upload database for a new project with a spreadsheet',
                              None],
                        'createTaskGroupByProjectTaskType' : [
                              conf.dataServerPythonPath,
                              'createTaskGroupByProjectTaskType.py',
                              'Create task group based on task type found in project tasks',
                              None],
                        'cleanScriptLogs' : [
                              conf.dataServerPythonPath,
                              'cleanScriptLogs.py',
                              'Clean the log file for a certain reflex',
                              None],
                        'initializationProject' : [
                              conf.dataServerPythonPath,
                              'initializationProject.py',
                              'Initialization of project',
                              'Project.postPersist'],
                        'template_film' : [
                              conf.dataServerPythonPath,
                              'templates/demo_film.py',
                              'Create demo of film',
                              None],
                        'template_cao' : [
                              conf.dataServerPythonPath,
                              'templates/demo_CAO.py',
                              'Create demo of CAO',
                              None],
                        'createFileTree' : [
                              conf.dataServerPythonPath,
                              'updateAssetFileFromDynamicPath.py',
                              'update asset revision from dynamic path and create file tree',
                              None]
                        }

    scriptPath = conf.dataServerScriptLocation
    if scriptPath is '':
        raise ValueError, 'Unable to locate reflex scripts : script location on server is not set.'

    scriptInstances = Script.query().getAll()
    for scriptInstance in scriptInstances:
        #renaming the script before deleting it is necessary
        #otherwise, it prevents creation of a script with the same name ...
        scriptInstance.name = str(scriptInstance.name) + str(scriptInstance.id)
        scriptInstance.update()
        scriptInstance.delete()

    for commandName in commandCollection.keys():
        scriptName = commandCollection[commandName][1]
        if commandName is not 'cleanScriptLogs': commandList.append(commandName)

        #For testing purpose only
        if USE_PLACEHOLDER: scriptName = 'AllEvents_PlaceHolder.py'

        #commandScript = commandCollection[commandName][0]+' '+scriptPath+scriptName
        commandScript = scriptName
        description = commandCollection[commandName][2]
        trigger = commandCollection[commandName][3]

        if trigger is None:
            t = Script(name = commandName, command = commandScript, description = description)
        else:
            t = Script(name = commandName, command = commandScript, description = description, trigger = trigger)
        t.create()
        if t.name == 'cleanScriptLogs': cleanScript = t

