# coding=utf-8
'''
Object oriented library to access and manage ArtForge data through REST 
services.
'''

__all__ = [	
    'BaseObject',
]

from baseobject import BaseObject


__all__.extend([ 'reflex' ])

import reflex
import util


def connect(server, port, baseUrl, protocol):
    global wsRequestHandler
    if not protocol:
        wsRequestHandler.connect(server, port, baseUrl, 'http')
    else: 
        wsRequestHandler.connect(server, port, baseUrl, protocol)
