# coding=utf-8

from datetime import datetime

from hd3dlib.asset.filerevisionlink import FileRevisionLink
from hd3dlib.asset.filesystem import FileSystem
from hd3dlib.asset.proxy import Proxy
from hd3dlib.baseobject import BaseObject
from hd3dlib.util import wsRequestHandler
from hd3dlib.util.classbuild import makeClass, Attribute
from string import Template





FILEREVISION_DEFINITION = {
    'name':'FileRevisionGenerated',
    'attributes':{
        '_class':Attribute(
             'class', 
             str, 
             default='fr.hd3d.model.lightweight.impl.LFileRevision', 
             mutable=False
        ),
        'key':Attribute('key', str),
        'variation':Attribute('variation',str),
        'revision':Attribute('revision',int),
        'format':Attribute('format',str),
        'sequence':Attribute('sequence',str),
        'creator':Attribute('creator',long),
        'creatorName':Attribute('creatorName',str),
        'creationDate':Attribute('creationDate',str),
        'location':Attribute('location',str),
        'relativePath':Attribute('relativePath',str),
        'fullPath':Attribute('fullPath',str),
        'size':Attribute('size',int),
        'checksum':Attribute('checksum',str),
        'status':Attribute('status',str),
        'lastOperationDate':Attribute('lastOperationDate',str),
        'lastUser':Attribute('lastUser',long),
        'comment':Attribute('comment',str),
        'attributes':Attribute('attributes', list, mutable=False),
        'assetRevision':Attribute('assetRevision',long),
        'state':Attribute('state',str),
        'proxies':Attribute('proxies', list),
        'mountPoint':Attribute('mountPoint', str),
        'taskTypeName':Attribute('taskTypeName',str),
    }
}

class FileRevision(makeClass(FILEREVISION_DEFINITION, BaseObject)):
    '''
    FileRevision represents a single file revision. A file revision can be 
    linked to an asset or not. File revision 
    dependencies are represented by FileRevisionLink objects.
    '''

    _locatorTmpl = Template('/file-revisions/')
    _locatorShort = '/file-revisions/'
    _locator = ''
    
    
    def __init__(self, **kw):
        BaseObject.__init__(self,**kw)
        
        if len(kw) > 0:
            if self.key is None:
                self.key = self._jicId
            if self.creationDate is None:
                self.creationDate = str(datetime.today())
            if self.lastOperationDate is None:
                self.lastOperationDate = str(datetime.today())

        self._resetLocator()
        
        
    def download(self, folder):
        '''
        Download corresponding file to folder given in parameter.
        
        Arguments:
            folder The folder where the file will be downloaded.
        '''
        url = self._locatorShort + str(self.id) + "/download"
        
        filename = folder + self.key

        wsRequestHandler.download(url, filename)
        
        
    def downloadAndRename(self, folder):
        '''
        Download corresponding file to folder given in parameter.
        
        Arguments:
            folder The folder where the file will be downloaded.
        '''
        i=0
        url = self._locatorShort + str(self.id) + "/download"
        name = self.key
        for letter in name:
            i = i+1
            if letter == '.':
                break
        name = '%s%s%s%s' % (name[:i-1], '_', self.revision, name[i-1:])
        filename = folder + name

        wsRequestHandler.download(url, filename)
    
    
    def removeLink(self, outputFile):
        '''
        Remove out link set with outputFile.
        
        Arguments:
            *outputFile* The file that depends of current file.
        '''
        link = FileRevisionLink.getByFiles(self, outputFile)
    
        if link is None:
            raise Exception("No link to delete between %s and %s" %
                            (self.key, outputFile.key))
        else:
            link.delete()
            
            
    def addLink(self, outputFile):
        '''
        Create a new out link from current file to outputFile. outputFile will
        depend of current file.
        
        Arguments:
            *outputFile* The file on which a dependency will be set. 
        '''
        return FileRevisionLink.getOrCreateByFiles(self, outputFile)


    def setNewRevisionWithUpload(self, filePath):
        '''
        Set a new revision for current file revision with file given in parameter.
        
        *filePath* Path of the file to set as new revision.
        '''
        fields = { "variation" : self.variation }        
        files = { "file" : filePath }
        
        
        fs = FileSystem()
        fs.locator = '/file-revisions/next-revision/%d/upload/' % self.id 
        
        fs.postForm('', files, fields) 
    
    
    def setNewRevision(self, location, filename,state, creator,format, checksum="",  size=0):
        '''
        Set a new revision for current file revision.
        
        *location* Path of the file to set as new revision.
        *filename* file name of the file to set as new revision.
        '''
        fileRevision = FileRevision()
        fileRevision.key = str(self.key);
        fileRevision.variation = str(self.variation);
        fileRevision.creationDate = str(datetime.today())
        fileRevision.lastOperationDate = str(datetime.today())
        fileRevision.state = state
        
        fileRevision.format = format
        fileRevision.location = location;
        fileRevision.relativePath = filename;
        fileRevision.creator = creator;
        fileRevision.size = size;
        fileRevision.checksum = checksum;
        fileRevision.lastUser = creator;
        fileRevision.status = "UNLOCKED"
        fileRevision.validity = "prod"
        
        
        revisionList=list()
        revisionList.append(fileRevision)
        ids=FileRevision.query() \
            .setLocator("/file-revisions/next-revision/%d" % self.id) \
            .postAll(revisionList);
        fileRevision=FileRevision.get(ids.pop())
        return fileRevision
         

    def getNextRevision(self):
        '''
        Return file revision corresponding to next revision, it means current
        revision + 1.
        '''
        query = FileRevision.query()
        query = query.filter('key', self.key)
        query = query.filter('revision', self.revision + 1)
        query = query.filter('variation', self.variation)
        
        return query.getFirst()
    
    def getAsset(self):
        '''
        Return the asset revision. If the file is not linked, it return None
        '''
        from hd3dlib.asset.assetrevision import AssetRevision
        
        
        if self.assetRevision:
            return AssetRevision.get(self.assetRevision)
        else :
            return None
    
    
    def getAllRevision(self):
        '''
        Return all revision of the file.
        '''
        query = FileRevision.query()
        query = query.filter('key', self.key)
        
        return query.getAll()
    
    def addProxy(self, proxyTypeId, mountPoint, location,filename, description=""):
        '''
        Add a proxy to this file revision.
        '''
        proxy = Proxy();
        proxy.proxyTypeId=proxyTypeId
        proxy.mountPoint=str(mountPoint)
        proxy.location=str(location)
        proxy.filename=str(filename)
        proxy.description=description
        proxy.fileRevisionId=self.id
        proxy.create()
        self.proxies.append(proxy.id)
        

    @classmethod
    def query(cls):
        return BaseObject.queryClass(cls)  
    
    

        