# coding=utf-8

from hd3dlib.util.httprequests import HttpRequestHandler, HttpRequestTrace
from hd3dlib.task import Task
from hd3dlib.task import TaskType
from hd3dlib.task import TaskGroup
from hd3dlib.task import EntityTaskLink
 
from hd3dlib.production import Project
from hd3dlib.team import Person
from hd3dlib.production import Step

from hd3dlib import conf

global user
PROJECT_NAME = 'ProjectA'
UTF8 = 'utf-8'


#Task lists
global previousTaskNames
global droppedTaskNames
global keptTaskNames
global createdTaskNames

previousTaskNames = []
droppedTaskNames = []
keptTaskNames = []
createdTaskNames = []



# Each work object has one task created per non-null step value
# When possible (i.e. when step value is not just a boolean), the step value is 
# used to set task duration
def createWorkObjectTasks(project, workObject, stepArray, taskTypesArray):
    global user
    global previousTaskNames    
    global droppedTaskNames
    global keptTaskNames
    global createdTaskNames
    
    for stepName in stepArray.keys():
                
        if isinstance(stepName, str):
            stepName = stepName.decode(UTF8)
        
        taskName =  (stepName + u'_' + workObject.hook)
        taskName = taskName.encode(UTF8)
        
        task = Task.query().branch('projects', project.id).filter('name', taskName).getFirst()
        step = stepArray[stepName]
        duration = step.getValue(workObject)
        
        if duration > 0:
                
            if task is not None:
                 keptTaskNames.append(taskName)
            
            else:
                taskType = taskTypesArray[stepName]
                
                t = Task(name = taskName, 
                     status = 'STAND_BY', 
                     project = project.id, 
                     creator = user.id,
                     duration = long(duration * 28800),
                     taskType = taskType.id)
                t._resetLocator()
                t.create()
                     
                classElems = workObject._class.split('.L')
                className = str(classElems[len(classElems) - 1])
                etl = EntityTaskLink(boundEntity = workObject.id,
                                     boundEntityName = className,
                                     task = t.id)
                etl._resetLocator()
                etl.create()
                                 
                createdTaskNames.append(t.name)
        else:
            if task is not None:
                 droppedTaskNames.append(taskName)
            
              
def doIt(args, log):
    global user
    global previousTaskNames    
    global droppedTaskNames
    global keptTaskNames
    global createdTaskNames
    
    log.write('Before get project')
    project =  Project.get(args['projectId'])
    log.write('Processing for project \'%s\'' % (project.name.encode(UTF8)))
    user = Person.getScriptUser()
    
    #resetTaskLists(project)
    
    #Constituents handling 
    constituents = project.getAllConstituents()
    log.write('%d constituents found.' % len(constituents))
    
    stepArray = Step.getArray('Constituent', project.id)
    stepList =  str(stepArray.keys())
    log.write('Constituent steps : %s' % stepList)
    taskTypesArr = TaskType.getTaskTypesArray(stepArray.keys())
    
    for c in constituents:
        createWorkObjectTasks(project, c, stepArray, taskTypesArr)
            
    #Shots handling
    shots = project.getAllShots()
    log.write('%d shots found.' % len(shots))
    
    stepArray = Step.getArray('Shot', project.id)
    stepList =  str(stepArray.keys())
    log.write('Shot steps : %s' % stepList)
    taskTypesArr = TaskType.getTaskTypesArray(stepArray.keys())
    
    for s in shots:
        createWorkObjectTasks(project, s, stepArray, taskTypesArr)
        
    #Tasks to drop  
    for taskName in droppedTaskNames:
        task = Task.query().branch('projects', project.id).filter('name', taskName).getFirst()
        if task is not None:
             task.delete()
             
    #Log result
    log.write('%d tasks kept.' % len(keptTaskNames))
    log.write('%d tasks dropped.' % len(droppedTaskNames))
    log.write('%d tasks created.' % len(createdTaskNames))
    
    
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
    from hd3dlib.reflex import evalArgDictionary
        
    log = None
    
#    try:
    logLocation = conf.dataServerLogPath
    if not os.path.exists(logLocation):
        raise ValueError('Log directory does not exist.')
        
    log = Log(str(os.path.basename(__file__)), logLocation)
    log.enableConsole()
    log.start()
    
    log.write('Arguments are : ' + str(sys.argv))
    args = evalArgDictionary(sys.argv[1])
    
    if not isinstance(args, dict):
        raise ValueError('Malformed arguments dictionary.')
    
    log.write('Before I process everything.')
    doIt(args, log)
    log.write('After I\'ve processed everything.')
        