import datetime

from hd3dlib.extra.dynutils import DynUtils
from hd3dlib.production import Project
from hd3dlib.team import Person
    
       
def makeProject(name):
    project = Project.query().filter('name', name).getFirst()
    
    if project is None:        
        project = Project(name=name)
        project.create()
        print 'Project %s created' % name    
        
    return project


def makePerson(firstName, lastName, login):
    person = Person.query().filter('login', login).getFirst()
    
    if person is None:
        person = Person(firstName=firstName, lastName=lastName, login=login)
        person.create()
        print 'Person %s created' %  person.lastName
       
    return person
    

def doIt():
    
    # Attributs dynamiques
    
    # Creation des types a utiliser.    
    string = DynUtils.createType('string', 'java.lang.String')
    print 'Attribut type %s created' % string.name    
    boolean = DynUtils.createType('boolean', 'java.lang.Boolean')
    print 'Attribut type %s created' % boolean.name
    long = DynUtils.createType('long', 'java.lang.Long')
    print 'Attribut type %s created' % long.name
    date = DynUtils.createType('date', 'java.util.Date')
    print 'Attribut type %s created' % date.name
    person = DynUtils.createType('person', 'person')
    print 'Attribut type %s created' % person.name
    
    # Creation des attributs au niveau de l'entite projet.
    stringAttribute = DynUtils.createAttribute('mon-attribut-string', string, 'Project')
    print 'Attribut %s created' % stringAttribute.name
    booleanAttribute = DynUtils.createAttribute('mon-attribut-boolean', boolean, 'Project')
    print 'Attribut %s created' % booleanAttribute.name
    longAttribute = DynUtils.createAttribute('mon-attribut-long', long, 'Project')
    print 'Attribut %s created' % longAttribute.name
    dateAttribute = DynUtils.createAttribute('mon-attribut-date', date, 'Project')
    print 'Attribut %s created' % dateAttribute.name
    personAttribute = DynUtils.createAttribute('mon-attribut-person', person, 'Project')
    print 'Attribut %s created' % personAttribute.name
    
    # Preparation d'un projet sur lequel on positionnera des valeurs d'attributs dynamiques.
    project = makeProject('Projet dynamique')
    person = makePerson('Attribut', 'Dynamique', 'dyn')
    person2 = makePerson('Attribut-2', 'Dynamique-2', 'dyn-2')
    today = datetime.date.today()
    oneday = datetime.timedelta(days=1)
    tomorrow = today + oneday
    
    # Enregistrement des valeurs d'attributs dynamiques en utilisant la classe statique dediee.
    DynUtils.updateValue(project, stringAttribute, "Test-string")
    DynUtils.updateValue(project, booleanAttribute, True)
    DynUtils.updateValue(project, longAttribute, 12) 
    DynUtils.updateValue(project, dateAttribute, today.strftime("%Y-%m-%d %H:%M:%S"))
    DynUtils.updateValue(project, personAttribute, person.id)

    # Lecture des valeurs d'attributs dynamiques sauvegardees pour le projet incrimine.
    project = Project.query().filter('id', project.id).dyn().getFirst()
    
    print project.dynMetaDataValues
    print "mon-attribut-string : %s " % project.getDynValue(stringAttribute.name)
    print "mon-attribut-boolean : %s " % project.getDynValue(booleanAttribute.name)
    print "mon-attribut-integer : %s " % project.getDynValue(longAttribute.name)
    print "mon-attribut-date : %s " % project.getDynValue(dateAttribute.name)
    print "mon-attribut-person : %s " % project.getDynValue(personAttribute.name)
    
    # Modification des attributs dynamiques par les methodes d'instance et sauvegarde de ces valeurs.
    project.setDynValue(stringAttribute.name, "Test-string n2")
    project.setDynValue(booleanAttribute.name, False)
    project.setDynValue(longAttribute.name, 13)
    project.setDynValue(dateAttribute.name, tomorrow.strftime("%Y-%m-%d %H:%M:%S"))
    project.setDynValue(personAttribute.name, person2.id)
    project.updateDynValues()
    
    # Modification des attributs dynamiques
    project = Project.query().filter('id', project.id).dyn().getFirst()
    print "mon-attribut-string updated : %s " % project.getDynValue(stringAttribute.name)
    print "mon-attribut-boolean updated : %s " % project.getDynValue(booleanAttribute.name)
    print "mon-attribut-integer updated : %s " % project.getDynValue(longAttribute.name)
    print "mon-attribut-date updated : %s " % project.getDynValue(dateAttribute.name)
    print "mon-attribut-person updated : %s " % project.getDynValue(personAttribute.name)
    
    
if __name__ == '__main__':
    print '[INFO] Dynamic attributes demo starts.'
    doIt()
    print '[INFO] Dynamic attributes demo finished.'
