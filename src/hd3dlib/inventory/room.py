# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.inventory import Computer
from hd3dlib import BaseObject
from string import Template


ROOM_DEFINITION = {
    'name':'RoomGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LRoom', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
    }
}

class Room(makeClass(ROOM_DEFINITION, BaseObject)):

    _locatorTmpl = Template('/rooms/')
    _locatorShort = '/rooms/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()

    @classmethod
    def getByName(cls, name):
        '''
        Retrieve the room whose the name is equal to name. If the room
        does not exist, it returns null.
        
        Arguments:
            *name* The name used for filtering.
        '''
        return Room.query().filter('name', name).getFirst()
            
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Retrieve the room whose the name is equal to name. If the room
        does not exist, it create it.
        
        Arguments:
            *name* The name used for filtering.
        '''
        room = Room.getByName(name)
        
        if room is None:
            room = Room()
            room.name = name
            room.create()
        return room
    
    def getAllComputers(self):        
        '''
        Return list of computers on the room.
        '''
        return Computer.query().branch('room', self.id).getAll()

