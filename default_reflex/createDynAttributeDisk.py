#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

'''
Created on 02/02/2011

#@author: Melissa Faucher
'''

from hd3dlib import *
from hd3dlib import conf
from hd3dlib.production import Project
from hd3dlib.extra.dynutils import DynUtils
from hd3dlib.extra import Sheet, ItemGroup, Item, DynMetaDataValue


def doIt(itemGroupId, log):
    itemGroupId = long(itemGroupId)
    print 'itemGroup id :',itemGroupId
    
    itemGroup = ItemGroup.get(itemGroupId)
    
    if itemGroup is None: 
        print 'Error : Failed to retrieve itemGroup : wrong id'
        exit()
        
    sheet = Sheet.get(itemGroup.sheet)
    
    if sheet is None: 
        print 'Error : Failed to retrieve sheet : wrong id'
        exit()
        
    if sheet.boundClassName != 'Project':
        return

    project = Project.get(sheet.project)

    attributeName = 'disk path'
    attributetype = DynUtils.getType('string')
    className = 'Project'
    javaType = 'java.lang.String'
    if attributetype is None:
        attributetype = DynUtils.createType('string', javaType)
    
    stringAttribute = DynUtils.createAttribute(attributeName, attributetype, className)
    DynUtils.createValue(className, stringAttribute, project)
    DynUtils.updateValue(project, stringAttribute, "/u/store42/")
#    stringAttribute.update()
#    project.setDynValue(stringAttribute.name, "/u/store42/")
    project.updateDynValues()
  
    print "mon-attribut-string : %s " % project.getDynValue(stringAttribute.name)
    
    type = 'metaData'
    
    diskItem =    Item(sheet.id,
                   name = attributeName, 
                   query = str(stringAttribute.id), 
                   type = type, 
                   metaType = javaType,
                   renderer = '',
                   editor = 'short-text',
                   itemGroup = itemGroupId ).create()
                   
    log.write('    Item : %s' % diskItem.name)   
    
    
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
    from hd3dlib.reflex import evalArgDictionary
    
    log = None
    
#    try:
    logLocation = conf.dataServerLogPath
    if not os.path.exists(logLocation):
        raise ValueError('Log directory does not exist.')
        
    log = Log(str(os.path.basename(__file__)), logLocation)
    log.enableConsole()
    log.start()
    
    log.write('Arguments are : ' + str(sys.argv))
    projectPath = sys.argv[-1].strip('[]')
    if len( projectPath.rsplit('/',1) ) < 2:
        raise ValueError('Malformed project path.')
    
    itemGroupId = projectPath.rsplit('/',1)[1]
    
    doIt( itemGroupId, log )
    print '[INFO] Dynamic attributes disk applied.'
    