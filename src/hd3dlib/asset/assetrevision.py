# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute

from hd3dlib.baseobject import BaseObject

from hd3dlib.asset.filerevision import FileRevision
from hd3dlib.asset.assetrevisionlink import AssetRevisionLink
from hd3dlib.asset.filesystem import FileSystem

from datetime import datetime
from string import Template

ASSETREVISION_DEFINITION = {
        'name':'AssetRevisionGenerated',
        'attributes':{
            '_class':Attribute(
                'class', str,
                default = 'fr.hd3d.model.lightweight.impl.LAssetRevision'
            ),
            'project':Attribute('project', long),
            'key':Attribute('key', str),
            'variation':Attribute('variation', str),
            'revision':Attribute('revision', int, default = 1, mutable = False),
            'name':Attribute('name', str),
            'taskType':Attribute('taskType', long),
            'taskTypeName':Attribute('taskTypeName', str),
            'status':Attribute('status', str, default = 'UNVALIDATED'),
            'validity':Attribute('validity', str),
            'creator':Attribute('creator', long),
            'creationDate':Attribute('creationDate', str),
            'lastOperationDate':Attribute('lastOperationDate', str),
            'lastUser':Attribute('lastUser', long),
            'comment':Attribute('comment', str),
            'workObjectName':Attribute('workObjectName', str),
            'workObjectPath':Attribute('workObjectPath', str),
            'defaultPath':Attribute('defaultPath', str, mutable = False),
            'assetRevisionGroups':Attribute(
                'assetRevisionGroups', list, default = [], mutable = False
            ),
            'wipPath':Attribute('wipPath', str),
            'publishPath':Attribute('publishPath', str),
            'oldPath':Attribute('oldPath', str),
            'workObjectId':Attribute('workObjectId', long),
            'boundEntityName':Attribute('boundEntityName', str),
        }
}

class AssetRevision(makeClass(ASSETREVISION_DEFINITION, BaseObject)):
    '''
    AssetRevision allows to retrieve file list linked to a workflow step. This 
    step could have variants, so one asset could exist for each variant of the
    step. It is not advised to make more than one asset for a variant.  
    
    Moreover assets support the concept of dependency (ex: texture depends on
    modeling). It handles also revisions and validation status.
    
    Assets are linked together by AssetRevisionLinked object. All link are 
    oriented. Assets are also linked to files. 
    
    '''

    _locatorTmpl = Template('/asset-revisions/')
    _locatorShort = '/asset-revisions/'
    _locator = ''

    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)

        self.key = self._jicId
        self.creationDate = str(datetime.today())
        self.lastOperationDate = str(datetime.today())
        self._resetLocator()

    def getFiles(self, state = ""):
        '''
        Return all files linked to the asset for a given state.
        
        Argument:
            *state* the given state
        '''
        if state != "":
            print state
            return FileRevision.query().filter('state', state).filter('assetRevision', self.id).getAll()
        return FileRevision.query().branch('asset-revisions', self.id).getAll()
    
    def getLastFiles(self):
        '''
        Return last revisions of files linked to the asset.
        '''
        return FileRevision.query().setLocator("/asset-revisions/%d/last-filerevisions" % self.id).getAll()


    def addLinkWithAsset(self, assetToLink):
        ''' 
        Set a new asset revision link from self to assetToLink.
        
        Arguments:
            *assetToLink* The asset that will linked to the current asset. 
            Current asset will be the in asset, and *assetToLink* the out asset. 
        '''
        link = AssetRevisionLink.getOrCreateByAssets(self,
                                   assetToLink)
        return link

    def removeLinkWithAsset(self, assestOut):
        '''
        Remove link that goes from current asset to *assetOut*.
        
        Arguments:    
           *assetOut* Asset of which link with current asset will be removed.
        '''
        assetLink = AssetRevisionLink.getByData(str(self.key),
                                                  str(self.variation),
                                                  self.revision,
                                                  str(assestOut.key),
                                                  str(assestOut.variation),
                                                  assestOut.revision)

        if assetLink is not None:
            assetLink.delete()



    def addFileRevision(self, filerevision):
        '''
        Create a link between current asset and *filerevision*. 
        
        Arguments:
            *filerevision* The file where the link will be set.
        '''

        filerevision.assetRevision = self.id
        filerevision.update()


    def removeFileRevision(self, filerevision):
        '''
        Remove link between current asset revision and file revision.
        
        Arguments:
            *filerevision* The file of which link will be removed.
        '''

        filerevision.assetRevision = None
        filerevision.update()


    def uploadNewFile(self, filePath, variation):
        '''
        Create new file revision for file located at *filePath* and link it
        directly to current asset.
        
        Arguments:
            *filePath* Path of the file to upload.
            *variation*  Variation of file revision.
        '''
        fields = {
            "variation" : str(variation),
            "revision" : str(1),
            "assetId" : str(self.id)
        }

        files = {
             "file" : str(filePath)
        }

        fs = FileSystem()
        fs.locator = '/upload'

        fs.postForm('', files, fields)


    @classmethod
    def query(cls):
        return BaseObject.queryClass(AssetRevision)

