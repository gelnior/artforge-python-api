# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

ENTITY_TASK_LINK_DEFINITION = {
    'name':'EntityTaskLinkGenerated',
    'attributes': {
         '_class': Attribute(
             'class', 
             str, 
             default='fr.hd3d.model.lightweight.impl.LEntityTaskLink', 
             mutable=False),
        'boundEntity':Attribute('boundEntity', long),
        'boundEntityName':Attribute('boundEntityName', str),
        'task':Attribute('task', long),
        'woPath':Attribute('woPath', str),
        'woName':Attribute('woName', str)
    }
}

class EntityTaskLink(makeClass(ENTITY_TASK_LINK_DEFINITION, BaseObject)):
    ''' 
    Set a link between a task and a work object like a shot or a constituent.
    '''
    _locatorTmpl = Template('/entity-task-links/')
    _locatorShort = '/entity-task-links/'
    _locator = '/entity-task-links/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
    
    
    @classmethod
    def getOrCreate(cls, workObjectId, workObjectType, task):
        '''
        Retrieve link between work object described by workObjectId and 
        workObjectType. If link does not exist, it is created. 
        
        Arguments:
            workObjectId The id of the work to link.
            workObjectType The type of the work object to link.
            task The task to link with the workObject.
        '''
        query = cls.query().filter('boundEntity', workObjectId)
        query = query.filter('boundEntityName',workObjectType)
        query = query.filter('task', task.id)
        obj = query.getFirst()
        
        if obj is None:
            obj = cls()            
            obj.boundEntity = workObjectId
            obj.boundEntityName = workObjectType
            obj.task = task.id
            obj.create()
        
        return obj
