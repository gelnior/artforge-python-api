# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from string import Template

DYN_METADATA_TYPE_DEFINITION = {
    'name':'DynMetaDataTypeGenerated',
    'attributes':{
        '_class': Attribute('class', str, default='fr.hd3d.model.lightweight.impl.LDynMetaDataType', mutable=False),
        'name':Attribute('name', str),
        'nature':Attribute('nature', str),
        'type':Attribute('type',str),
        'defaultValue':Attribute('defaultValue',str),
        'structName':Attribute('structName', str),
        'structId':Attribute('structId', long)
    }
}

 
class DynMetaDataType( makeClass(DYN_METADATA_TYPE_DEFINITION, BaseObject) ):
    '''
    DynMetaDateType correspond to the type of dynamic attributes set on specified class
    (like string, boolean, long...).
    '''
    
    _locatorTmpl = Template('/dyn-metadata-types/')
    _locatorShort = '/dyn-metadata-types/'
    _locator = '/dyn-metadata-types/'
    
    
    def __init__ (self, **kw):
        BaseObject.__init__(self, **kw)


    @classmethod
    def query(cls):
        '''
        Return query object for DynMetaDataType.
        '''
        return BaseObject.queryClass(cls) 