#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

'''
Created on 01/21/2011

#@author: Melissa Faucher
'''

from hd3dlib import *
from hd3dlib.production import Project, Sequence, Shot
from hd3dlib.team import Person
from hd3dlib.team import ResourceGroup
from hd3dlib import conf
from hd3dlib.extra.dynutils import DynUtils
from hd3dlib.extra.classdynmetadatatype import ClassDynMetaDataType
from hd3dlib.extra.dynmetadatavalue import DynMetaDataValue

import sys
import pprint
import os

global user
global project   
global scriptPath 
   
quiet = True

def getProjectWorkObjects( project ):
    workObjList = []
    constituents = project.getAllConstituents()
    for constituent in constituents:
        workObjList.append( constituent.label )
    
    workObjList.extend( getProjectSeqShots( project ) )

    return workObjList

def getProjectSeqShots( project ):
    seqshotList = []
    
    sequences = Sequence.query().branch('projects', project.id).leaf('children').getAll()
    for seq in sequences:
        shots = Shot.query().branch('projects', project.id).branch('sequences', seq.id).getAll()
        for shot in shots:
            seqshotList.append( seq.name+shot.label )
            
    return seqshotList                 
              
def getProjectUsers( project ):
    usersList = []
    usersLoginList = []
    groupNameList = project.resourceGroupNames
            
    for groupName in groupNameList:
        group = ResourceGroup.query().filter('name', groupName).getFirst()
        usersList.extend( group.resourceNames )
    
    usersList = list( set( usersList ) )
    
    for user in usersList:
        login = user.rsplit(' - ',1)
        if len( login ) < 2:
            print 'Failed to retrieve login from user',user
            continue
        
        usersLoginList.append( login[1].strip() )
    
    return usersLoginList

def getStageFromAsset( asset ):
    if asset is None:
        return None
    
    taskTypes = {
                'Animation':'anim',
                'Compositing':'compo',
                'Design':'design',
                'Dressing':'dress',
                'Export':'export',
                'Hairs':'hairs',
                'Layout':'layout',
                'Lightning':'lightning',
                'MattePainting':'mattepainting',
                'Modeling':'modeling',
                'Rendering':'rendering',
                'Setup':'setup',
                'Shading':'shading',
                'Texturing':'texturing',
                'Tracking':'tracking'
                }
    
    if asset.taskTypeName in taskTypes.keys():
        return taskTypes[asset.taskTypeName]
    else:
        return None

def getProjectDisk( projectId ):
    dynAtt = DynMetaDataValue.query().filter('parent', projectId ).getAll()
    disk = None
    for att in dynAtt:
        if att.classDynMetaDataTypeName == 'disk path':
            disk = att.value
            break
            
    if disk is None:
        sys.exit('Error : wrong disk path ('+disk+' not found)')
    elif not os.access(disk, os.F_OK):
        sys.exit('Error : specified disk '+disk+' does not exist')
    elif not os.access(disk, os.R_OK):
        sys.exit('Error : '+disk+' not readable')
    elif not os.access(disk, os.W_OK):
        sys.exit('Error : '+disk+' not writable')
        
    return disk
              
              
def doIt(args, log):
    
    global user
    global project
    global scriptPath

    command = ''
    
    quietOpt = ''
    
    if quiet:
        quietOpt = ' -Q '
    
    scriptPathMGA = os.path.join( scriptPath, 'ScriptsMGA' )

    if 'projectId' not in args.keys():
        sys.exit('Error : Failed to retrieve project id')
        
    project =  Project.get(args['projectId'])
    if project is None:
        sys.exit('Error : Failed to retrieve project : wrong id')
        
    if 'disk' not in args.keys():
        ## get path from project dynamic attribute
        disk = getProjectDisk( project.id )
    else:    
        disk    =  Project.get(args['disk'])
        if disk is None:
            print 'Warning : Failed to retrieve disk : wrong id'
            # get path from project dynamic attribute
            disk = getProjectDisk( project.id )
        
    #--- CREATION DU REPERTOIRE DU PROJET
    
    projectName = project.name
    inDirectory = projectName + 'IN'
    outDirectory = projectName + 'OUT'
    
    log.write('Processing for project %s' % (projectName))
    
    command +=   os.path.join(scriptPathMGA, 'mgaProject') + quietOpt + ' -W ' +  disk  + ' -P ' +  projectName + '\n'
    
    #--- CREATION DES REPERTOIRES DES WORK OBJECTS
    
    workObjList = getProjectWorkObjects( project )
    
    
    for workObj in workObjList:
        workObjName = workObj
        workDisk = disk
        command += os.path.join(scriptPathMGA, 'mgaSubInOffshore') + quietOpt + ' -W ' +  disk  + ' -P ' +  projectName + ' -S ' +  workObjName + ' -w ' +  workDisk + ' -p ' + inDirectory + '\n'
        command += os.path.join(scriptPathMGA, 'mgaSubOutOffshore') + quietOpt + ' -W ' +  disk  + ' -P ' +  projectName + ' -S ' +  workObjName + ' -w ' +  workDisk + ' -p ' + outDirectory + '\n'
    
    
    #--- CREATION DES REPERTOIRES DES USERS ET DE LEURS WORK OBJECTS
    constituents = project.getAllConstituents()
    userList = getProjectUsers( project )
    if 'COM' not in userList:
        userList.append( 'COM' )
    if 'OFF' not in userList:
        userList.append( 'OFF' )
    
    for user in userList:
        command +=  os.path.join(scriptPathMGA, 'mgaUserOfProject') + quietOpt + ' -W ' +  disk  + ' -P ' +  projectName + ' -U ' +  user + '\n'
        
        for workObj in workObjList:
            workObjName = workObj
            workDisk = disk
            command +=  os.path.join(scriptPathMGA, 'mgaUserSubOffshore') + quietOpt + ' -W ' +  disk  + ' -P ' +  projectName + ' -S ' +  workObjName + ' -U ' +  user + ' -w ' +  workDisk + ' -p ' + outDirectory + '\n'
        
    if constituents:
        for constituent in constituents:
            if constituent is None:
                continue
            assets=constituent.getAssets()
            if assets:
                for asset in assets:
                    etape = getStageFromAsset( asset )
                    publishPath = os.path.join(disk,etape,projectName,'COM')
                    asset.publishPath=publishPath.encode()
                    oldPath = os.path.join(disk,etape,projectName,'OFF')
                    asset.publishPath=publishPath.encode()
                    wipPath = os.path.join(disk,etape,projectName,'${user}')
                    asset.wipPath=wipPath.encode()
                    
                    # update asset with solved dynamic paths
                    asset.wipPath=wipPath.encode()
                    asset.publishPath=publishPath.encode()
                    asset.oldPath=oldPath.encode()
                    asset.update()
    os.system(command)   
    
    
if __name__ == '__main__':
    import sys
    import os
    from hd3dlib.reflex import Log
    from hd3dlib.reflex import evalArgDictionary
    
    global scriptPath  
        
    log = None
    
    logLocation = conf.dataServerLogPath
    if not os.path.exists(logLocation):
        raise ValueError('Log directory does not exist.')
        
    log = Log(str(os.path.basename(__file__)), logLocation)
    log.enableConsole()
    log.start()
    
    args = evalArgDictionary(sys.argv[1])[0]
    
    if not isinstance(args, dict):
        raise ValueError('Malformed arguments dictionary.')
    
    scriptPath = conf.dataServerScriptLocation
    if scriptPath is '':
        raise ValueError, 'Unable to locate reflex scripts : script location on server is not set.' 
    
    doIt(args, log)
