# coding=utf-8
  
from hd3dlib.team import Person, ResourceGroup
from hd3dlib.production import Category, Constituent, Sequence, Shot
from hd3dlib.production import Project, ProjectType
from hd3dlib.task import Task, TaskType, EntityTaskLink

import time
from datetime import timedelta
from datetime import date


global project
global textureBots
global ModelingBots
global hairBots

global layoutBots
global compoBots

global constituentIds
global shotIds

global groups

def initProject():
    global project
      
    type = ProjectType.getOrCreateByName("Long métrage")
    project = Project.getOrCreateByName("Haribo big production")

    if not project.id:
        project.status = "OPEN"
        project.projectTypeId = type.id
        project.save()
    
    
def initGroups():
    global textureBots
    global ModelingBots
    global hairBots
    
    global layoutBots
    global compoBots
    
    mainGroup = ResourceGroup.getOrCreateByName("Haribo team")
    
    textureBots = ResourceGroup.getOrCreateByName("Texture team", mainGroup.id)
    ModelingBots = ResourceGroup.getOrCreateByName("Modeling team", mainGroup.id)
    hairBots = ResourceGroup.getOrCreateByName("Hair team", mainGroup.id)
    
    layoutBots = ResourceGroup.getOrCreateByName("Layout team", mainGroup.id)
    compoBots = ResourceGroup.getOrCreateByName("Compositing team", mainGroup.id)


def initPersons():
    global groups 
    groups = dict()
    groups['Texture'] = list()
    groups['Modeling'] = list()
    groups['Hair'] = list()
    groups['Layout'] = list()
    groups['Compositing'] = list()

    
    for i in range(300):
        login = 'bot%03d' % (i + 1) 
        firstName = 'Blender %03d' % (i + 1)
        lastName = 'Bot %03d' % (i + 1)

        person = Person.getOrCreateByLogin(firstName, lastName, login)

        if i < 60:
            groups['Texture'].append(person.id)
        elif i < 120:
            groups['Modeling'].append(person.id)
        elif i < 180:
            groups['Hair'].append(person.id)
        elif i < 240:
            groups['Layout'].append(person.id)
        elif i < 300:
            groups['Compositing'].append(person.id)

        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
            if i < 60:
                person.resourceGroupIDs.append(textureBots.id)
            elif i < 120:
                person.resourceGroupIDs.append(ModelingBots.id)
            elif i < 180:
                person.resourceGroupIDs.append(hairBots.id)
            elif i < 240:
                person.resourceGroupIDs.append(layoutBots.id)
            elif i < 300:
                person.resourceGroupIDs.append(compoBots.id)
    
            person.update()


def initRoles():
    pass


def initConstituents():
    global constituentIds
    constituentIds = list()
    
    for i in range(10):
        categoryName = 'Category %02d' % (i + 1)
        category = Category.getOrCreateByName(project, categoryName)
        
        for j in range(20):            
            constituentName = 'Constituent %03d' % (j + 1 + 20 * i)
            constituent = Constituent.getOrCreateByName(project, category, constituentName)
            constituentIds.append(constituent.id)

def initShots():    
    global shotIds
    shotIds = list()
    
    for i in range(20):
        sequenceName = 'Sequence %02d' % (i + 1)
        sequence = Sequence.getOrCreateByName(project, sequenceName)
        
        for j in range(20):            
            shotName = 'Shot %03d' % (j + 1 + 20 * i)
            shot = Shot.getOrCreateByName(project, sequence, shotName)
            shotIds.append(shot.id)

def initTasks():    
    modeling = TaskType.getOrCreateByName("Modeling")
    texture = TaskType.getOrCreateByName("Texture")
    hair = TaskType.getOrCreateByName("Hair")
    layout = TaskType.getOrCreateByName("Layout")
    compositing = TaskType.getOrCreateByName("Compositing")
    
    i = 0
    for id in constituentIds:
        i = (i + 1) % 60
        
        initTask(id, 'Constituent', modeling, i)
        initTask(id, 'Constituent', texture, i)
        initTask(id, 'Constituent', hair, i)

    i = 0
    for id in shotIds:
        i = (i + 1) % 60
        
        initTask(id, 'Shot', layout, i)
        initTask(id, 'Shot', compositing, i)

def initTask(id, boundEntityName, type, personIdx):
        
        taskName = '%d_%s_%s' % (id, boundEntityName, type.name)
        task = Task.getByName(project, taskName, False)
        duration = timedelta(days=3)

        if not task:
            taskDate = date.today()
            
            task = Task()
            task.project = project.id
            task.name = taskName.encode("utf-8")
            task.status = "STAND_BY"
            task.startDate = taskDate.strftime('%Y-%m-%d %H:%M:%S')
            task.endDate = (taskDate + duration).strftime('%Y-%m-%d %H:%M:%S')
            task.worker = groups[str(type.name)][personIdx]      
            task.taskType = type.id
            task.create()
        
        link = EntityTaskLink.getOrCreate(id, boundEntityName, task)
        

def doIt():
    initProject()
    initGroups()
    initPersons()
    initRoles()
    initConstituents()
    initShots()
    initTasks()
    
    
if __name__ == '__main__':
         
    print '[INFO] Haribo init starts.'
    doIt()
    print '[INFO] Haribo init finished.'
    