# coding=utf-8
'''
Class for human resources management.
'''

__all__ = [    
    'Person',
    'ResourceGroup',    
]

from resourcegroup import ResourceGroup
from person import Person