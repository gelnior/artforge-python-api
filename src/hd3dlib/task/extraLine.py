# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from string import Template

SCRIPT_DEFINITION = {
    'name':'Extra line generated',
    'attributes':{
        'class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LExtraLine', 
            mutable=False
        ),
        'taskGroup':Attribute('taskGroupID', long),
        'name':Attribute('name', str),
        'hiddenPersonID':Attribute('hiddenPersonID', long)
    }
}

class ExtraLine(makeClass(SCRIPT_DEFINITION, BaseObject)):
    '''
    classdocs
    '''
    _locatorTmpl = Template('/projects/$projects/plannings/$plannings/task-groups/$taskGroups/extra-lines')
    _locatorShort = '/projects/$projects/plannings/$plannings/task-groups/$taskGroups/extra-lines'
    _locator = ''
    _planning = None
    _project = None

    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()        
            
    def _completeAttributeValues(self):
        pass
    
    def _resetLocator(self):
        if self._planning is not None and self._project is not None:
            self._locatorTmpl = Template(ExtraLine._locatorTmpl.template)
            self._locator = self._locatorTmpl.safe_substitute(projects=self._project, plannings=self._planning, taskGroups=self.taskGroup)