# coding=utf-8

import hashlib
from base64 import b64encode
from datetime import datetime


def generateRandomId(obj, length=8):
    '''
    Returns a pseudo UUID string calculated from object fields and the current 
    date. Algorithm used is SHA1 algorithm.
    
    Default result length is 8 chars by default but it is parameterizable.
    
    Arguments : 
         *length* Length of result.
    '''
    value = str(obj) + '\n' + str(datetime.today()) + '\n'
    for p in dir(obj):
        value += str(getattr(obj,p)) + '\n'
        
    digester = hashlib.sha1()
    digester.update(value)
    
    out = b64encode(digester.digest(), '_*')
    
    if length > len(out): length = len(out)
    return out[:length]
    
    
    