# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute

from hd3dlib.baseobject import BaseObject



from string import Template

MOUNTPOINT_DEFINITION = {
        'name':'MountPointGenerated',
        'attributes':{
            '_class':Attribute(
                'class',str,
                default='fr.hd3d.model.lightweight.impl.LMountPoint'
            ),
            'storageName':Attribute('storageName', str),
            'operatingSystem':Attribute('operatingSystem', str),
            'mountPointPath':Attribute('mountPointPath', str),
        }
}

class MountPoint(makeClass(MOUNTPOINT_DEFINITION, BaseObject)):
    '''
    MountPoint allows to retrieve mount point list.
    '''
    
    _locatorTmpl = Template('/mount-points/')
    _locatorShort = '/mount-points/'
    _locator = '/mount-points/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
