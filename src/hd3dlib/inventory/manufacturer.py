# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


MANUFACTURER_DEFINITION = {
    'name':'ManufacturerGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LManufacturer', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
    }
}

class Manufacturer(makeClass(MANUFACTURER_DEFINITION, BaseObject)):
    '''
    Manufacturer allow to link manufacturers with computers.
    '''

    _locatorTmpl = Template('/manufacturers/')
    _locatorShort = '/manufacturers/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()