# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


DEVICE_DEFINITION = {
    'name':'DeviceGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LDevice', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
        "billingReference" : Attribute('billingReference', str),
        "purchaseDate" : Attribute('purchaseDate', str),
        "warrantyEnd" : Attribute('warrantyEnd', str),
        "deviceType" : Attribute('deviceTypeId', long),
        "deviceTypeName" : Attribute('deviceTypeName', str),
        "deviceModel" : Attribute('deviceModelId', long),
        "deviceModelName" : Attribute('deviceModelName', str),
        "size" : Attribute('size', long),
        "serial" : Attribute('serial', str),
        "inventoryStatus" : Attribute('inventoryStatus', str),

        "projectIDs" : Attribute('projectIDs', list, mutable=False),
        "projectNames" : Attribute('projectNames', list, mutable=False),
        
        "computerIDs" : Attribute('computerIDs', list, mutable=False),
        "computerNames" : Attribute('computerNames', list, mutable=False),

        "resourceGroupIDs" : Attribute('resourceGroupIDs', list, mutable=False),
        "resourceGroupNames" : Attribute('resourceGroupNames', list, mutable=False)
        
    }
}

class Device(makeClass(DEVICE_DEFINITION, BaseObject)):
    '''
    Device describes a device and its links with computers.
    '''

    _locatorTmpl = Template('/devices/')
    _locatorShort = '/devices/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()


    @classmethod
    def query(cls):
        '''
        Return query object for item group.
        '''
        return BaseObject.queryClass(cls) 
    
    
    @classmethod
    def getByName(cls, name):
        '''
        Retrieve the device whose the name is equal to name. If the device
        does not exist, it returns null.
        
        Arguments:
            *name* The name used for filtering.
        '''
        return Device.query().filter('name', name).getFirst()
    
    
    @classmethod
    def getOrCreateByName(cls, name):
        '''
        Retrieve the device whose the name is equal to name. If the device
        does not exist, it create it.
        
        Arguments:
            *name* The name used for filtering.
        '''
        device = Device.getByName(name)
        
        if device is None:
            device = Device()
            device.name = name
            device.create()
           
        return device
    
    def addComputer(self, computerId):
        '''
        Adds a computer to the list of associated computers
        
        Arguments:
            *computerId* The id of the computer
        '''
        
        if computerId not in self.computerIDs:
            self.computerIDs.append(computerId)

    