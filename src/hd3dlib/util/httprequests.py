# coding=utf-8

import sys

#try:
try: 
    import simplejson as json
except ImportError:
    import json
    
#except ImportError:
#    import simplejson as json
    
import urllib2
import urllib
from urllib2 import HTTPError
from urllib2 import BaseHandler

from authutils import Authentication

from datetime import datetime



class MyDebuggedHTTPErrorProcessor(BaseHandler):
    '''
    Debug tool that sends error when server response is not a success. 
    '''

    handler_order = 1000  # after all other processing
    
    # Check if server response code is corresponding to a success or not
    # (success code are between 200 and 300).
    # If server code is wrong, an error is set as a response.
    def http_response(self, request, response):
        code, msg, hdrs = response.code, response.msg, response.info()
        if not (200 <= code < 300):
            response = self.parent.error(
                'http', request, response, code, msg, hdrs)
        return response
    
    https_response = http_response
    
# Set the custom error processor as default processor on urllib2.
urllib2.HTTPErrorProcessor = MyDebuggedHTTPErrorProcessor


class HttpRequestHandler(object):
    '''
    Communicates with the web services through HTTP. it handles GET, PUT, POST
    and delete requests.
    If any, the returned data are parsed to python-usable objects.
    '''
    requestTraces = []
    
    def __init__(self, server, port=80, baseUrl='/', protocol='http'):
        ''' 
        Constructor : open a  connection with given server at the specified port.
        
        Arguments :
            *server* : The server on which connection should be set.
            *port* : The port to use to establish connection.
            *baseUrl* : The base URL needed to acccess to services.
        '''
        if not isinstance(server, str) or not isinstance(port, int) or not isinstance(baseUrl, str) or not isinstance(baseUrl, str):
            raise TypeError("Argument types must be : server <str>, port <int>, baseUrl <str>, procotol <str>")
        self.connect(server, port, baseUrl, protocol)
        
    
    def connect(self, server, port=80, baseUrl='/', protocol='http'):
        ''' 
        Open a  connection with given server at the specified port.
        
        Arguments :
            *server* : The server on which connection should be set.
            *port* : The port to use to establish connection.
            *baseUrl* : The base URL needed to acccess to services.
        '''
        import urlparse
        
        self.server = server
        self.port = port
        self.protocol = protocol
        
        self.baseUrl = urlparse.urlunparse((protocol, '%s:%d'%(server,port), baseUrl, None, None, None))
        

    def authenticate(self, user, password):
        '''
        Set authentication. From this point, when a request will return a 401 
        response (not authorized). If authentication is set, a new request with 
        the given credentials will be automatically sent.
        
        Arguments: 
            *user* : user name set in credentials. 
            *password* : password set in credentials.
        ''' 
        self.authentication = Authentication(self.baseUrl, user, password)
    

    def get(self, url, headers={'Accept':'application/json', 'Charset':'utf-8'}):
        '''
        Send a GET request to specified url. Response expected is at JSON 
        format. So data retrieved are automatically parsed and return as a dict.
        
        Arguments : 
            *url* : URL on which a get request will be sent.
            *headers* : HTTP request header (automatically configured).
        '''
        # Prepare url
        url = url.lstrip(' /')
        requestedUrl = urllib2.urlparse.urljoin(self.baseUrl, url)
        
        # Prepare trace.
        lastRequestTrace = HttpRequestTrace('GET', requestedUrl)
        lastRequestTrace.start()
        
        # Go make the request
        try:
            request = urllib2.Request(url=requestedUrl, headers=headers)
            request.get_method = lambda: 'GET'
            response = urllib2.urlopen(request)
            lastRequestTrace.stop(response)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)

            # Read the response content
            data = response.read().decode("utf-8")
            
            # Translate into python dict
            if 'Accept' in headers and headers['Accept'] == 'application/json':
                try:
                    dic = json.loads(data)
                except ValueError, e:
                    sys.stderr.write('[ERROR] returned json cannot be parsed. \n') 
                    sys.stderr.write('Maybe returned json is empty. Did you encounter a 404 error ? \n ')
                    sys.stderr.write('GET ' + urllib.unquote_plus(requestedUrl) + '\n')                  
                    #raise ValueError 
                    return None                   
                return dic
            else:
                return data
            
        except HTTPError, e:        
            lastRequestTrace.stop(e)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)
            
            # If exception occurred, message is printed.           
            if isinstance(e.msg, str):
                msg = e.msg.decode('latin1')
            
            msg = u'[ERROR] returned ' + unicode(e.code) + u' ' + unicode(msg) + u'\n'
            sys.stderr.write(msg.encode('utf-8', 'replace'))
            sys.stderr.write('GET ' + urllib.unquote_plus(e.url) + '\n')
            raise e
        
            # Print message error from server.
            try :                 
                data = e.read() 
                dic = json.loads(data)
                msg = '[SERVER-INFO] %s \n' % dic["records"][0][0]
                sys.stderr.write(msg.encode('utf-8', 'replace'))
            except ValueError:
                pass
        
        
    def download(self, url, filename, headers={'Charset':'utf-8'}):
        '''
        Download from a given URL (that should be a file revision download URL), 
        the file corresponding and save it as *filename*.
        
        Arguments :
            *url* The url from where to download the file.
            *filename* The file name where the file must be downloaded.
            *headers* Request headers (automatically configured).
        
        '''
        
        # Prepare url
        url = url.lstrip(' /')
        requestedUrl = urllib2.urlparse.urljoin(self.baseUrl, url)
        
        # Prepare request trace
        lastRequestTrace = HttpRequestTrace('GET', requestedUrl)
        lastRequestTrace.start() 
        
        # Go make the request
        try:            
            request = urllib2.Request(url=requestedUrl, headers=headers)
            request.get_method = lambda: 'GET'
            webFile = urllib2.urlopen(request)
            localFile = open(filename, 'wb')
            localFile.write(webFile.read())
            webFile.close()
            localFile.close()
            
            HttpRequestHandler.requestTraces.append(lastRequestTrace)
            
        except HTTPError, e:        
            lastRequestTrace.stop(e)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)
            
            if isinstance(e.msg, str):
                msg = e.msg.decode("utf-8")
            
            msg = u'[ERROR] returned ' + unicode(e.code) + u' ' + unicode(msg) + u'\n'
            sys.stderr.write(msg.encode('latin1', 'replace'))                                                  
            sys.stderr.write(msg)
            raise e
     
     
    # Send a POST request to specified URL (most of the time for creation purpose).
    def post(self, url, body, 
            returnsOnlyId=True, returnDataAsDict=False, 
            headers={'Accept':'application/json', 'Content-type':'application/json', 'Charset':'utf-8'}):
        
        '''
        Send a POST request to specified URL (most of the time for creation 
        purpose). Json can be sent via *body*.
        
        Arguments : 
            *url* URL on which the POST request will be sent.
            *returnsOnlyId* True to return only id of created object, false to
            return The URL corresponding to created resource. 
            *returnDataAsDict* True to return data as dict (needed if response
            is made of JSON). 
            *headers* HTTP request headers (automatically configured).
        '''
        
        # Prepare URL
        url = url.lstrip(' /')
        requestedUrl = urllib2.urlparse.urljoin(self.baseUrl, url)

        # Build the request
        lastRequestTrace = HttpRequestTrace('POST', requestedUrl)
        lastRequestTrace.start()
        
        try:
            request = urllib2.Request(url=requestedUrl, data=body, headers=headers)
            request.get_method = lambda: 'POST'
            response = urllib2.urlopen(request)
            lastRequestTrace.stop(response)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)
            
            # If data are returned, data are returned instead of returning id of 
            # newly created entity, data are returned as a dictionary.
            if returnDataAsDict:
                data = response.read().decode("utf-8")
                return json.loads(data)
            
            # When a post succeeds server returns the URL of the newly created instance.
            # Id is extracted from this URL and returned.            
            if response.info().getheader('Content-location'):
                if returnsOnlyId == True:
                    spliturl = response.info().getheader('Content-location').split('/')
                    return spliturl[len(spliturl) - 1]
                else:
                    return response.info().getheader('Content-location')
                
        except HTTPError, e:        
            # Error handling            
            lastRequestTrace.stop(e)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)
            
            code = e.code
            if isinstance(code, str):
                code = code.decode('latin1', "ignore")
            
            msg = e.msg
            if isinstance(msg, str):
                msg = msg.decode('latin1', "ignore")
                            
            msg = u'[ERROR] POST %s  returned %s %s \n' % (urllib.unquote_plus(e.url), code, msg)                                    
            sys.stderr.write(msg.encode('utf-8', 'replace'))
            
            # Print message error from server. If no message found, nothing is
            # printed.
            try :                 
                data = e.read() 
                dic = json.loads(data)
                sys.stderr.write('[SERVER-INFO] %s \n' % dic["records"][0][0])
            except ValueError:
                sys.stderr.write('[SERVER-INFO] No value returned \n')
        
        
    # Send a PUT request to specified URL (for instance update purpose).
    def put(self, url, body, headers={'Content-type':'application/json', 'Charset':'utf-8'}):
        '''
        Send a PUT request to specified URL (most of the time for update 
        purpose). Json can be sent via *body*.
        
        Arguments : 
            *url* URL on which the POST request will be sent.
            *headers* HTTP request headers (automatically configured).
        '''
        
        # Prepare URL
        url = url.lstrip(' /')
        requestedUrl = urllib2.urlparse.urljoin(self.baseUrl, url)
        
        lastRequestTrace = HttpRequestTrace('PUT', requestedUrl, body)
        lastRequestTrace.start()
        try:
            request = urllib2.Request(url=requestedUrl, data=body, headers=headers)
            request.get_method = lambda: 'PUT'
            response = urllib2.urlopen(request)
            lastRequestTrace.stop(response)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)

            return response.read()
        
        except HTTPError, e:        
            # Error handling
            lastRequestTrace.stop(e)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)
            
            code = e.code
            if isinstance(code, str):
                code = code.decode('latin1')
            
            msg = e.msg
            if isinstance(msg, str):
                msg = e.msg.decode('latin1')
                            
            msg = u'[ERROR] PUT %s  returned %s %s \n' % (urllib.unquote_plus(e.url), code, msg)                                                     
            sys.stderr.write(msg.encode('utf-8', "ignore"))
            raise e
        
            # Print message error from server. If no message found, nothing is
            # printed.
            try :                 
                data = e.read() 
                dic = json.loads(data)
                sys.stderr.write('[SERVER-INFO] %s \n' % dic["records"][0][0])
            except ValueError:
                pass
    

    def delete(self, url, headers={'Accept':'application/json', 'Charset':'utf-8'}):
        '''
        Send a DELETE request to specified url. This method is used most of the 
        time in an instance delete purpose. 
        
        Arguments : 
            *url* : URL on which a get request will be sent.
            *headers* : HTTP request header (automatically configured).
        '''
        
        # Prepare URL
        url = url.lstrip(' /')
        requestedUrl = urllib2.urlparse.urljoin(self.baseUrl, url)
        
        # Prepare HTTP trace
        lastRequestTrace = HttpRequestTrace('DELETE', requestedUrl)
        lastRequestTrace.start()
        
        # Go make the request
        try:
            request = urllib2.Request(url=requestedUrl, headers=headers)
            request.get_method = lambda: 'DELETE'
            response = urllib2.urlopen(request)
            lastRequestTrace.stop(response)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)
            
            return response.read()
        
        except HTTPError, e:        
            lastRequestTrace.stop(e)
            HttpRequestHandler.requestTraces.append(lastRequestTrace)
            
            code = e.code
            if isinstance(code, str):
                code = code.decode('latin1', "ignore")
            
            msg = e.msg
            if isinstance(msg, str):
                msg = msg.decode('latin1', "ignore")
                            
            msg = u'[ERROR] DELETE %s  returned %s %s \n' % (urllib.unquote_plus(e.url), code, msg)                                                   
            sys.stderr.write(msg)
                                       
            raise e
        
            # Print message error from server.
            try :                 
                data = e.read() 
                dic = json.loads(data)
                msg = u'[SERVER-INFO] %s \n' % dic["records"][0][0]
                sys.stderr.write(msg.encode("utf-8", "replace"))
            except ValueError:
                pass
        
            
    @staticmethod
    def popRequestTraces():
        '''
        Return last requests traces as a list.
        '''
        res = HttpRequestHandler.requestTraces
        HttpRequestHandler.requestTraces = []
        return res
    
class HttpRequestTrace(object):
    '''
    Utility class for printing log access to services. It takes as members
    request description. It also gives process time for each request. 
    
    Its string representation is formated for log displaying.
    '''
    timespan = None
    method = None
    data = None
    url = None
    status = None
    length = None
    
    _starttime = None
    
    def __init__(self, method, url, data=None):
        '''
        Constructor : registers method, URL and data.
        
        Arguments : 
            *method* : Request method
            *url* : The URL on which request is made.
        '''
        self.timespan = None
        self.status = None
        self.length = None
        self.method = method
        self.url = url
        self.data = data
        
    def start(self):
        '''
        Set now as start time. Start time is used for logging access and to
        calculate process time.
        '''
        self._starttime = datetime.today()
    
    def stop(self, response):
        '''
        When stop method is called, now is set as stop time. Response code
        and process time (timespan) are registered.  
        
        Arguments:
            *response* : The response returned.
        '''
        stoptime = datetime.today()
        delta = stoptime - self._starttime
        self.timespan = delta.days * 24 * 3600 * 1000 + delta.seconds * 1000 + delta.microseconds / 1000
        self.status = response.code
        
        try:
            self.length = int(response.info().getheader('Content-Length'))
        except (ValueError, TypeError):
            self.length = None
            
    def __repr__(self):
        '''
        Reprensentation of the trace formatted for clean logging.
        '''
        repr = '%s %s\n' % (self.method, self.url)
        repr += str(self.timespan) + 'ms' + '\t' + 'status : ' + str(self.status) + '\n'
        repr += '----\n'
        return repr
