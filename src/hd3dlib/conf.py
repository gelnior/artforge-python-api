# -*- coding: utf-8 -*-

dataServerName = '@@WEB_SERVICES--SERVER_NAME@@'#Mandatory
dataServerPort = @@WEB_SERVICES--SERVER_PORT@@ #Mandatory
dataServerRootUrl = '/@@WEB_SERVICES_PATH@@' #Mandatory
protocol = '@@WEB_SERVICES--HTTP_PROTOCOL@@' #http or https

#reflex script parameters
dataServerScriptLocation = '@@WEB_SERVICES_DATA_PATH@@/scripts/'
dataServerPythonPath = '@@SCRIPTS-PYTHON--PYTHON_PATH@@'
dataServerLogPath = '@@SCRIPTS-PYTHON--LOG_PATH@@'

defaultUser = '@@SCRIPTS-PYTHON--DEFAULT_USER@@'
defaultPassword = '@@SCRIPTS-PYTHON--DEFAULT_USER_PASSWORD@@'
