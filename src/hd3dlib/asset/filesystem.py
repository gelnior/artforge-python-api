# coding=utf-8

from hd3dlib.util import wsRequestHandler

class FileSystem(object):
    '''
    Utility class to access to file system resource.
    '''
    locator = '/filesystem/'

    def __init__(self):
        pass
    
    def postForm(self, target, files={}, fields={}):
        '''
        Allow to post file without creating file revision.
        
        Arguments:
            *target* The url to post files.
            *files* Files to post.
            *fields* Extra data sent to file system service.
        '''
        from hd3dlib.util import multipartformencode
        head, body = multipartformencode.encode(files, fields)
        print body
        return wsRequestHandler.post('%s%s' % (self.locator, str(target)), body, False, headers=head)
        
    def read(self, target):
        '''
        ''' 
        return wsRequestHandler.get('%s%s' % (self.locator, str(target)), 
                                    headers={
                                             'Content-Type':'text/plain', 
                                             'Accept':'text/plain'
                                    }
               )

