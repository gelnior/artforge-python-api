# coding=utf-8

import os, sys

from hd3dlib.reflex import evalArgDictionary
from hd3dlib.team import Person, ResourceGroup
from hd3dlib.production import Category, Constituent, Sequence, Shot
from hd3dlib.production import Project, ProjectType,Breakdown,Step, ApprovalNoteType
from hd3dlib.task import Task, TaskType, EntityTaskLink
from hd3dlib.admin import Role
from hd3dlib.planning import Planning
from hd3dlib.extra import Sheet,Item,ItemGroup
from hd3dlib.asset import FileRevision
from hd3dlib.util.odspreadsheet import ODSpreadsheet


import time
from datetime import timedelta
from datetime import date


global project
global groups
global shots
global constituents


global odsServerPath
global projectName

# type constants
ATTRIBUTE = 'attribute'
METHOD = 'method'
COLLECTIONQUERY = 'collectionQuery'

#collection query
STEP_QUERY ='fr.hd3d.services.resources.collectionquery.StepsByTaskTypeCollectionQuery'
APPROVALNOTE_QUERY ='fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery'
TASK_APPROVALNOTE_QUERY ='fr.hd3d.services.resources.collectionquery.TaskBoundEntityApprovalNotesCollectionQuery'
WORKOBJECT_QUERY='fr.hd3d.services.resources.collectionquery.TaskBoundEntityCollectionQuery'    

# metaType constants
JAVA_LANG_STRING = 'java.lang.String'
JAVA_LANG_BOOLEAN = 'java.lang.Boolean'
JAVA_LANG_LONG = 'java.lang.Long'
JAVA_LANG_DATE = 'java.util.Date'
JAVA_LANG_INTEGER = 'java.lang.Integer' 
APPROVAL_METATYPE = 'ApprovalNote'
JAVA_UTILS_LIST="java.utils.List"

# renderer constants
SHORT_TEXT = 'short-text'
BOOLEAN = 'boolean'
DATE = 'date'
STARTENDDATE = 'startEndDate'
SEQUENCE = 'Sequence'
INT = 'int'
STEP = 'step'
APPROVALNOTE = 'approval'


def initProject():
    global project
      
    typeSerie = ProjectType.getOrCreateByName("Serie")
    typeSerie.description="project type for serie"
    typeSerie.color="#00FF00"
    typeSerie.update()
    
    typePub = ProjectType.getOrCreateByName("Ad")
    typePub.description="project type for ad"
    typePub.color="#FF0000"
    typePub.update()
    
    typeLong = ProjectType.getOrCreateByName("feature film")
    typeLong.description="project type for feature film"
    typeLong.color="#0000FF"
    typeLong.update()
    
    if projectName :
        project = Project.getOrCreateByName(projectName)

    project.status = "OPEN"
    project.projectTypeId = typePub.id
    project.update()
  

def initTeams():
    initGroups()
    initPersons()
    initRoles()
      
    
def initGroups():
    
    #global mainGroup
    global groups
    groups = dict()
    
    #mainGroup = ResourceGroup.getOrCreateByName("Project")
    
    groups['Admin'] = ResourceGroup.getOrCreateByName("Admin")
    groups['Supervisor'] = ResourceGroup.getOrCreateByName("Supervisor")
    groups['Artist'] = ResourceGroup.getOrCreateByName("Artist")
    
    if not project.resourceGroupIDs:
        project.resourceGroupIDs = list()
        
    project.resourceGroupIDs.append(groups['Admin'].id)
    project.resourceGroupIDs.append(groups['Supervisor'].id)
    project.resourceGroupIDs.append(groups['Artist'].id)
    project.update()
    

def initPersons():
    
    for i in range(3):
        login = 'Supervisor%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Supervisor'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Supervisor'].id)
        person.update()

    groups["Supervisor"] = ResourceGroup.getOrCreateByName("Supervisor")
    
    for i in range(3):
        login = 'Admin%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Admin'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Admin'].id)
        person.update()
        
    groups['Admin'] = ResourceGroup.getOrCreateByName("Admin")

    for i in range(10):
        login = 'Artist%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Artist'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Artist'].id)
        person.update()
        
    groups['Artist'] = ResourceGroup.getOrCreateByName("Artist")
                        
            

def initRoles():
    role = Role.getOrCreateByName("admin")
     
    if not role.permissions :
        role.permissions=list()
        role.permissions.append("*:*")
     
    if not role.resourceGroupIDs :
        role.resourceGroupIDs=list()
        
    role.resourceGroupIDs.append(groups['Admin'].id)
    role.resourceGroupIDs.append(groups['Supervisor'].id)
    role.resourceGroupIDs.append(groups['Artist'].id)
    
    role.update()
     
        

def initBreakdown(odsServerPath):
    global constituents
    global shots
    shots =list()
    constituents=list()
    if odsServerPath != "":
        try:
            odsServerPath = os.path.basename(odsServerPath)
            f = FileRevision().query().filter('key', odsServerPath).getLast()
            odsServerPath = os.path.join(f.location , f.relativePath)  
            b = Breakdown()
            b.setODS(odsServerPath)
            b.setProjectDBId(project.id)
            b.processCategories()
            b.processConstituents()
            b.processSequences()
            b.processShots()
            b.createInDatabase()
            
           
            dictSequence=b.getEntityRecords('Sequence')
            dictShots=b.getEntityRecords('Shots');
            for shot in dictShots.values():
                sequence=Sequence.getByName(project, shot["sequence.name"]);
                shot=Shot.getOrCreateByName(project, sequence, shot["shot.label"])
                shots.append(shot)
                
            dictCategories=b.getEntityRecords('Categories')
            dictConstituents=b.getEntityRecords('Constituents');
            for constituent in dictConstituents.values():
                category=Category.getByName(project, constituent["category.name"]);
                constituent=Constituent.getOrCreateByName(project, category, constituent["constituent.label"])
                constituents.append(constituent)
        except Exception :
            print sys.exc_info()
            print "[Warning] errors  occurred, while reading the ods file %s. %s " % (odsServerPath,sys.exc_info()[1])
         
        
    initTaskType()
    initStep()
    initSheetBreakdown(project.id)
    
def initSheetBreakdown(projectId):

        Sheet._locator = 'sheets/'
        
        # Create sheet for breakdown
        constituentSheetBreak = Sheet.getOrCreate('Quote for library', 
                  'Quote for library',
                  projectId,
                  'Constituent',
                  'BREAKDOWN')
        print 'New sheet created : %s' % constituentSheetBreak.name
        
     
        
        # Create main group and items
        igDefault=ItemGroup.getOrCreate('Default',constituentSheetBreak.id)
        
        itLabel         = Item.getOrCreate(
                               'Label', #name
                               igDefault,#itemGroup
                               'label', #query
                               ATTRIBUTE, #type
                              JAVA_LANG_STRING, #metaType
                               '', #renderer
                               SHORT_TEXT, #editor
                               "" #transformerParameters
                                )
        
        itLabel         = Item.getOrCreate(
                               'Thumbnail', #name
                               igDefault,#itemGroup
                               'thumbnail', #query
                               METHOD, #type
                              JAVA_LANG_STRING, #metaType
                               'thumbnail', #renderer
                               '', #editor
                               "" #transformerParameters
                                )

        itDescription     = Item.getOrCreate(
                               'Description',
                               igDefault, 
                               'description', 
                               ATTRIBUTE,
                               JAVA_LANG_STRING,
                               '',SHORT_TEXT,"")
        
        igStep=ItemGroup.getOrCreate("Steps",constituentSheetBreak.id)
        
        taskType = taskTypes["Modeling"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Texturing"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Setup"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
              
        print 'Sheet creation finished : %s' % constituentSheetBreak.name
        
        # Create sheet for breakdown
        shotSheetBreak = Sheet.getOrCreate('Quote for Production', 
                  'Quote for Production',
                  projectId,
                  'Shot',
                  'BREAKDOWN')
        print 'New sheet created : %s' % shotSheetBreak.name
        
     
        
        # Create main group and items
        igDefault=ItemGroup.getOrCreate('Default',shotSheetBreak.id)
        
        itLabel         = Item.getOrCreate(
                               'Label', #name
                               igDefault,#itemGroup
                               'label', #query
                               ATTRIBUTE, #type
                              JAVA_LANG_STRING, #metaType
                               '', #renderer
                               SHORT_TEXT, #editor
                               "" #transformerParameters
                                )
        
        itLabel         = Item.getOrCreate(
                               'Thumbnail', #name
                               igDefault,#itemGroup
                               'thumbnail', #query
                               METHOD, #type
                              JAVA_LANG_STRING, #metaType
                               'thumbnail', #renderer
                               '', #editor
                               "" #transformerParameters
                                )

        itDescription     = Item.getOrCreate(
                               'Description',
                               igDefault, 
                               'description', 
                               ATTRIBUTE,
                               JAVA_LANG_STRING,
                               '',SHORT_TEXT,"")
        
        igStep=ItemGroup.getOrCreate("Steps",shotSheetBreak.id)
        
        taskType = taskTypes["Layout"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Animation"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Lighting"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Rendering"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
            
        taskType = taskTypes["Compositing"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
                
        print 'Sheet creation finished : %s' % shotSheetBreak.name
        
    
def initTaskType():
    global taskTypes
    global approvalNoteTypes
    taskTypes=dict()
    approvalNoteTypes=dict()
    
    taskTypeName="Animation"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#2222FF");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Modeling"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#22FF22");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Texturing"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#FF2222");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Setup"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#556677");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Layout"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#44AA77");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Lighting"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#995577");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Rendering"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#BB55CC");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Compositing"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#ABCDEF");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    approvalNoteType=ApprovalNoteType.getOrCreate("Director Approval",project.id)
    approvalNoteTypes["Director Approval"]=approvalNoteType
    
    
def initStep(): 
    taskType =taskTypes["Modeling"]
    
    for constituent in constituents:
        step = Step.getOrCreate(taskType,constituent,"Constituent",2*28800)
    
def initProduction():
    initSheetProduction()
    
def initSheetProduction():
      # Create sheet for production
        constituentSheetProd = Sheet.getOrCreate('Constituents tracking', 
                  'Constituents tracking',
                  project.id,
                  'Constituent',
                  'PRODUCTION')
        print 'New sheet created : %s' % constituentSheetProd.name
        
         # Create main group and items
        igDefault=ItemGroup.getOrCreate('Default',constituentSheetProd.id)
        
        itLabel         = Item.getOrCreate(
                               'Label', #name
                               igDefault,#itemGroup
                               'label', #query
                               ATTRIBUTE, #type
                              JAVA_LANG_STRING, #metaType
                               '', #renderer
                               SHORT_TEXT, #editor
                               "" #transformerParameters
                                )
        
        itLabel         = Item.getOrCreate(
                               'Thumbnail', #name
                               igDefault,#itemGroup
                               'thumbnail', #query
                               METHOD, #type
                              JAVA_LANG_STRING, #metaType
                               'thumbnail', #renderer
                               '', #editor
                               "" #transformerParameters
                                )

        itDescription     = Item.getOrCreate(
                               'Description',
                               igDefault, 
                               'description', 
                               ATTRIBUTE,
                               JAVA_LANG_STRING,
                               '',SHORT_TEXT,"")
        
        igApproval=ItemGroup.getOrCreate("Approval notes",constituentSheetProd.id)
        
        approvalNoteType = approvalNoteTypes["Modeling Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Texturing Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY,
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Setup Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Director Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
              
        print 'Sheet creation finished : %s' % constituentSheetProd.name
        
        # Create sheet for breakdown
        shotSheetProd = Sheet.getOrCreate('Shots tracking', 
                  'Shots tracking',
                  project.id,
                  'Shot',
                  'PRODUCTION')
        print 'New sheet created : %s' % shotSheetProd.name
        
     
        
        # Create main group and items
        igDefault=ItemGroup.getOrCreate('Default',shotSheetProd.id)
        
        itLabel         = Item.getOrCreate(
                               'Label', #name
                               igDefault,#itemGroup
                               'label', #query
                               ATTRIBUTE, #type
                              JAVA_LANG_STRING, #metaType
                               '', #renderer
                               SHORT_TEXT, #editor
                               "" #transformerParameters
                                )
        
        itLabel         = Item.getOrCreate(
                               'Thumbnail', #name
                               igDefault,#itemGroup
                               'thumbnail', #query
                               METHOD, #type
                              JAVA_LANG_STRING, #metaType
                               'thumbnail', #renderer
                               '', #editor
                               "" #transformerParameters
                                )

        itDescription     = Item.getOrCreate(
                               'Description',
                               igDefault, 
                               'description', 
                               ATTRIBUTE,
                               JAVA_LANG_STRING,
                               '',SHORT_TEXT,"")
        
        igApproval=ItemGroup.getOrCreate("Approval notes",shotSheetProd.id)
        
        approvalNoteType = approvalNoteTypes["Layout Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Animation Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Lighting Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Rendering Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
            
        approvalNoteType = approvalNoteTypes["Compositing Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Director Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
                
        print 'Sheet creation finished : %s' % shotSheetProd.name
    
def initTaskManager():
    initSheetTaskManager()
    initTasks()

def initSheetTaskManager():
    Sheet._locator = 'sheets/'
    
    #create sheet
    sheet = Sheet.getOrCreate("Tasks", 
              'Standard task sheet',
              project.id,
              'Task','TASK')
    
    print 'New sheet created : %s' % sheet.name
    
    #create main group and items
    igDefault=ItemGroup.getOrCreate('Default',sheet.id)
    
    
    item         = Item.getOrCreate('Work Object', 
                            igDefault,
                           WORKOBJECT_QUERY, 
                           COLLECTIONQUERY, 
                           'IBase',
                           'work-object','',"")
    
    
    item         = Item.getOrCreate('Task Type',
                            igDefault,
                           'taskType', 
                           ATTRIBUTE, 
                           JAVA_LANG_STRING,
                           'task-type','',
                           "")
    
    item         = Item.getOrCreate('Status Notes', igDefault,
                           TASK_APPROVALNOTE_QUERY, 
                           COLLECTIONQUERY, 
                           JAVA_UTILS_LIST,
                           'approval','approval',""
                           )
    
    item         = Item.getOrCreate('Status', igDefault,
                           'status', 
                           ATTRIBUTE, 
                           'ETaskStatus',
                           'task-status','',""
                           )
    
    item         = Item.getOrCreate('Worker', 
                           igDefault,
                           'worker', 
                           ATTRIBUTE, 
                           'Person',
                           'person-task','',"")
    
    item         = Item.getOrCreate('Duration',
                           igDefault, 
                           'duration', 
                           ATTRIBUTE, 
                           JAVA_LANG_LONG,
                           'duration','',"")
    
    item         = Item.getOrCreate('Start date',
                            igDefault,   
                            'startDate', 
                            ATTRIBUTE, 
                            JAVA_LANG_DATE,
                            'date-and-day',
                            DATE,
                           "")
    
    
    item         = Item.getOrCreate('End date',
                            igDefault, 
                            'endDate', 
                            ATTRIBUTE, 
                            JAVA_LANG_DATE,
                            'date-and-day',
                            DATE,
                           "")
    
    print 'Sheet creation finished : %s' % sheet.name
    
def initTasks():    
    modeling = taskTypes["Modeling"]
    taskDate = date.today()
    
    i=0
    for constituent in constituents:
        tasks=Task.getTaskByWOTasktype(constituent, 'Constituent', modeling)
        for task in tasks:
            task.startDate = taskDate.strftime('%Y-%m-%d %H:%M:%S')
            duration = timedelta(days=task.duration/28800)#28800 = nb temps de travail par jour en secondes
            taskDate=(taskDate + duration)
            task.endDate = taskDate.strftime('%Y-%m-%d %H:%M:%S')
            taskDate=taskDate+timedelta(days=1)
            size=groups["Artist"].resourceIDs.__sizeof__()
            task.worker =groups["Artist"].resourceIDs.pop()
            groups["Artist"].resourceIDs.insert(0,task.worker)
            task.update()
            
        i=i+1
            
def initPlanning():
    
    planning= Planning.getOrCreateByProject(project.id)
    today = date.today()
    duration = timedelta(days=2)
    planning.startDate=(today-duration).strftime('%Y-%m-%d %H:%M:%S')
    duration = timedelta(days=30)
    planning.endDate=(today+duration).strftime('%Y-%m-%d %H:%M:%S')
    planning.master=True
    planning.name=project.name.encode()
    
    substitute=dict()
    substitute["projects"]=project.id
    Planning._locator=Planning._locatorTmpl.safe_substitute(substitute)
    
    planning.update()
    
    
def doIt():
    initProject()
    initTeams()
    initBreakdown(odsServerPath)
    initTaskManager()
    initProduction()
    #initPlanning() //nouvelle version initialisation du planning automatique
    
    
if __name__ == '__main__':
   
    odsServerPath=""
    projectName="test"
    args=dict()
    
    if len(sys.argv) > 1 :
        args=evalArgDictionary(sys.argv[1])

    if args.has_key("odsFile"):
        odsServerPath=args["odsFile"]
   
    if args.has_key("projectName"):
        projectName=args["projectName"]                         
    
    
     
    print '[INFO] feature movie Demo init starts.'
    doIt()
    print '[INFO] feature movie Demo init finished.'
    