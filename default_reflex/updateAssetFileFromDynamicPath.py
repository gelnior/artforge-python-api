#coding utf-8

from hd3dlib.asset import DynamicPath
from hd3dlib.production import Constituent, Shot, Sequence, Category, Project
from hd3dlib.task import TaskType
from hd3dlib.reflex import evalArgDictionary
from hd3dlib import conf

from hd3dlib.util import DynamicPathUtils

import os

global projectId
global assetDir

def doIt():

    #dynamicPathsList = DynamicPath.getByProjectAndType(projectId,DynamicPath.type_file);
    global project
    project = Project.get(projectId)


    constituents = Constituent.getByProject(projectId)
    global constituentDict
    constituentDict = dict()
    if constituents is not None :
        for constituent in constituents:
            constituentDict[constituent.id] = constituent


    shots = Shot.getByProject(projectId)
    global shotDict
    shotDict = dict()
    if shots is not None :
        for shot in shots:
            shotDict[shot.id] = shot


    categories = Category.getByProject(projectId)
    global categoriesDict
    categoriesDict = dict()
    for category in categories:
        categoriesDict[category.id] = category

    sequences = Sequence.getByProject(projectId)
    global sequencesDict
    sequencesDict = dict()
    for sequence in sequences:
        sequencesDict[sequence.id] = sequence

    if constituents:
        for constituent in constituents:
            assets = constituent.getAssets()
            if assets:
                for asset in assets:
                    #get dynamic paths corresponding to asset
                    #dynamicPath=getDynamicPathConstituent(asset, categoriesDict)
                    dynamicPath = DynamicPathUtils.getDynamicPathFromConstituent(projectId, DynamicPath.type_file, constituent, asset.taskType)
                    if not dynamicPath:
                        print "[Error] no dynamic path found to work object -> %s" % constituent.label
                        continue



                    #solve the dynamic paths
                    wipPath = resolvePathConstituent(constituent, dynamicPath.wipDynamicPath, asset.taskTypeName);
                    publishPath = resolvePathConstituent(constituent, dynamicPath.publishDynamicPath, asset.taskTypeName);
                    oldPath = resolvePathConstituent(constituent, dynamicPath.oldDynamicPath, asset.taskTypeName);

                    # create the repository
                    createDirectory(assetDir + wipPath)
                    createDirectory(assetDir + publishPath)
                    createDirectory(assetDir + oldPath)

                    # update asset with solved dynamic paths
                    updateAsset(asset, wipPath, publishPath, oldPath)

    if shots :
        for shot in shots:
            assets = shot.getAssets()
            if assets :
                for asset in assets:
                    #get dynamic paths corresponding to asset
                    #dynamicPath=getDynamicPathShot(asset, sequencesDict)
                    dynamicPath = DynamicPathUtils.getDynamicPathFromShot(projectId, DynamicPath.type_file, shot, asset.taskType)
                    if not dynamicPath:
                        print "[Error] no dynamic path found to work object -> %s" % shot.label
                        continue

                    #solve the dynamic paths
                    wipPath = assetDir + resolvePathShot(shot, dynamicPath.wipDynamicPath, asset.taskTypeName);
                    publishPath = assetDir + resolvePathShot(shot, dynamicPath.publishDynamicPath, asset.taskTypeName);
                    oldPath = assetDir + resolvePathShot(shot, dynamicPath.oldDynamicPath, asset.taskTypeName);


                    # create the repository
                    createDirectory(wipPath)
                    createDirectory(publishPath)
                    createDirectory(oldPath)

                    updateAsset(asset, wipPath, publishPath, oldPath)


def updateAsset(asset, wipPath, publishPath, oldPath):
    '''
    update asset with paths.
    
    Arguments:
            *asset* the asset to update.
            *wipPath* the path to wip files.
            *publishPath* the path to publish files.
            *oldPath*    the path to old files
    '''
    asset.wipPath = str(wipPath)
    asset.publishPath = str(publishPath)
    asset.oldPath = str(oldPath)
    asset.update()


def createDirectory(dir):
    '''
    Create a directory according to the given directory path. If a variable (ex : ${user} ) is in directory path, it create the directories before the variable.
    All directory, which doesn't exist, will be create.
    
    Arguments:
            *dir* the directory to create.
    '''
    if not dir :
        return
    if dir.count('$'):
        dir = dir[:dir.find('$')]
    if not os.path.isdir(dir):
        os.makedirs(dir)



def resolvePathConstituent(constituent, path, taskTypeName):
    '''
    Resolve the path according to the miscellaneous parameters for a constituent.
    
    Arguments:
            *constituent* the constituent.
            *path* the path to resolve
            *taskTypeName* the task type name
    '''
    if not path:
        return ""
    path = resolveCommun(path, taskTypeName)

    path = path.replace("${category}", categoriesDict[constituent.category].name);
    path = path.replace("${constituent}", constituent.label);
    return path

def resolvePathShot(shot, path, taskTypeName):
    '''
    Resolve the path according to the miscellaneous parameters for a shot.
    
     Arguments:
            *shot* the shot.
            *path* the path to resolve
            *taskTypeName* the task type name
    '''
    if not path:
        return ""
    path = resolveCommun(path, taskTypeName)

    path = path.replace("${sequence}", sequencesDict[shot.sequence].name);
    path = path.replace("${shot}", shot.label);
    return path

def resolveCommun(path, taskTypeName):
    '''
    Resolve the path according to the miscellaneous parameters.
    
     Arguments:
            *path* the path to resolve
            *taskTypeName* the task type name
    '''
    if not path:
        return ""
    path = path.replace("${project}", project.name);
    path = path.replace("${tasktype}", taskTypeName);
    return path


if __name__ == '__main__':
    import sys
    projectId = 1282
    assetDir = ""

    args = dict()

    if len(sys.argv) > 1 :
        args = evalArgDictionary(sys.argv[1])[0]



    if args.has_key("projectId"):
        projectId = args["projectId"]

    if args.has_key("assetDir"):
        assetDir = args["assetDir"]

    #print "project: %s, assetDir:%s" % (projectId, assetDir)

    doIt()


