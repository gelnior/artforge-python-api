# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


PROCESSOR_DEFINITION = {
    'name':'ProcessorGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LProcessor', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
    }
}

class Processor(makeClass(PROCESSOR_DEFINITION, BaseObject)):
    '''
    Processor allow to link processors with computers.
    '''

    _locatorTmpl = Template('/processors/')
    _locatorShort = '/processors/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()