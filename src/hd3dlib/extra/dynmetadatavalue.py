# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

DYN_METADATA_VALUE_DEFINITION = {
    'name':'DynMetaDataValueGenerated',
        'attributes':{
            '_class': Attribute(
                'class', 
                str, 
                default='fr.hd3d.model.lightweight.impl.LDynMetaDataValue', 
                mutable=False
            ),
            'parent':Attribute('parent', long),
            'parentType':Attribute('parentType', str),
            'value':Attribute('value',str),
            'classDynMetaDataType':Attribute('classDynMetaDataType',long),
            'classDynMetaDataTypeName':Attribute('classDynMetaDataTypeName',str)
        }
}

class DynMetaDataValue(makeClass(DYN_METADATA_VALUE_DEFINITION, BaseObject)):
    '''
    Dynamic attribute value for an object instance. 
    '''
    
    _locatorTmpl = Template('/dyn-metadata-values/')
    _locatorShort = '/dyn-metadata-values/'
    _locator = '/dyn-metadata-values/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        
    @classmethod
    def query(cls):
        '''
        Return query object for DynMetaDataValue.
        '''
        return BaseObject.queryClass(cls) 