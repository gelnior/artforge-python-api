# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template


RESOURCEGROUP_DEFINITION = {
    'name':'GroupGenerated',
    'attributes':{
        '_class' : Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LResourceGroup', 
            mutable=False
        ),
        "name" : Attribute('name', str),
        "parent" : Attribute('parent', long),
        "leader" : Attribute('leader', long),
        "leaderName" : Attribute('leaderName', long),
        "resourceNames" : Attribute('resourceNames', list, mutable=False),
        "resourceIDs" : Attribute('resourceIDs', list, mutable=False),
        "roleNames" : Attribute('roleNames', list, mutable=False),
        "roleIDs" : Attribute('roleIDs', list, mutable=False),
        "projectNames" : Attribute('projectNames', list, mutable=False),
        "projectIDs" : Attribute('projectIDs', list, mutable=False),
        
        "hasAccount" : Attribute('hasAccount', bool, mutable=False),
        "userCanDelete" : Attribute('userCanDelete', bool, default=False),
        "userCanUpdate" : Attribute('userCanUpdate', bool, default=False),
        "skillLevelNames" : Attribute('skillLevelNames', list, mutable=False),
        "skillLevelIDs" : Attribute('skillLevelIDs', list, mutable=False),
        "defaultPath" : Attribute('defaultPath', str, mutable=False),
    }
}


class ResourceGroup(makeClass(RESOURCEGROUP_DEFINITION, BaseObject)):
    '''
    Resource group is a list of resources. Resources can be a person, a machine,
    a device...
    Rights are granted to it via role.    
    '''
    
    _locatorTmpl = Template('/resource-groups/')
    _locatorShort = '/resource-groups/'
    _locator = ''

    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()
    

    @classmethod
    def getOrCreateByName(cls, name, parentId=None):
        '''    
        Retrieves the group of which the name is equal name parameter and
        the parent id is equal to parent (if given).         
        If the group does not exist, it is created.
        
        Arguments :
            *name* Name of the group to retrieve or create.    
            *parent* Id of the parent of the group to retrieve or create.
        '''
        
        if parentId is None or isinstance(parentId, long) or isinstance(parentId, int):
            if parentId is None:
                obj = cls.query().filter('name', name).filter_custom('isnull', 'parent' ,None).getFirst()
            else:
                obj = cls.query().filter('name', name).filter('parent', parentId).getFirst()
            
            if obj is None:
                obj = ResourceGroup(name = name)
                if parentId:
                    obj.parent = parentId
                obj.create()
                print 'Group %s created' %  obj.name
                
        else:            
            raise Exception('parentId should be a long, group has not been created.')
        
        return obj
    
    