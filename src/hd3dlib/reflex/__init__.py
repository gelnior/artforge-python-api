# coding=utf-8
'''
Reflex library to run reflex scripts while handling their logs.
'''

__all__ = [ 'Log', 'BaseReflex' ]

from log import Log
from baseReflex import BaseReflex

def evalArgDictionary(dictAsAString):
    ''' 
    Utility method for cross platform dictionary parsing.
    '''
    from platform import platform
    
    if platform().startswith('Windows'):
        return eval(dictAsAString)
    else:
        return eval(eval(dictAsAString))