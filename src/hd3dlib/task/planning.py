# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject
from string import Template

SCRIPT_DEFINITION = {
    'name':'PlanningGenerated',
    'attributes':{
        'class':Attribute(
            'class', 
            str, 
            default='fr.hd3d.model.lightweight.impl.LPlanning'
        ),
        'name':Attribute('name', str),
        'project':Attribute('project', long),
        'master':Attribute('master', int),
        'startDate':Attribute('startDate', str),
        'endDate':Attribute('endDate', str)
    }
}

class Planning(makeClass(SCRIPT_DEFINITION, BaseObject)):
    _locatorTmpl = Template('/projects/$projects/plannings/')
    _locatorShort = '/plannings/'
    _locator = '/plannings/'

 
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
    
    @classmethod
    def _initByUrl(self, url):
        urlSplit = url.split('/')
        templateSplit = self._locatorTmpl.template.split('/')
        diffIndex = len(urlSplit) - len(templateSplit)
        urlValues = {}
        for i in range(len(templateSplit)):
            if urlSplit[i + diffIndex] != templateSplit[i]:
                urlValues[templateSplit[i]] = long(urlSplit[i + diffIndex])
        print urlValues
        project = urlValues['$projects']
        if project is not None:
            self._locatorShort = self._locatorTmpl.safe_substitute(projects=project)
            self._locator = self._locatorShort
            print self._locatorShort
        
    def _resetLocator(self):
        if self.project is not None:
            self._locatorShort = self._locatorTmpl.safe_substitute(projects=self.project)
            self._locator = self._locatorShort
            print self._locatorShort
    