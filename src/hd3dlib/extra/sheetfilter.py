# coding=utf-8
from hd3dlib.baseobject import BaseObject
from hd3dlib.util.classbuild import makeClass, Attribute

from string import Template


SHEETFILTER_DEFINITION = {
        'name':'SheetFilterGenerated',
        'attributes':{
            '_class':Attribute(
                'class', 
                str, 
                default='fr.hd3d.model.lightweight.impl.LSheetFilter', 
                mutable=False
            ),
            'name':Attribute('name', str), 
            'sheetId':Attribute('sheetId', long),
            'filter':Attribute('filter', str),
    }
}

class SheetFilter(makeClass(SHEETFILTER_DEFINITION, BaseObject)):
    '''
    Sheet Filter allows user to make a filter on data of a view. A sheet filter linked to a sheet. 
    '''
    
    _locatorTmpl = Template('/sheet-filters/')
    _locatorShort = '/sheet-filters/'
    _locator = '/sheet-filters/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self,**kw)

        
   
    
    @classmethod
    def getOrCreate(cls, name, sheetId, filter=""):
        '''
        Return the sheet filter of which name is equal to name and sheet id. 
        It creates it if the sheet filter does not already exists.
        
        Arguments:
            *name* sheet filter name.
            *sheetId* the project id of the sheet
            *filter* filter, use if it need to create
        '''
        
        sheetfilter = SheetFilter.getByName(name, sheetId)
        
        if not sheetfilter :
            sheetfilter = SheetFilter()
            sheetfilter.name = str(name)
            sheetfilter.sheetId=sheetId
            sheetfilter.filter=str(filter)
            sheetfilter.create()
           
        return sheetfilter
    
    @classmethod
    def getByName(cls, name, sheetId):
        '''
        Return the sheet filter of which name is equal to name to a given sheet .
        
        Arguments:
            *name* Name of sheet filter to retrieve
            *sheetId* the sheet id 
        '''   
        return SheetFilter.query().filter('name', name).filter('sheetId',sheetId).getFirst()
    
    @classmethod
    def getBySheet(cls,sheetId):
        '''
        Return sheet filters for a sheet.
        
        Arguments:
            *sheetId* the sheet id
        '''
        
        return SheetFilter.query().filter('sheet',sheetId).getAll()