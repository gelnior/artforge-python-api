# coding=utf-8
'''
URL Parameters

Set of classes to filter on queried data, to be used especially with collection 
resources (equivalent to an SQL `WHERE` clause).
All this classes are needed by queries to parameterized service call. Filters
are written as a JSON string and set as a GET parameter. 
'''

__all__ = [ 'Filter',
            'NotLogic',
            'AndLogic',
            'OrLogic',
            'Constraint',
            'OrderBy',
            'Pagination', ]

try: 
    import simplejson as json
except ImportError:
    import json
    
class Filter(dict):
    '''
    A basic filter,  
    The column name (column parameter) has to be a field name from the returned data.
    The type of the filter may be :
    
       * eq (equals)
       * neq (not equals)
       * lt (less than)
       * gt (greater than)
       * leq (less or equal)
       * get (greater or equal)
       * in (matches any value in the list passed)
       * notin (matches no element in the list passed)
       * like (implements the SQL `LIKE` string comparison)
       * isnull (is null)
       * isnotnull (is not null)
       * btw (between the `start` and the `end` values from the dict passed)
              
    Filters are dict for easy translation at JSON format.
    '''
    type = property(lambda self: self['type'])
    column = property(lambda self: self['column'])
    value = property(lambda self: self['value'])
    

    def __init__(self, type, column, value):
        '''
        Arguments :
            *type* filter type : equality, less than, more than, etc.
            *column* column on which the filter is applied
            *value* filter value
        '''
        if type is not None:
            self['type'] = type
        if column is not None:
            self['column'] = column
        if value is not None:
            self['value'] = value

class LogicAbstract(dict):
    '''
    Abstract class implementing logic operators
    
    All logic parameters are dict for easy translation at JSON format.
    '''
    logic = property(lambda self: self['logic'])
    filter = property(lambda self: self['filter'])
    
    def __init__(self):
        raise NotImplementedError('Can not instantiate LogicAbstract directly')
        
class NotLogic(LogicAbstract):
    '''
    Implementation of the NOT logic constraint.
    
    Constructor takes only one filter.
    '''
    def __init__(self, filter):
        self['logic'] = 'NOT'
        self['filter'] = [filter]

class AndLogic(LogicAbstract):
    '''
    Implementation of the AND logic.
    
    Constructor takes two or more filters.
    '''
    def __init__(self, *filters):
        self['logic'] = 'AND'
        self['filter'] = filters

class OrLogic(LogicAbstract):
    '''
    Implementation of the OR logic.
    
    Constructor takes two or more filters.
    '''
    def __init__(self, *filters):
        self['logic'] = 'OR'
        self['filter'] = filters
        
class Constraint(list):
    '''
    Constraint is a filter list.
    '''
    def __init__(self):
        list.__init__(self)
        pass
        
    def tojson(self):
        '''
        Converts the constraint object to a query string argument.        
        '''
        return 'constraint', json.dumps(self, ensure_ascii=False)

class OrderBy(list):
    '''
    Order by is used to sort retrieved data.
    '''
    def __init__(self, *fields):
        '''
        Constructor
        
        Arguments :
            *fields* Fields on which to sort. Fields prefixed with - are 
            descending sorted.
        '''
        list.__init__(self)
        self.extend(fields)
        
    def tojson(self):
        '''
        Converts the constraint object to a query string argument.
        '''
        return 'orderBy', json.dumps(self)
        
        
class Pagination(dict):
    '''
    Pagination allow to retrieve a predefined quantity of data from a big list.
    It starts at *first* (begin index) and retrieves *quantity* of records 
    from this start point.
    '''
    def __init__(self, first, count):
        self['first'] = first
        self['quantity'] = count
        
    def tojson(self):
        '''
        Converts the constraint object to a query string argument.
        '''
        return 'pagination', json.dumps(self)
    
    
class Bulk(dict):
    '''
    A simple parameter to add when you post a list of data instead of 
    a single data (check *query.postAll(list)*). 
    '''        
    def tojson(self):
        '''
        Converts the constraint object to a query string argument.
        '''
        return 'mode', 'bulk'

class Dyn(dict):
    '''
    A simple parameter that asks to services to retrieve also dynamic data.
    This request is slower than no dynamic data requests.
    '''        
    def tojson(self):
        '''
        Converts the constraint object to a query string argument.
        '''
        return 'dyn', 'true'
