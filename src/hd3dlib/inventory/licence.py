# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib import BaseObject
from string import Template


LICENCE_DEFINITION = {
    'name':'LicenceGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LLicence', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
        "billingReference" : Attribute('billingReference', str),
        "purchaseDate" : Attribute('purchaseDate', str),
        "warrantyEnd" : Attribute('warrantyEnd', str),
        
        "type" : Attribute('type', str),
        "serial" : Attribute('serial', str),
        "inventoryStatus" : Attribute('inventoryStatus', str),
        
        "software" : Attribute('softwareId', long),
        "softwareName" : Attribute('softwareName', str),
        
        "computerIDs" : Attribute('computerIDs', list),
        "computerNames" : Attribute('computerNames', list),
        
        "resourceGroupIDs" : Attribute('resourceGroupIDs', list),
        "resourceGroupNames" : Attribute('resourceGroupNames', list),
        
        "projectIDs" : Attribute('projectIDs', list),
        "projectNames" : Attribute('projectNames', list)
    }
}

class Licence(makeClass(LICENCE_DEFINITION, BaseObject)):
    '''
    Licence describe the right to use a specific software.
    '''

    _locatorTmpl = Template('/licences/')
    _locatorShort = '/licences/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()


    @classmethod
    def query(cls):
        '''
        Return query object for item group.
        '''
        return BaseObject.queryClass(cls) 
