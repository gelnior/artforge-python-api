# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

SCRIPT_DEFINITION = {
    'name':'TaskActivityGenerated',
    'attributes':{
        '_class':Attribute(
           'class', 
           str, 
           default='fr.hd3d.model.lightweight.impl.LTaskActivity', 
           mutable=False
       ),           
       'worker':Attribute('worker', long),
       'duration':Attribute('duration', int),
       'completion':Attribute('completion', int),
       'project':Attribute('projectID', long),
       'dayDate':Attribute('dayDate', str),
       'day':Attribute('dayId', str),
       'task':Attribute('taskID', long),
    }
}


class TaskActivity(makeClass(SCRIPT_DEFINITION, BaseObject)):
    '''
    Object to set time worked on a task. It is linked to a task and day object. 
    Day object represent a date for a given worker.
    '''
    
    _locatorTmpl = Template('/task-activities/')
    _locatorShort = '/task-activities/'
    _locator = '/task-activities/'
    
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self.completion = 0
        self.startable = True
        
        if self.project is not None:
            self._resetLocator()

    def _resetLocator(self):
        self._locatorTmpl = Template(TaskActivity._locatorTmpl.template)
        self._locator = self._locatorTmpl.safe_substitute(projects=self.project)
        
