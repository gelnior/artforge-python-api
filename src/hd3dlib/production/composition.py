# coding=utf-8

from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.baseobject import BaseObject

from string import Template

COMPOSITION_DEFINITION = {
    'name':'ShotGenerated',
    'attributes':{
          '_class' : Attribute(
               'class', 
               str, 
               default='fr.hd3d.model.lightweight.impl.LComposition'
           ),
          'name' : Attribute('name', str),
          'description' : Attribute('description', str),
          'constituent' : Attribute('constituent', long),
          'shot' : Attribute('shot', long, default=-1l),
          'sequence' : Attribute('sequence', long, default=-1l),
          'nbOccurence' : Attribute('nbOccurence', int, default=1)
     }
}

class Composition(makeClass(COMPOSITION_DEFINITION, BaseObject)):
    '''
    Composition allows to make casting. It means associating constituents with 
    shot. So you know, the composition of a shot.
    '''

    _locatorTmpl = Template('')
    _locatorShort = '/compositions/'
    _locator = '/compositions/'
        
    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        
        if len(kw) is not 0:
            if self.name is None or self.name == '': 
                self.name = "Composition " + self._jicId    
            
    def _resetLocatorForConstituent(self, projectId, categoryId):
        '''
        Set URL locator with following pattern : 
        projects/projectId/categories/categoryId/constituents/self.constituent/compositions/
        
        Arguments: 
            *proejctId* Id of project in which composition is.
            *categoryId* Id of category in which constituent set in composition is.
        '''
        const = self.constituent
        self._locator = '/projects/%d/categories/%d/constituents/%d/compositions/' % (projectId, categoryId, const)
         

    def _resetLocatorForShot(self, projectId, sequenceId):
        '''
        Set URL locator with following pattern : 
        projects/projectId/categories/categoryId/shots/self.shot/compositions/
        
        Arguments: 
            *projectId* Id of project in which composition is.
            *sequenceId* Id of sequence in which shot set in composition is.
        '''
        shot = self.shot
        self._locator = '/projects/%d/sequences/%d/shots/%d/compositions/' % (projectId, sequenceId, shot)
   
    
    @classmethod
    def getByShot(cls, projectId, sequenceId, shotId):
        '''
        Return compositions linked to shot of which id is given in parameter.
        
        Arguments:
            *projectId* ID of project in which compositions to retrieve are.            
            *sequenceId* ID of sequence in which compositions to retrieve are.
            *shotId* ID of shot linked to compositions to retrieve.
        '''
        req = cls.query().branch('projects', projectId)
        req = req.branch('sequences', sequenceId)
        req = req.branch('shots', shotId)
        return req.leaf('/compositions').getAll()
        
    @classmethod
    def query(cls):
        '''
        Return query object for Composition.
        '''
        return BaseObject.queryClass(cls) 