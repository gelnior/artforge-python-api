import os
import sys

from hd3dlib import conf
from log import Log

class BaseReflex():    
    '''
    BaseReflex can be used to make easy reflex script building. Once extended
    it initializes log tools and provides some informations :
    
    * self.event : The event which triggered the script. 
    * self.entitype : The type of instance concerned by the event.
    * self.url : The URL of the instance concerned by the event.
    * self.entityId : The id of the instance concerned by the event.
    '''
    scriptName = None
    event = None
    entityType = None
    url = None
    entityId = None
    entityIds = list()
    log = None
    
    def __init__(self, scriptPath):
        ''' 
        Constructor : it extracts all context variable values received from
        script call. The path of the launch script must be given in parameters.
        
        It also starts logging.
        
        Arguments :
            scriptPath The path of the script to run.
        ''' 
        self.scriptPath = scriptPath
        if len(sys.argv) == 4:
            self.event = sys.argv[1]
            self.entityType = sys.argv[2]
            self.url = sys.argv[3]
            
            if self.url.startswith("[") and len(self.url) > 2:
            
                # for bulk creation, new uri come as follows [uri1,uri2,...]
                self.url = self.url.lstrip("[")
                self.url = self.url.rstrip("]")
                
                urls = self.url.split(",")                
                self.entityId = self._findObjectId(urls[0])
                
                for tmpUrl in urls:
                    self.entityIds.append(self._findObjectId(tmpUrl))
                
            else:
                self.entityId = self._findObjectId(self.url)
                self.entityIds.append(self.entityId)
        
        self._initLog()
    
    def __del__(self):
        '''
        On object destruction, logging is stopped.
        '''
        self.log.stop()
    
    def _initLog(self):
        '''
        Start logging action in folder specified in configuration file.
        
        The file name for logs is based on the script name.
        '''
        scriptName = os.path.basename(self.scriptPath)
        logLocation =  conf.dataServerLogPath
        self.log = Log(scriptName, logLocation)
        self.log.enableConsole()
        self.log.start()

            
    def _findObjectId(self, url):
        """
        Find object id in *url*.
        Supported url sample :
         
        * http://localhost/Hd3dServices/v1/sheets/323/item-groups/327/?objectId=309
        * http://localhost/Hd3dServices/v1/constituents/309
        
        :arguments:
        url
            URL form which object id is extracted.        
        """        
        id = 0
        
        
        if( url.count("sheets") + url.count("item-groups") + url.count("/?objectId=") == 3 ):
            # partial PUT in explorer
            urlParts = url.split("=")    
        else:
            # classic address
            urlParts = url.split("/")
    
        print "DEBUG CREATE PROJECT SHEETS"
        print urlParts[-1]
        if (len(urlParts) > 0) and (urlParts[-1].isdigit()):            
            id = long(urlParts[-1])
        return id

    
if __name__ == '__main__':
    reflex = BaseReflex()
    