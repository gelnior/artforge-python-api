# coding=utf-8
from hd3dlib.util.classbuild import makeClass, Attribute
from hd3dlib.inventory import Computer
from hd3dlib import BaseObject
from string import Template


STUDIOMAP_DEFINITION = {
    'name':'StudioMapGenerated',
    'attributes':{
        '_class' : Attribute(
            'class',
            str, 
            default='fr.hd3d.model.lightweight.impl.LStudioMap', 
            mutable=False
        ),
        
        "name" : Attribute('name', str),
        "imagePath" : Attribute('imagePath', str),
        "sizeX" : Attribute('sizeX', int),
        "sizeY" : Attribute('sizeY', int),
    }
}

class StudioMap(makeClass(STUDIOMAP_DEFINITION, BaseObject)):
    '''
    Computer describes a computer machine and its links with other devices
    or softwares.
    '''

    _locatorTmpl = Template('/studiomaps/')
    _locatorShort = '/studiomaps/'
    _locator = ''


    def __init__(self, **kw):
        BaseObject.__init__(self, **kw)
        self._resetLocator()

    def getAllComputers(self):        
        '''
        Return list of computers on the map.
        '''
        return Computer.query().branch('studioMap', self.id).getAll()

