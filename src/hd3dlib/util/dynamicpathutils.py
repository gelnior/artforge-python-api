

class DynamicPathUtils(object):
    
    @classmethod
    def getDynamicPathFromConstituent(cls, projectId, 
                                      dynamicPathType, constituent, taskType):
        '''
        Return the dynamic path of the given constituent and the given task type.
        
         Arguments:
               *projectId* the project id
                *dynamicPathType* the type of dynamic path retrieved
                *constituent* the constituent
                *taskType* the task type
        '''     
        from hd3dlib.asset.dynamicpath import DynamicPath
        from hd3dlib.production.constituent import Constituent
        from hd3dlib.production.category import Category
        
        
        dynamicPathsDict = dict()
        dynamicPathsList = DynamicPath.getByProjectAndType(projectId, 
                                                           dynamicPathType);
        for dynamicPath in dynamicPathsList:
            # task type == -1 for all task types.
            # id == -1 for all categories or sequences
            
            taskTypeId =dynamicPath.taskTypeId == None and -1 or dynamicPath.taskTypeId
            key = "%d_%s_%d" % (dynamicPath.workObjectId, 
                                           dynamicPath.boundEntityName, 
                                            taskTypeId)
            dynamicPathsDict[key] = dynamicPath
            
            
        categories=Category.getByProject(projectId)
        
        categoriesDict=dict()
        for category in categories:
            categoriesDict[category.id] = category
        
        key="%d_%s_%d" % (constituent.id,
                          Constituent._simpleclassname, 
                          taskType)
        
        if not dynamicPathsDict.has_key(key):
            
            key="%d_%s_-1" % (constituent.id, Constituent._simpleclassname)
            if not dynamicPathsDict.has_key(key):
                
                if categoriesDict.has_key(constituent.category):
                    parent = categoriesDict[constituent.category]
                    dynamicPath = cls.getDynamicPathParent(dynamicPathsDict, 
                                                         taskType,parent,
                                                         categoriesDict)
                else :
                    return None
            else :
                dynamicPath = dynamicPathsDict[key]
        else :
            dynamicPath = dynamicPathsDict[key]
            
        return dynamicPath
    
    
    @classmethod
    def getDynamicPathFromShot(cls, projectId, dynamicPathType, shot, taskType):
        '''
        Return the dynamic path of the given shot and the given task type.
        
        Arguments:
                *projectId* the project id
                *dynamicPathType* the type of dynamic path retrieved
                *shot* the shot
                *taskType* the task type
        '''  
        from hd3dlib.asset.dynamicpath import DynamicPath
        from hd3dlib.production.sequence import Sequence
        from hd3dlib.production.shot import Shot
        
        dynamicPathsDict = dict()
        dynamicPathsList = DynamicPath.getByProjectAndType(projectId, 
                                                           dynamicPathType);
        for dynamicPath in dynamicPathsList:
            #task type == -1 for all task types.
            #id == -1 for all categories or sequences
            
            taskTypeId = dynamicPath.taskTypeId ==None and -1 or dynamicPath.taskTypeId
            key = "%d_%s_%d" % (dynamicPath.workObjectId, 
                                           dynamicPath.boundEntityName, 
                                            taskTypeId)
            dynamicPathsDict[key] = dynamicPath
           
        
        sequences=Sequence.getByProject(projectId)
        
        sequencesDict=dict()
        for sequence in sequences:
            sequencesDict[sequence.id] = sequence   
         
        key="%d_%s_%d" % (shot.id, Shot._simpleclassname, taskType)
        
        if not dynamicPathsDict.has_key(key)  :
            #for all task types
            key="%d_%s_-1" % (shot.id, Shot._simpleclassname)
            if not dynamicPathsDict.has_key(key) :
                if sequencesDict.has_key(shot.sequence):
                    parent = sequencesDict[shot.sequence]
                    dynamicPath = cls.getDynamicPathParent(dynamicPathsDict,taskType, 
                                                         parent, 
                                                         sequencesDict)
                else :
                    return None
            else :
                dynamicPath = dynamicPathsDict[key]
        else :
            dynamicPath = dynamicPathsDict[key]
            
        return dynamicPath
        
        
    @classmethod 
    def getDynamicPathParent(cls, dynamicPathsDict, taskType, parent, parents):     
        '''
        Return the dynamic path of the given task type for a category of a sequence.
        
        Arguments:
                *dynamicPathsDict* the dict of dynamic paths
                *taskType* the given task type
                *parent* the category or the sequence
                *parents* the dict of categories or the dict of sequences
        '''  
        key="%d_%s_%d" % (parent.id, parent._simpleclassname, taskType)
        
        if not dynamicPathsDict.has_key(key)  :
            
            #for all task type
            key="%d_%s_-1" % (parent.id, parent._simpleclassname)
            if not dynamicPathsDict.has_key(key):
                
                if parents.has_key(parent.parent):
                    parent = parents[parent.parent]
                    dynamicPath = cls.getDynamicPathParent(taskType,
                                                           parent,
                                                           parents)
                else :
                    
                    #for all categories or sequences                    
                    key="-1_%s_%d" % (parent._simpleclassname, taskType)
                    if not dynamicPathsDict.has_key(key):
                        
                        #for all categories or sequences and all task types
                        key="-1_%s_-1" % (parent._simpleclassname)
                        if not dynamicPathsDict.has_key(key):
                            return None
                        else :
                            dynamicPath = dynamicPathsDict[key]
                    else :
                        dynamicPath = dynamicPathsDict[key]
            else :
                dynamicPath = dynamicPathsDict[key]    
        else :
            dynamicPath = dynamicPathsDict[key]
            
        return dynamicPath
    