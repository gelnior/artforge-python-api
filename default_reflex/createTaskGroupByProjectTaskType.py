# coding=utf-8


from hd3dlib import conf
from hd3dlib.reflex.log import Log
from hd3dlib.production import Project
from hd3dlib.task import Planning
from hd3dlib.task import TaskGroup
from hd3dlib.task import Task
from hd3dlib.task import ExtraLine
from hd3dlib.task import TaskType
from hd3dlib.reflex import evalArgDictionary
from hd3dlib.util import httprequestconstraints
from hd3dlib.util.httprequestconstraints import OrLogic 

from datetime import date
from datetime import timedelta


import unicodedata
import sys
import os



class TaskGroupInitializer():
    '''
    Create one task group for each task type for planning corresponding
    to planning ID given via arguments.
    '''

    
    def __init__(self):    
        '''
        Constructor : init logs and retrieve needed data from arguments (project
        id and planning id).
        '''    
        self.loadLogs()
        self.loadArgs()    
        
        
    def loadLogs(self):
        '''
        Setup logs for easy logging during script execution. Logs appear
        in file corresponding to current script in logs directory and in
        server logs. 
        '''
        import os
        from hd3dlib.reflex import Log
            
        self.log = None
        logLocation = conf.dataServerLogPath
        if not os.path.exists(logLocation):
            raise ValueError('Log directory does not exist.')
            
        self.log = Log(str(os.path.basename(__file__)), logLocation)
        self.log.enableConsole()
        self.log.start()    
        
        
    def loadArgs(self):
        '''
        Retrieve current project and current planings IDs from arguments.
        '''
        args = evalArgDictionary(sys.argv[1])
        if not isinstance(args, dict):
            raise ValueError('Malformed arguments dictionary.')
        self.log.write('Arguments are : ' + str(sys.argv))
        self.planningId = args['planningId']
        self.projectId = args['projectId']
            
    
    def doIt(self):
        '''
        Build a task group for each task type if it does not exist.
        '''
        self.log.write('*** Task group initialization is starting. ***')            
        #mainTaskGroup = self.getPlanningTaskGroup()
        taskTypes = self.getProjectTaskTypes()
        planning = Planning.query().branch('projects', self.projectId).getFirst()
        
        index = 1
        for taskType in taskTypes:
            taskGroup = self.getTaskTypeTaskGroup(taskType)
            
            task = Task.query().branch('projects', self.projectId).filter('taskType.id', taskType.id).getFirst()
            
            if not taskGroup and task:
                self.log.write('Task group creation for task type : %s' % taskType.name.encode('utf-8'))
                
                startDate = self.getTaskTypeStartDate(taskType)
                endDate = self.getTaskTypeEndDate(taskType)
                
                
                if not startDate or not endDate:
                    startDate = planning.startDate
                    endDate = planning.endDate
                    
                taskTypeColor = "#FFFFFF"
                if taskType.color:
                    taskTypeColor = taskType.color
                
                taskGroup = TaskGroup(
                    index=1,
                    color=taskTypeColor.encode('utf-8'),
                    taskType=taskType.id,
                    name=taskType.name.encode('utf-8'),
                    planning=self.planningId,
                    startDate=startDate.encode('utf-8'),
                    endDate=endDate.encode('utf-8'),
                )
                
                taskGroup._resetLocator(self.projectId)                
                taskGroup.create()
                
            elif taskGroup:
                self.log.write('Task group already exists for task type : %s' % taskType.name.encode('utf-8'))
                if taskGroup and (taskGroup.color != taskType.color or taskGroup.name != taskType.name) :
                    
                    taskTypeColor = "#FFFFFF"
                    if taskType.color:
                        taskTypeColor = taskType.color
                    taskGroup.color = taskTypeColor.encode('utf-8')
                    taskGroup.name = taskType.name.encode('utf-8')
                    taskGroup.update()
                    self.log.write('Task type %s attributes changed, so task group has been updated.' % taskType.name.encode('utf-8'))
                    
                if not task:
                    taskGroup.delete()
                    self.log.write('Task group %s is deleted because no corresponding task exists.' % taskType.name.encode('utf-8'))
            else:
                self.log.write('Task group %s is not created because no corresponding task exists.' % taskType.name.encode('utf-8'))
                
        self.log.write('*** Task group synchronization is finished. ***')     
            

    
    
    def getPlanningTaskGroup(self):
        '''
        Return planning main task group.
        '''
        taskGroupQuery = TaskGroup.query().branch('projects', self.projectId)
        taskGroupQuery = taskGroupQuery.branch('plannings', self.planningId)
        taskGroupQuery.filter_custom('isnull', 'taskGroup', None)
        taskGroup = taskGroupQuery.getFirst()
        
        return taskGroup
    
    
    def getTaskTypeTaskGroup(self, taskType):
        '''
        Return task group having *mainTaskGroup* as parent and *taskType* as 
        task type.
        
        Arguments:
            *mainTaskGroup* 
            *taskType*
        '''
        taskGroupQuery = TaskGroup.query().branch('projects', self.projectId)
        taskGroupQuery = taskGroupQuery.branch('plannings', self.planningId)
        #taskGroupQuery.filter('taskGroup.id', mainTaskGroup.id)
        taskGroupQuery.filter('taskType.id', taskType.id)
        taskGroup = taskGroupQuery.getFirst()
        
        return taskGroup
    
    
    def getProjectTaskTypes(self):
        '''
        Return project task types.
        '''
        taskTypes = list()
        taskTypesTmp = list()
        
#        constraints = list()
#        constraints.append(httprequestconstraints.Filter('eq', 'project.id', self.projectId))
#        constraints.append(httprequestconstraints.Filter('isnull', 'project.id', None))        
#        filter = OrLogic(constraints)
#        
#        taskTypes = TaskType.query().filter_built(filter).getAll()
        
        taskTypes = TaskType.query().order('name').getAll()
#        for taskType in taskTypeTmp:
#            task =Task.query().filter('taskType.id', taskType.id).getFirst()
#            if task:
#                taskTypes
        
        return taskTypes
    

    def getTaskTypeStartDate(self, taskType):
        '''
        Return first start date of planned task for given task type.
        '''    
        query = Task.query().branch('projects', self.projectId)
        query = query.filter_custom('isnotnull', 'startDate', None)
        query = query.filter('taskType.id', taskType.id).order('startDate')
        task = query.getFirst()

        if task and task.startDate:
            return task.startDate
        else:
            return None



    def getTaskTypeEndDate(self, taskType):
        '''
        Return last start date of planned task for given task type.
        '''    
        query = Task.query().branch('projects', self.projectId)
        query = query.filter_custom('isnotnull', 'endDate', None)
        query = query.filter('taskType.id', taskType.id).order('-endDate')
        task = query.getFirst()
                
        if task and task.endDate:
            return task.endDate
        else:
            return None


if __name__ == '__main__':           
    
    initializer = TaskGroupInitializer()
    initializer.doIt()
    
    