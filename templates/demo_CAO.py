# coding=utf-8

import os, sys
from hd3dlib.reflex import evalArgDictionary
from hd3dlib.team import Person, ResourceGroup
from hd3dlib.production import Category, Constituent, Sequence, Shot
from hd3dlib.production import Project, ProjectType,Breakdown,Step, ApprovalNoteType
from hd3dlib.task import Task, TaskType, EntityTaskLink
from hd3dlib.admin import Role
from hd3dlib.planning import Planning
from hd3dlib.extra import Sheet,Item,ItemGroup
from hd3dlib.asset import FileRevision
from hd3dlib.util.odspreadsheet import ODSpreadsheet


import time
from datetime import timedelta
from datetime import date


global project
global groups
global shots
global constituents


global odsServerPath
global projectName

# type constants
ATTRIBUTE = 'attribute'
METHOD = 'method'
COLLECTIONQUERY = 'collectionQuery'

#collection query
STEP_QUERY ='fr.hd3d.services.resources.collectionquery.StepsByTaskTypeCollectionQuery'
APPROVALNOTE_QUERY ='fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery'
TASK_APPROVALNOTE_QUERY ='fr.hd3d.services.resources.collectionquery.TaskBoundEntityApprovalNotesCollectionQuery'
WORKOBJECT_QUERY='fr.hd3d.services.resources.collectionquery.TaskBoundEntityCollectionQuery'    

# metaType constants
JAVA_LANG_STRING = 'java.lang.String'
JAVA_LANG_BOOLEAN = 'java.lang.Boolean'
JAVA_LANG_LONG = 'java.lang.Long'
JAVA_LANG_DATE = 'java.util.Date'
JAVA_LANG_INTEGER = 'java.lang.Integer' 
APPROVAL_METATYPE = 'ApprovalNote'
JAVA_UTILS_LIST="java.utils.List"

# renderer constants
SHORT_TEXT = 'short-text'
BOOLEAN = 'boolean'
DATE = 'date'
STARTENDDATE = 'startEndDate'
SEQUENCE = 'Sequence'
INT = 'int'
STEP = 'step'
APPROVALNOTE = 'approval'


def initProject():
    global project
      
    typeEtude = ProjectType.getOrCreateByName("Etude")
    typeEtude.color="#00FF00"
    typeEtude.update()
    
    typeAvantProjet = ProjectType.getOrCreateByName("Avant-projet")
    typeAvantProjet.color="#FF0000"
    typeAvantProjet.update()
    
    typeConception = ProjectType.getOrCreateByName("Conception")
    typeConception.color="#0000FF"
    typeConception.update()
    
    if projectName :
        project = Project.getOrCreateByName(projectName)

    project.status = "OPEN"
    project.projectTypeId = typeAvantProjet.id
    project.update()
  

def initTeams():
    initGroups()
    initPersons()
    initRoles()
      
    
def initGroups():
    
    #global mainGroup
    global groups
    groups = dict()
    
    #mainGroup = ResourceGroup.getOrCreateByName("Project")
    
    groups['Client'] = ResourceGroup.getOrCreateByName("Client")
    groups['Commercial'] = ResourceGroup.getOrCreateByName("Commercial")
    groups['Bureau Etudes'] = ResourceGroup.getOrCreateByName("Bureau Etudes")
    groups['Design'] = ResourceGroup.getOrCreateByName("Design",groups['Bureau Etudes'].id)
    groups['Modeling'] = ResourceGroup.getOrCreateByName("Modeling",groups['Bureau Etudes'].id)
    groups['Calcul'] = ResourceGroup.getOrCreateByName("Calcul",groups['Bureau Etudes'].id)
    
    groups['Fabrication'] = ResourceGroup.getOrCreateByName("Fabrication")
    groups['Achats'] = ResourceGroup.getOrCreateByName("Achats")
    groups['Sous-traitants'] = ResourceGroup.getOrCreateByName("Sous-traitants")
    
    if not project.resourceGroupIDs:
        project.resourceGroupIDs = list()
        
    project.resourceGroupIDs.append(groups['Client'].id)
    project.resourceGroupIDs.append(groups['Design'].id)
    project.resourceGroupIDs.append(groups['Modeling'].id)
    project.resourceGroupIDs.append(groups['Calcul'].id)
    #project.resourceGroupIDs.append(groups['Bureau Etudes'].id)  
    project.resourceGroupIDs.append(groups['Fabrication'].id)
    project.resourceGroupIDs.append(groups['Achats'].id)
    project.resourceGroupIDs.append(groups['Sous-traitants'].id)
    project.update()
    

def initPersons():
    
    for i in range(3):
        login = 'client%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Client'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Client'].id)
        person.update()

    groups["Client"] = ResourceGroup.getOrCreateByName("Client")
    
    for i in range(3):
        login = 'commercial%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Commercial'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Commercial'].id)
        person.update()
        
    groups['Commercial'] = ResourceGroup.getOrCreateByName("Commercial")

    for i in range(3):
        login = 'design%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Design'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Design'].id)
        person.update()
        
    groups['Design'] = ResourceGroup.getOrCreateByName("Design")
    
    for i in range(3):
        login = 'modeling%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Modeling'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Modeling'].id)
        person.update()
        
    groups['Modeling'] = ResourceGroup.getOrCreateByName("Modeling")
    
    for i in range(3):
        login = 'calcul%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Calcul'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Calcul'].id)
        person.update()
        
    groups['Calcul'] = ResourceGroup.getOrCreateByName("Calcul")
    
    for i in range(3):
        login = 'fabrication%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Fabrication'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Fabrication'].id)
        person.update()
        
    groups['Fabrication'] = ResourceGroup.getOrCreateByName("Fabrication")
    
    for i in range(3):
        login = 'achats%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Achats'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Achats'].id)
        person.update()
        
    groups['Achats'] = ResourceGroup.getOrCreateByName("Achats")
    
    for i in range(3):
        login = 'soustraitants%d' % (i + 1) 
        firstName = 'mr %d' % (i + 1)
        lastName = 'Sous-traitants'

        person = Person.getOrCreateByLogin(firstName, lastName, login)
        person.createAccount(login)
        
        if not person.resourceGroupIDs:
            person.resourceGroupIDs = list()
            
        person.resourceGroupIDs.append(groups['Sous-traitants'].id)
        person.update()
        
    groups['Sous-traitants'] = ResourceGroup.getOrCreateByName("Sous-traitants")
                        
            

def initRoles():
    role = Role.getOrCreateByName("admin")
     
    if not role.permissions :
        role.permissions=list()
        role.permissions.append("*:*")
     
    if not role.resourceGroupIDs :
        role.resourceGroupIDs=list()
    
    role.resourceGroupIDs.append(groups['Client'].id)
    role.resourceGroupIDs.append(groups['Bureau Etudes'].id)
    role.resourceGroupIDs.append(groups['Design'].id)
    role.resourceGroupIDs.append(groups['Modeling'].id)
    role.resourceGroupIDs.append(groups['Calcul'].id)
    role.resourceGroupIDs.append(groups['Fabrication'].id)
    role.resourceGroupIDs.append(groups['Achats'].id)
    role.resourceGroupIDs.append(groups['Sous-traitants'].id)
    
    role.update()
     
        

def initBreakdown(odsServerPath):
    global constituents
    global shots
    shots =list()
    constituents=list()
    if odsServerPath != "":
        try:
            odsServerPath = os.path.basename(odsServerPath)
            f = FileRevision().query().filter('key', odsServerPath).getLast()
            odsServerPath = os.path.join(f.location , f.relativePath)  
            b = Breakdown()
            b.setODS(odsServerPath)
            b.setProjectDBId(project.id)
            b.processCategories()
            b.processConstituents()
            b.processSequences()
            b.processShots()
            b.createInDatabase()
            
           
            dictSequence=b.getEntityRecords('Sequence')
            dictShots=b.getEntityRecords('Shots');
            for shot in dictShots.values():
                sequence=Sequence.getByName(project, shot["sequence.name"]);
                shot=Shot.getOrCreateByName(project, sequence, shot["shot.label"])
                shots.append(shot)
                
            dictCategories=b.getEntityRecords('Categories')
            dictConstituents=b.getEntityRecords('Constituents');
            for constituent in dictConstituents.values():
                category=Category.getByName(project, constituent["category.name"]);
                constituent=Constituent.getOrCreateByName(project, category, constituent["constituent.label"])
                constituents.append(constituent)
        except Exception :
            print sys.exc_info()
            print "[Warning] errors  occurred, while reading the ods file %s. %s " % (odsServerPath,sys.exc_info()[1])
         
        
    initTaskType()
    initStep()
    initSheetBreakdown(project.id)
    
def initSheetBreakdown(projectId):

        Sheet._locator = 'sheets/'
        
        # Create sheet for breakdown
        constituentSheetBreak = Sheet.getOrCreate('Parts Management', 
                  'Parts Management',
                  projectId,
                  'Constituent',
                  'BREAKDOWN')
        print 'New sheet created : %s' % constituentSheetBreak.name
        
     
        
        # Create main group and items
        igDefault=ItemGroup.getOrCreate('Default',constituentSheetBreak.id)
        
        itLabel         = Item.getOrCreate(
                               'Label', #name
                               igDefault,#itemGroup
                               'label', #query
                               ATTRIBUTE, #type
                              JAVA_LANG_STRING, #metaType
                               '', #renderer
                               SHORT_TEXT, #editor
                               "" #transformerParameters
                                )
        
        itLabel         = Item.getOrCreate(
                               'Thumbnail', #name
                               igDefault,#itemGroup
                               'thumbnail', #query
                               METHOD, #type
                              JAVA_LANG_STRING, #metaType
                               'thumbnail', #renderer
                               '', #editor
                               "" #transformerParameters
                                )

        itDescription     = Item.getOrCreate(
                               'Description',
                               igDefault, 
                               'description', 
                               ATTRIBUTE,
                               JAVA_LANG_STRING,
                               '',SHORT_TEXT,"")
        
        igStep=ItemGroup.getOrCreate("Steps",constituentSheetBreak.id)
        
        taskType = taskTypes["Exigences client"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Reuse"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        
        taskType = taskTypes["Sub-contracting"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Design"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Modeling"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Analysis"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Costing"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
              
              
        print 'Sheet creation finished : %s' % constituentSheetBreak.name
        
        # Create sheet for breakdown
        shotSheetBreak = Sheet.getOrCreate('Assembly Management', 
                  'Assembly Management',
                  projectId,
                  'Shot',
                  'BREAKDOWN')
        print 'New sheet created : %s' % shotSheetBreak.name
        
     
        
        # Create main group and items
        igDefault=ItemGroup.getOrCreate('Default',shotSheetBreak.id)
        
        itLabel         = Item.getOrCreate(
                               'Label', #name
                               igDefault,#itemGroup
                               'label', #query
                               ATTRIBUTE, #type
                              JAVA_LANG_STRING, #metaType
                               '', #renderer
                               SHORT_TEXT, #editor
                               "" #transformerParameters
                                )
        
        itLabel         = Item.getOrCreate(
                               'Thumbnail', #name
                               igDefault,#itemGroup
                               'thumbnail', #query
                               METHOD, #type
                              JAVA_LANG_STRING, #metaType
                               'thumbnail', #renderer
                               '', #editor
                               "" #transformerParameters
                                )
        
        itDescription     = Item.getOrCreate(
                               'Description',
                               igDefault, 
                               'description', 
                               ATTRIBUTE,
                               JAVA_LANG_STRING,
                               '',SHORT_TEXT,"")
        
        igStep=ItemGroup.getOrCreate("Steps",shotSheetBreak.id)
        
        taskType = taskTypes["Exigences client"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Assembly"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Elec"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Kinematics"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
            
        taskType = taskTypes["Analysis"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
        
        taskType = taskTypes["Costing"]
        itStep     = Item.getOrCreate(
                               taskType.name,
                               igStep, 
                               STEP_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_LONG,
                               STEP,STEP,str(taskType.id))
                
        print 'Sheet creation finished : %s' % shotSheetBreak.name
        
    
def initTaskType():
    global taskTypes
    global approvalNoteTypes
    taskTypes=dict()
    approvalNoteTypes=dict()
    
    taskTypeName="Exigences client"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#2222FF");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Design"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#22FF22");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Reuse"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#55FF55");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Modeling"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#FF2222");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Tolerie"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#556677");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Elec"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#44AA77");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Analysis"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#ABCDEF");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Assembly"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#FEDCBA");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Kinematics"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#77AA44");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Costing"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#9955CC");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    taskTypeName="Sub-contracting"
    tasktype = TaskType.getOrCreateByName(taskTypeName,"description of task type %s" % taskTypeName,"#9900FF");
    taskTypes[taskTypeName]=tasktype;
    approvalNoteType=ApprovalNoteType.getOrCreate("%s Approval"%taskTypeName,project.id,tasktype.id)
    approvalNoteTypes["%s Approval"%taskTypeName]=approvalNoteType
    
    
    approvalNoteType=ApprovalNoteType.getOrCreate("Technical Approval",project.id)
    approvalNoteTypes["Technical Approval"]=approvalNoteType
    
    approvalNoteType=ApprovalNoteType.getOrCreate("Manufacturing Approval",project.id)
    approvalNoteTypes["Manufacturing Approval"]=approvalNoteType
    
    approvalNoteType=ApprovalNoteType.getOrCreate("Quality Approval",project.id)
    approvalNoteTypes["Quality Approval"]=approvalNoteType
    
    approvalNoteType=ApprovalNoteType.getOrCreate("Customer Approval",project.id)
    approvalNoteTypes["Customer Approval"]=approvalNoteType
    
def initStep(): 
    taskType =taskTypes["Modeling"]
    
    for constituent in constituents:
        step = Step.getOrCreate(taskType,constituent,"Constituent",2*28800)
    
def initProduction():
    initSheetProduction()
    
def initSheetProduction():
      # Create sheet for production
        constituentSheetProd = Sheet.getOrCreate('Part Design', 
                  'Part Design',
                  project.id,
                  'Constituent',
                  'PRODUCTION')
        print 'New sheet created : %s' % constituentSheetProd.name
        
         # Create main group and items
        igDefault=ItemGroup.getOrCreate('Default',constituentSheetProd.id)
        
        itLabel         = Item.getOrCreate(
                               'Label', #name
                               igDefault,#itemGroup
                               'label', #query
                               ATTRIBUTE, #type
                              JAVA_LANG_STRING, #metaType
                               '', #renderer
                               SHORT_TEXT, #editor
                               "" #transformerParameters
                                )
        
        itLabel         = Item.getOrCreate(
                               'Thumbnail', #name
                               igDefault,#itemGroup
                               'thumbnail', #query
                               METHOD, #type
                              JAVA_LANG_STRING, #metaType
                               'thumbnail', #renderer
                               '', #editor
                               "" #transformerParameters
                                )

        itDescription     = Item.getOrCreate(
                               'Description',
                               igDefault, 
                               'description', 
                               ATTRIBUTE,
                               JAVA_LANG_STRING,
                               '',SHORT_TEXT,"")
        
        igApproval=ItemGroup.getOrCreate("Internal",constituentSheetProd.id)
        
        approvalNoteType = approvalNoteTypes["Exigences client Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Reuse Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY,
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Sub-contracting Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Design Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Modeling Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Analysis Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Costing Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        igApproval=ItemGroup.getOrCreate("Approvals",constituentSheetProd.id)
        
        approvalNoteType = approvalNoteTypes["Technical Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Manufacturing Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Quality Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               JAVA_LANG_BOOLEAN,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
              
        print 'Sheet creation finished : %s' % constituentSheetProd.name
        
        # Create sheet for breakdown
        shotSheetProd = Sheet.getOrCreate('Product Lifecycle', 
                  'Product Lifecycle',
                  project.id,
                  'Shot',
                  'PRODUCTION')
        print 'New sheet created : %s' % shotSheetProd.name
        
     
        
        # Create main group and items
        igDefault=ItemGroup.getOrCreate('Default',shotSheetProd.id)
        
        itLabel         = Item.getOrCreate(
                               'Label', #name
                               igDefault,#itemGroup
                               'label', #query
                               ATTRIBUTE, #type
                              JAVA_LANG_STRING, #metaType
                               '', #renderer
                               SHORT_TEXT, #editor
                               "" #transformerParameters
                                )
        
        itLabel         = Item.getOrCreate(
                               'Thumbnail', #name
                               igDefault,#itemGroup
                               'thumbnail', #query
                               METHOD, #type
                              JAVA_LANG_STRING, #metaType
                               'thumbnail', #renderer
                               '', #editor
                               "" #transformerParameters
                                )

        itDescription     = Item.getOrCreate(
                               'Description',
                               igDefault, 
                               'description', 
                               ATTRIBUTE,
                               JAVA_LANG_STRING,
                               '',SHORT_TEXT,"")
        
        igApproval=ItemGroup.getOrCreate("Internal",shotSheetProd.id)
        
        approvalNoteType = approvalNoteTypes["Exigences client Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Assembly Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Elec Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Kinematics Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
            
        approvalNoteType = approvalNoteTypes["Analysis Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Costing Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        igApproval=ItemGroup.getOrCreate("Approvals",shotSheetProd.id)
        
        approvalNoteType = approvalNoteTypes["Technical Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Manufacturing Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        approvalNoteType = approvalNoteTypes["Quality Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
        igApproval=ItemGroup.getOrCreate("Customer",shotSheetProd.id)
        
        approvalNoteType = approvalNoteTypes["Customer Approval"]
        itApproval     = Item.getOrCreate(
                               approvalNoteType.name,
                               igApproval, 
                               APPROVALNOTE_QUERY, 
                               COLLECTIONQUERY,
                               APPROVAL_METATYPE,
                               APPROVALNOTE,APPROVALNOTE,str(approvalNoteType.id))
        
                
        print 'Sheet creation finished : %s' % shotSheetProd.name
    
def initTaskManager():
    initSheetTaskManager()
    initTasks()

def initSheetTaskManager():
    Sheet._locator = 'sheets/'
    
    #create sheet
    sheet = Sheet.getOrCreate("Tasks", 
              'Standard task sheet',
              project.id,
              'Task','TASK')
    
    print 'New sheet created : %s' % sheet.name
    
    #create main group and items
    igDefault=ItemGroup.getOrCreate('Default',sheet.id)
    
    
    item         = Item.getOrCreate('Work Object', 
                            igDefault,
                           WORKOBJECT_QUERY, 
                           COLLECTIONQUERY, 
                           'IBase',
                           'work-object','',"")
    
    
    item         = Item.getOrCreate('Task Type',
                            igDefault,
                           'taskType', 
                           ATTRIBUTE, 
                           JAVA_LANG_STRING,
                           'task-type','',
                           "")
    
    item         = Item.getOrCreate('Status Notes', igDefault,
                           TASK_APPROVALNOTE_QUERY, 
                           COLLECTIONQUERY, 
                           JAVA_UTILS_LIST,
                           'approval','approval',""
                           )
    
    item         = Item.getOrCreate('Status', igDefault,
                           'status', 
                           ATTRIBUTE, 
                           'ETaskStatus',
                           'task-status','',""
                           )
    
    item         = Item.getOrCreate('Worker', 
                           igDefault,
                           'worker', 
                           ATTRIBUTE, 
                           'Person',
                           'person-task','',"")
    
    item         = Item.getOrCreate('Duration',
                           igDefault, 
                           'duration', 
                           ATTRIBUTE, 
                           JAVA_LANG_LONG,
                           'duration','',"")
    
    item         = Item.getOrCreate('Start date',
                            igDefault,   
                            'startDate', 
                            ATTRIBUTE, 
                            JAVA_LANG_DATE,
                            'date-and-day',
                            DATE,
                           "")
    
    
    item         = Item.getOrCreate('End date',
                            igDefault, 
                            'endDate', 
                            ATTRIBUTE, 
                            JAVA_LANG_DATE,
                            'date-and-day',
                            DATE,
                           "")
    
    print 'Sheet creation finished : %s' % sheet.name
    
def initTasks():    
    modeling = taskTypes["Modeling"]
    taskDate = date.today()
    
    i=0
    for constituent in constituents:
        tasks=Task.getTaskByWOTasktype(constituent, 'Constituent', modeling)
        for task in tasks:
            task.startDate = taskDate.strftime('%Y-%m-%d %H:%M:%S')
            duration = timedelta(days=task.duration/28800)#28800 = nb temps de travail par jour en secondes
            taskDate=(taskDate + duration)
            task.endDate = taskDate.strftime('%Y-%m-%d %H:%M:%S')
            taskDate=taskDate+timedelta(days=1)
            size=groups["Artist"].resourceIDs.__sizeof__()
            task.worker =groups["Artist"].resourceIDs.pop()
            groups["Artist"].resourceIDs.insert(0,task.worker)
            task.update()
            
        i=i+1
            
def initPlanning():
    
    planning= Planning.getOrCreateByProject(project.id)
    today = date.today()
    duration = timedelta(days=2)
    planning.startDate=(today-duration).strftime('%Y-%m-%d %H:%M:%S')
    duration = timedelta(days=30)
    planning.endDate=(today+duration).strftime('%Y-%m-%d %H:%M:%S')
    planning.master=True
    planning.name=project.name.encode()
    
    substitute=dict()
    substitute["projects"]=project.id
    Planning._locator=Planning._locatorTmpl.safe_substitute(substitute)
    
    planning.update()
    
    
def doIt():
    initProject()
    initTeams()
    initBreakdown(odsServerPath)
    initTaskManager()
    initProduction()
    #initPlanning() //nouvelle version initialisation du planning automatique
    
    
if __name__ == '__main__':
   
    import json
    odsServerPath=""
    projectName="test"
    args=dict()
    
    
    if len(sys.argv) > 1 :
        args=evalArgDictionary(sys.argv[1])
        

    if args.has_key("odsFile"):
        odsServerPath=args["odsFile"]
   
    if args.has_key("projectName"):
        projectName=args["projectName"]                         
    
    
     
    print '[INFO] CAO Demo init starts.'
    doIt()
    print '[INFO] CAO Demo init finished.'
    