# coding=utf-8

import sys
import urllib
import warnings

from util.classbuild import Attribute, makeClass
from string import Template
from copy import copy, deepcopy
from hd3dlib.util import wsRequestHandler, httprequestconstraints, generateRandomId

try:
    import simplejson as json
except ImportError:
    import json


BASE_DEFINITION = {'name':'BaseObjectGenerated',
                   'attributes':{'id':Attribute('id', long, mutable=False),
                                 '_class':Attribute('class', str),
                                 'dynMetaDataValues':Attribute('dynMetaDataValues', list),
                                 'entityTaskLinks':Attribute('entityTaskLinks', list),
                                 'tags':Attribute('tags', list),
                                 'approvalNotes':Attribute('approvalNotes', list),
                                 'defaultPath':Attribute('defaultPath', str, mutable=False)
                                 }
                   }

class BaseObject(makeClass(BASE_DEFINITION, object)):
    '''            
    Base object provides base functions to any Artforge API object for easy
    services querying, such as : creation, update, deletion, list retrieving...
    
    Every HD3D classes has a locator field (i.e. URL) to connect it to the right 
    REST service. Baseobject stores it already and has also some tools to 
    generate the locator depending on some criteria like the project it depends 
    on.
    '''
        
    @classmethod
    def get(cls, id):
        '''
        Retrieve from services the object whom ID is equal to *id*.
        
        arguments:
        id
            ID of the object to retrieve.
        '''
        r = cls()
        r.read(long(id))
        return r
    
    @classmethod
    def hasAttribute(cls, attributeName):
        '''
        This method is deprecated and should not be used anymore.
        '''
        warnings.warn('hasAttribute deprecated. Please use hasattr instead.')
        #TODO : There must be some better ways to implement this test ...

        res = True
        try:
            dummyValue = eval('inst.' + attributeName)
        except Exception:
            res = False
        finally:
            return res
    
    @classmethod
    def query(cls):
        '''
        Return query object for list retrieving or retrieving based on other
        field than ID.        
        '''
        return Query(cls)
    
    _locatorTmpl = Template('')
    _locatorShort = ''
    _locator = ''


    @classmethod
    def queryClass(cls, queriedClass):
        '''
        Return query object for list retrieving or retrieving based on other
        field than ID.        
        '''
        return Query(queriedClass)
    

    def __init__(self, **params):
        '''
        BaseObject initializer
        '''
        self._isDirty = False
        self._locatorTmpl = deepcopy(self._locatorTmpl)
        self._locatorShort = str(self._locatorShort)
        self._locator = str(self._locator)
        self._jsonRepresentation = deepcopy(self._jsonRepresentation)
        
        self._jicId = str(generateRandomId(self))

        for (key, value) in params.items():
            if hasattr(self, key):
                setattr(self, key, value)
        #self._resetLocator()
        
                
    def _resetLocator(self):
        '''
        Reset locator with a new value created from locator template.
        '''
        self._locator = self._locatorTmpl.safe_substitute({})
            
    def _resetAttributeValues(self, data):
        '''
        ??
        '''
        self._jsonRepresentation = data
        self._resetLocator()
    
    def create(self):
        '''
        Save object to services and retrieve its new ID if creation succeeds. 
        '''        
        if self.id is not None and self.id > 0:
            raise RuntimeError, 'Unable to create object : seems already created. (id=%d)' % self.id
        if self._class is None or self._class == '':
            raise ValueError, 'Unable to persist object : datamodel class is not set.'
        entry = self._jsonRepresentation
             
        jsonStr = json.dumps(entry, ensure_ascii=False)
        id = wsRequestHandler.post(self._locator, jsonStr.encode('utf-8'), returnsOnlyId=True)
        
        
        if id is not None:
            self._jsonRepresentation['id'] = long(id)
        else:
            sys.stderr.write('[WARN] It seems that object creation (type : %s) failed (no ID returned). \n' % self._class)
        return self
            
    def read(self, id):
        '''
        Retrieve object from URL composed of locatorShort + object id.
        '''
        url = self._locatorShort + str(id)
                
        data = wsRequestHandler.get(url)
        result = {}
        
        if data:
            if 'records' in data:
                result = data['records'][0]
                self._resetAttributeValues(result)
            else:
                return None
        return self
    
    def update(self):
        '''
        Update object data on services side with local data. It works only if an 
        ID is set.     
        '''
        if self.id is None:
            raise RuntimeError, 'Unable to update a non-created object.'
        entry = self._jsonRepresentation
        
        jsonStr = json.dumps(entry, ensure_ascii=False)       
        if isinstance(jsonStr, str):
            jsonStr = jsonStr.decode("utf-8") 
            
        wsRequestHandler.put(self._locator + str(self.id), jsonStr.encode("utf-8"))
        return self
    
    def delete(self):
        '''
        Delete object instance on services side.    
        '''
        wsRequestHandler.delete(self._locator + str(self.id))
        return None


    def getDynValue(self, key):     
        '''
        Return dynamic value of which name corresponds to key. If key does not 
        exist, it returns None.
        
        *NB : Dynamic values should have been retrieved before.*
        
        Arguments:
            key The key of the value to return.
        '''   
        if self.dynMetaDataValues is not None:
            for dynobj in self.dynMetaDataValues:
                if key == dynobj['classDynMetaDataTypeName']:
                    return dynobj.get('value', None)
        return None

    def getDynValueWithId(self, key):      
        '''
        Return dynamic value of which id  corresponds to key. If key does not 
        exists, it returns None.  
        *NB : Dynamic values should have been retrieved before.*
        
        Arguments:
            key Id of corresponding to dyn meta data value.
        '''
        if self.dynMetaDataValues is not None:
            for dynobj in self.dynMetaDataValues:
                if key == dynobj['classDynMetaDataType']:
                    return dynobj['value']
        return None
    
    def setDynValue(self, key, value):            
        '''
        Set a dynamic value of which name corresponds to key. Previous dynamic 
        value should have been retrieved before (even if this value was null).
        isChanged flag is set to true.
     
        For updating directly a dynamic value on server use 
        DynUtils.updateValue.
        
        Arguments:
            key The key corresponding to *value*.
            value The value to set.
        '''
        if self.dynMetaDataValues is not None:
            for dynobj in self.dynMetaDataValues:
                if key == dynobj['classDynMetaDataTypeName']:
                    dynobj['value'] = str(value)
                    dynobj['isChanged'] = True    
                    return dynobj                
        return None
    
    def setDynValueWithId(self, key, value):      
        '''
        Set a dynamic value of which id corresponds to key. Previous dynamic 
        value should have been retrieved before (even if this value was null).
        isChanged flag is set to true.
         
        To directly update a dynamic value on server use DynUtils.updateValue.
        
        Arguments:
            key The id corresponding to *value*.
            value The value to set.
        '''      
        if self.dynMetaDataValues is not None:
            for dynobj in self.dynMetaDataValues:
                if key == dynobj['classDynMetaDataType']:
                    dynobj['value'] = value
                    dynobj['isChanged'] = True
                    return dynobj                    
        return None
    
    def updateDynValues(self):
        '''
        Save changed dynamic values to server. 
        For each saved values, change flag is set to false after saving.
        '''
        if self.dynMetaDataValues is not None:
            for dynobj in self.dynMetaDataValues:
                if dynobj.get('isChanged', False) == True:                    
                    tmp = dict()
                    tmp['id'] = dynobj['id']
                    tmp['value'] = dynobj['value']       
                    tmp['class'] = 'fr.hd3d.model.lightweight.impl.LDynMetaDataValue' 
                    tmp['classDynMetaDataType'] = dynobj['classDynMetaDataType']   
    
                    jsonStr = json.dumps(tmp, ensure_ascii=False)
                    wsRequestHandler.put('/dyn-metadata-values/' + str(dynobj['id']), jsonStr)
                    dynobj['isChanged'] = False
        
        
    def copy(self):        
        '''
        Create another instance of object with exactly the same data.
        '''
        res = copy(self)
        res._jsonRepresentation = deepcopy(self._jsonRepresentation)
        if 'id' in res._jsonRepresentation:
            del res._jsonRepresentation['id']
        return res
        
        
    def show(self):        
        '''
        Create another instance of object with exactly the same data.
        '''
        warnings.warn(DeprecationWarning('Use a print command instead'))
        print 'Class : ' + str(self)
        for key in self._fields_:
            print '\t', key, ':', str(getattr(self, key))
        print '\n'
            
            
    def __repr__(self):        
        '''
        Return a string containing all fields and their value.
        '''
        repr = 'Class : ' + str(object.__repr__(self))
        for key in self._fields_:
            repr += '\n\t' + str(key) + ':' + str(getattr(self, key))
        return repr
    
    
    def getJsonRepresentation(self):
        '''
        Return JSON representation of the object, fit for being sent to 
        services.
        '''
        return self._jsonRepresentation

    
    
class Query(object):
    insertpattern = '[++]'
    leafpattern = None
    
    
    def __init__(self, queriedClass):
        '''
        Constructor: Build URL to query and initialize constraint list 
        (as empty).
        
        *queriedClass* The class related to query (ex: task query,         
        '''
        self.substitutes = {}
        self.constraint = httprequestconstraints.Constraint()
        self.queriedClass = queriedClass
        self.locTemplate = self.insertpattern + queriedClass._locatorTmpl.template
               
                
    def filter(self, column, value):
        '''
        Add an equal filter to query.
        
        Arguments: 
            *column* The column on which filter apply.
            *value* The value used by equal filter.
        '''
        return self.filter_eq(column, value)
    
    
    def filter_eq(self, column, value):
        '''
        Add an equal filter to query.
        
        Arguments: 
            *column* The column on which filter apply.
            *value* The value used by equal filter.
        '''
        self.constraint.append(httprequestconstraints.Filter('eq', column, value))
        return self
    
    
    def filter_custom(self, type, column, value):
        '''
        Add an custom filter to query.
        
        Filters available:
            
           * eq (equals)
           * neq (not equals)
           * lt (less than)
           * gt (greater than)
           * leq (less or equal)
           * get (greater or equal)
           * in (matches any value in the list passed)
           * notin (matches no element in the list passed)
           * like (implements the SQL `LIKE` string comparison)
           * isnull (is null)
           * isnotnull (is not null)
           * btw (between the `start` and the `end` values from the dict passed)
        
        Arguments : 
            *column* The column on which filter apply.
            *value* The value used by equal filter.
        '''
        self.constraint.append(httprequestconstraints.Filter(type, column, value))
        return self


    def filter_built(self, filter):
        self.constraint.append(filter)
        return self
    
    
    def branch(self, parentResource, parentId):
        '''
        Branch helps you to build url on query. Most of non-root URL depends
        on parameters. (like project id). Query URL templates marked this 
        parameters by putting $ in front of them. When you call branch method,
        it will replace parameter*parentResource* by *parentId*.
        
        Ex: query has template: projects/$projects/categories/. We want 
        all categories for project of which ID is 5. So we call branch method
        to build the right query: 
        >>> query.branch('projects', 5)
        It will set as URL: projects/5/categories/.  
        
        Arguments: 
            *parentResource* The parameter to change.
            *parentId* The id to set as new value.
        '''
        self.substitutes[parentResource.replace('-', '')] = parentId
        moreTemplate = '/' + str(parentResource) + '/$' + str(parentResource.replace('-', '')) 
        if '/' + parentResource + '/' not in self.locTemplate:
            self.locTemplate = self.locTemplate.replace(self.insertpattern, moreTemplate + self.insertpattern)
        else:
            self.locTemplate = self.locTemplate.replace(self.insertpattern, '')
            self.locTemplate = self.locTemplate.replace(moreTemplate, moreTemplate + self.insertpattern)
        return self
        
    
    def leaf(self, leafResource, selfId=None):
        '''
        Add a leaf to requested url.
        
        Ex: If query has http://server/services/categories/ URL set. Calling 
        leaf method with 'children' as arguments will change the URL to:
        http://server/services/categories/ 
        
        
        Arguments: 
            *leafResource* The segment to add at the end of the URL.
            *selfId* An optional id to add as segment before leaf. 
        '''
        self.leafpattern = (str(selfId) + '/') if selfId is not None else ''
        self.leafpattern += str(leafResource)
        return self
    
    
    def paginate(self, first, count):
        '''
        Add a pagination parameter to the request. It is needed to retrieve only
        a small part of big lists. List returned will contain  *count* records. 
        *first* corresponds to the index of first element returned..  
        
        Arguments:
            *first* Index of first element returned.
            *count* Number of elements returned.
        '''
        self.pagination = httprequestconstraints.Pagination(first, count)
        return self
        
        
    def order(self, *fields):
        '''
        Add an order by parameter to the request. Sorting will be done on field list given
        in parameter.
        
        Arguments:
            *fields* Fields on wich sorting applies.
        '''
        self.ordering = httprequestconstraints.OrderBy(*fields)
        return self
    
    
    def dyn(self):
        '''
        Add dyn parameter to query. It is needed to retrieve dynamic attributes
        with static data.
        '''
        self.dyna = httprequestconstraints.Dyn()
        return self
    
    
    def _get(self):
        '''
        Private method that build request to send via a get method. It applies
        templates, branching, filters and parameters to query, then it processes
        it.
        '''
        strtmpl = self.locTemplate.replace(self.insertpattern, '')
        tmpl = Template(strtmpl)
        locator = tmpl.safe_substitute(self.substitutes)
        
        url = locator
        url += self.leafpattern if self.leafpattern is not None else ''
        queryString = list()
        if len(self.constraint) > 0:
            queryString.append(self.constraint.tojson())
        if hasattr(self, 'pagination'):
            queryString.append(self.pagination.tojson())
        if hasattr(self, 'ordering'):
            queryString.append(self.ordering.tojson())
        if hasattr(self, 'dyna'):
            queryString.append(self.dyna.tojson())
        if len(queryString) > 0:
            url += '?' + urllib.urlencode(queryString)
            
        dic = wsRequestHandler.get(url)
        if 'records' in dic and len(dic['records']) > 0 :
            data = dic['records']
            result = []
            for line in data:
                if line['class'] != self.queriedClass._jsonRepresentation['class']:
                    raise TypeError('Type mismatch between queried class and read json.\(URL: %s)' % (url))
                else:
                    obj = self.queriedClass()
                    obj._jsonRepresentation = line
                    obj._locatorTmpl.template = strtmpl
                    obj._locator = locator
                    result.append(obj)
            return result
        else:
            return []
    
    
    def getAll(self):        
        '''
        Perform a get request applying all branching called before. Data are returned
        as a list.
        '''
        return self._get()
    
    
    def getFirst(self):
        '''
        Perform a get request applying all branching called before and add it 
        a pagination constraint to retrieve only first element of request.
        '''
        self.paginate(0, 1)
        l = self._get()
        if len(l) > 0:
            return l[0]
        else:
            return None
        
        
    def getLast(self):
        '''
        Perform a get request applying all branching called before and add it 
        a pagination and an order constraint to retrieve only last element of 
        request (with the biggest ID).
        '''
        self.order('-id')
        self.paginate(0, 1)
        l = self._get()
        if len(l) > 0:
            return l[0]
        else:
            return None
        
    
    def postAll(self, objects):
        '''
        Method to perform a bulk post of objects. Bulk request is built from 
        object list.
        
        Arguments:
            *objects* Objects to post.
        '''
        
        # Build URL
        strtmpl = self.locTemplate.replace(self.insertpattern, '')
        tmpl = Template(strtmpl)
        url = tmpl.safe_substitute(self.substitutes)
        
        if len(url) == 0:
            url = self.queriedClass._locator
        
        # Build URL parameters
        queryString = list()        
        queryString.append(httprequestconstraints.Bulk().tojson())
        url += '?' + urllib.urlencode(queryString)
            
        # Build json        
        if len(objects) > 0:
            jsonObjects = list()        
            for object in objects:
                jsonObjects.append(object.getJsonRepresentation())
                        
            jsonStr = json.dumps(jsonObjects, ensure_ascii=False)
            ids = wsRequestHandler.post(url, jsonStr, False)
            
            #Get all ids         
            if ids is not None:
                urlLocators = ids[1:len(ids) - 1].split(',')
            else:             
                sys.stderr.write('[WARNING] It seems that an error occurred'
                    + ' while posting your data because no created instance id'
                    + ' has been returned. \n')
                return None    
            
            idsLong = list()
            for urlLocator in urlLocators:
                spliturl = urlLocator.split('/')
                idLong = long(spliturl[len(spliturl) - 1])
                idsLong.append(idLong) 
                    
            return idsLong
        
        return None
        
        
    
        
    def putAll(self, objects):
        '''
        Method to perform a bulk update (PUT) of objects. Bulk request is built from 
        object list.
        
        Arguments:
            *objects* Objects to modify.
        '''
        url = self._buildUrl()
        url = self._setParameterOnUrl(url)
        
        # Build json        
        if len(objects) > 0:
            jsonObjects = list()        
            for object in objects:
                jsonObjects.append(object.getJsonRepresentation())
            
            jsonStr = json.dumps(jsonObjects, ensure_ascii=False)
            
            wsRequestHandler.put(url, jsonStr)
            
        
    def _setParameterOnUrl(self, url):        
        queryString = list()        
        queryString.append(httprequestconstraints.Bulk().tojson())
        url += '?' + urllib.urlencode(queryString)
        
        return url
        
        
    def _buildUrl(self):        
        strtmpl = self.locTemplate.replace(self.insertpattern, '')
        tmpl = Template(strtmpl)
        url = tmpl.safe_substitute(self.substitutes)    
    
        if len(url) == 0:
            url = self.queriedClass._locator
            

        return url
    
    def setLocator(self, locTemplate):
        '''
        Set current locator template (string used to build current locator).
        
        Arguments:
            *locTemplate* The template to set.
        '''
        self.locTemplate=locTemplate
        return self;
